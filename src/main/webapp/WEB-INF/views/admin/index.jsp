<%-- 
    Document   : index
    Created on : 14/03/2017, 08:36:13 PM
    Author     : Lasar-Soporte
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="refresh" content="${session.maxInactiveInterval}"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><spring:message code="main.title"/></title>
        <link rel="shortcut icon" href="resources/img/ic_launcher.png" />
        <link rel="stylesheet" href="resources/css/vendor/google/css/material-icons.css">
        <link rel="stylesheet" href="resources/css/vendor/fontawesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="resources/css/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/vendor/bootstrap/css/simple-sidebar.css">
        <link rel="stylesheet" href="resources/css/vendor/data-tables/datatables.bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/vendor/angular/angular-material.min.css">
        <link rel="stylesheet" href="resources/css/vendor/data-tables/buttons.dataTables.min.css">
        <link rel="stylesheet" href="resources/css/vendor/data-tables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="resources/css/vendor/w3css/w3.css">
        <link rel="stylesheet" href="resources/css/style.css">   
        
        <script src="resources/js/vendor/jQuery/jquery-2.0.3.min.js"></script>        
        <script src="resources/js/vendor/Bootstrap/bootstrap.min.js"></script>
        <script src="resources/js/vendor/jQuery/jquery.dataTables.min.js"></script>
        <script src="resources/js/vendor/Angular/angular.min.js"></script>
        <script src="resources/js/vendor/Angular/ui-bootstrap-tpls-2.5.0.min.js"></script>                
        <script src="resources/js/vendor/Angular/angular-route.min.js"></script>
        <script src="resources/js/vendor/Angular/angular-cookies.min.js"></script>
        <script src="resources/js/vendor/Angular/angular-sanitize.js"></script>
        <script src="resources/js/vendor/Angular/angular-animate.min.js"></script>
        <script src="resources/js/vendor/Angular/angular-aria.min.js"></script>
        <script src="resources/js/vendor/Angular/angular-resource.js"></script>
        <script src="resources/js/vendor/Angular/angular-material.min.js"></script>
        <script src="resources/js/vendor/Angular/angular-datatables.min.js"></script>
        <script src="resources/js/vendor/Angular/angular-datatables.buttons.min.js"></script>
        <script src="resources/js/vendor/Angular/dataTables.buttons.min.js"></script>
        <script src="resources/js/vendor/Angular/dataTables.buttons.min.js"></script>
        <script src="resources/js/vendor/Angular/jszip.min.js"></script>
        <script src="resources/js/vendor/Angular/pdfmake.min.js"></script>
        <script src="resources/js/vendor/Angular/vfs_fonts.js"></script>
        <script src="resources/js/vendor/Angular/buttons.html5.min.js"></script>
        <script src="resources/js/vendor/Angular/blob.js"></script>
        <script src="resources/js/vendor/Angular/FileSaver.js"></script>
        <script src="resources/js/App/app.js"></script>
        <script src="resources/js/Services/CountryService.js"></script>        
        <script src="resources/js/Controller/MenuController.js"></script>       
        <script src="resources/js/Services/BankService.js"></script>
        <script src="resources/js/Services/CardService.js"></script>
        <script src="resources/js/Services/ClientStatusService.js"></script>
        <script src="resources/js/Services/FranchiseService.js"></script>
        <script src="resources/js/Services/ClientService.js"></script>
        <script src="resources/js/Controller/ClientController.js"></script>
        <script src="resources/js/Services/ProductService.js"></script>
        <script src="resources/js/Services/TransactionService.js"></script>
        <script src="resources/js/Controller/TransactionController.js"></script>
        <script src="resources/js/Services/ReverseTransactionService.js"></script>
        <script src="resources/js/Controller/ReverseTransactionController.js"></script>
        <script src="resources/js/Services/LocksService.js"></script>
        <script src="resources/js/Controller/LocksController.js"></script>
        <script src="resources/js/Services/UserTagService.js"></script>
        <script src="resources/js/Controller/UserTagController.js"></script>
        <script src="resources/js/Services/ProviderService.js"></script>
        <script src="resources/js/Controller/ProviderController.js"></script>
        <script src="resources/js/Services/RoleService.js"></script>
        <script src="resources/js/Services/UserService.js"></script>
        <script src="resources/js/Controller/UserController.js"></script>
        <script src="resources/js/Services/ConciliationService.js"></script>
        <script src="resources/js/Controller/ConciliationController.js"></script>
        <script src="resources/js/Services/AmericanExpressService.js"></script>
        <script src="resources/js/Controller/AmericanExpressController.js"></script>
    </head>
    <body ng-app="MyApp">                            
        <div id="wrapper" ng-controller="MenuController as ctrl" ng-class="ctrl.menuClass">
            <!-- Sidebar -->
            <div id="sidebar-wrapper" class="w3-card-8">
                <ul class="sidebar-nav">
                    <li>                         
                        <a href="" id="menu-toggle" ng-if="ctrl.menuClass !== 'toggled'"
                           ng-click="ctrl.menuToggle()">
                            <i class="material-icons w3-left w3-xlarge w3-text-white w3-padding-top">menu</i>
                        </a>
                        <a href="" id="menu-toggle" ng-if="ctrl.menuClass === 'toggled'"
                           ng-click="ctrl.menuToggle()">
                            <i class="material-icons w3-right w3-xlarge w3-text-white w3-padding-top">close</i>
                        </a>
                    </li>
                    <br>
                    <br>
                    <li>
                        <a href="#/home" class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-hover-text-white w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="<spring:message code="main.menu.home"/>">
                                home
                            </i>
                            <spring:message code="main.menu.home"/>
                        </a>
                    </li>
                    <!--
                    <li ng-if="ctrl.showModuleUser(10)">
                    --> 
                    <li ng-show="ctrl.showModuleUser(10)">                        
                        <a href="" ng-click="ctrl.menu.client.active = !ctrl.menu.client.active"
                           class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                person_outline
                            </i>
                            Clientes                            
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="!ctrl.menu.client.active"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                add
                            </i>
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="ctrl.menu.client.active"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                remove
                            </i>
                        </a>
                        <div uib-collapse="!ctrl.menu.client.active">
                            <div>
                                <ul class="list-group w3-text-white w3-padding-0 w3-margin-0">
                                    <li class="list-group-item w3-padding-0 w3-hover-amber w3-small">
                                        <a href="" ng-click="ctrl.showClientView('register')" 
                                            class="w3-text-orange w3-hover-amber">
                                             <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                                data-toggle="tooltip" data-container="body" title="Registrar cliente">
                                                 add_box
                                             </i>
                                             Registro
                                        </a>
                                    </li>
                                    <li class="list-group-item w3-padding-0 w3-hover-amber w3-small">
                                        <a href="" ng-click="ctrl.showClientView('search')" 
                                            class="w3-text-orange w3-hover-amber">
                                             <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                                data-toggle="tooltip" data-container="body" title="Buscar cliente">
                                                 search
                                             </i>
                                             Busqueda
                                        </a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    <!--
                    <li ng-if="ctrl.showModuleUser(12)">
                    --> 
                    <li ng-show="ctrl.showModuleUser(12)">
                        <a href="" ng-click="ctrl.showTransactionView()" 
                            class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Transacciones">
                                payment
                            </i>
                            Transacciones
                        </a>
                    </li>
                    
                    <!--
                    <li>
                        <a href="" ng-click="ctrl.showReverseView()" class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Reversos">                                
                                undo
                            </i>
                            Reversos
                        </a>
                    </li>
                    -->
                    <!--
                    <li ng-if="ctrl.showModuleUser(14)">
                    --> 
                    <li ng-if="ctrl.showModuleUser(14)">
                        <a href="" ng-click="ctrl.showLocksView()" class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Bloqueos">                                
                                block
                            </i>
                            Bloqueos
                        </a>
                    </li>
                    <!--
                    <li>
                        <a href="" ng-click="ctrl.showTagsView()" class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Tag">                                
                                bookmark_border
                            </i>
                            Tag
                        </a>
                    </li>
                    <li ng-if="ctrl.showModuleUser(16)">
                    -->                    
                    <li ng-if="ctrl.showModuleUser(16)">
                        <a href="" ng-click="ctrl.showConciliationView()" class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Conciliación">
                                compare_arrows
                            </i>
                            Conciliación
                        </a>
                    </li>
                    
                     <!--
                    <li ng-if="ctrl.showModuleUser(24)">
                    --> 
                    <li ng-show="ctrl.showModuleUser(24)">                        
                        <a href="" ng-click="ctrl.menu.americanExpress.active = !ctrl.menu.americanExpress.active"
                           class="w3-text-white w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                credit_card
                            </i>
                            American Express                            
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="!ctrl.menu.americanExpress.active"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                add
                            </i>
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="ctrl.menu.americanExpress.active"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                remove
                            </i>
                        </a>
                        <div uib-collapse="!ctrl.menu.americanExpress.active">
                            <div>
                                <ul class="list-group w3-text-white w3-padding-0 w3-margin-0">
                                    <li class="list-group-item w3-padding-0 w3-hover-amber w3-small">
                                        <a href="" ng-click="ctrl.showAmericanExpressView('non_processed_operations')" 
                                            class="w3-text-orange w3-hover-amber">
                                             <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                                data-toggle="tooltip" data-container="body" title="Registrar cliente">
                                                 announcement
                                             </i>
                                             Operaciones no procesadas
                                        </a>
                                    </li>
                                    <li class="list-group-item w3-padding-0 w3-hover-amber w3-small">
                                        <a href="" ng-click="ctrl.showAmericanExpressView('reverse')" 
                                            class="w3-text-orange w3-hover-amber">
                                             <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                                data-toggle="tooltip" data-container="body" title="Buscar cliente">
                                                 undo
                                             </i>
                                             Reverso operaciones 
                                        </a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                   
                    <!--
                    <li ng-if="ctrl.showModuleUser(18)">
                    --> 
                    <span class="menu_split"></span>
                    <li class="w3-text-black" ng-if="ctrl.showModuleUser(18)">                        
                        <a href="" ng-click="ctrl.menu.settings.active = !ctrl.menu.settings.active"
                           class="w3-text-black w3-hover-amber">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Configuración">
                                settings
                            </i>
                            Configuración                            
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="!ctrl.menu.settings.active"
                               data-toggle="tooltip" data-container="body" title="Configuración">
                                add
                            </i>
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="ctrl.menu.settings.active"
                               data-toggle="tooltip" data-container="body" title="Configuración">
                                remove
                            </i>
                        </a>
                        <div uib-collapse="!ctrl.menu.settings.active">
                            <div>
                                <ul class="list-group w3-text-white w3-padding-0 w3-margin-0">
                                    <!--
                    <li ng-if="ctrl.showModuleUser(20)">
                    --> 
                                    <li class="list-group-item w3-padding-0 w3-hover-amber w3-small" ng-if="ctrl.showModuleUser(20)">
                                        <a href="" ng-click="ctrl.showProvidersView()" 
                                           class="w3-text-orange w3-hover-amber">
                                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                               data-toggle="tooltip" data-container="body" title="Proveedores">
                                                group_work
                                            </i>
                                            Proveedores y Productos
                                        </a>
                                    </li>
                                    <!--
                    <li ng-if="ctrl.showModuleUser(22)">
                    --> 
                                    <li class="list-group-item w3-padding-0 w3-hover-amber w3-small" ng-if="ctrl.showModuleUser(22)">
                                        <a href="" ng-click="ctrl.showUsersView()" 
                                           class="w3-text-orange w3-hover-amber">
                                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                               data-toggle="tooltip" data-container="body" title="Usuarios">
                                                supervisor_account
                                            </i>
                                            Usuarios
                                        </a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="w3-text-black bottom-menu-item">
                        <a href="<c:url value="/logout" />"  class="w3-text-black w3-hover-orange"> 
                            <i class="fas fa-sign-out-alt w3-left w3-xlarge w3-padding-top"></i>
                            <spring:message code="main.menu.logout"/>
                        </a>                                                
                    </li>
                </ul>                
            </div>
            <!-- /#sidebar-wrapper -->
            <!-- Page Content -->
            <div id="page-content-wrapper" class="container">                             
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 w3-margin-0 w3-padding-top">
                        <img class="img-responsive" src="resources/img/ic_launcher.png">
                    </div>                                    
                </div>
                <div class="row" ng-view></div>
            </div>            
            <!-- /#page-content-wrapper -->                                                 
        </div>
    </body>
</html>
