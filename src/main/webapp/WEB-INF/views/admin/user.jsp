<%-- 
    Document   : user
    Created on : 23/04/2017, 12:48:54 PM
    Author     : Lasar-Soporte
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="UserController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">                
        <h4 class="w3-padding-8" >Módulo Administrador - Usuarios</h4>                
    </header>
    <div class="w3-center" ng-if="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>    
    <div class="w3-margin" ng-if="!ctrl.wait">                        
        <table id="usersTable" datatable="ng" 
               dt-options="ctrl.dtOptions" dt-column-defs="ctrl.dtColumnDefs" dt-instance="ctrl.dtInstance"
               class="table-responsive table-bordered w3-tiny">
            <thead>
                <tr class="table-header w3-padding-0">            
                    <th class="w3-text-white w3-padding-2 w3-center">ID</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Nombre de usuario</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Activo</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Cuenta expira</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Cuenta bloqueada</th>                    
                    <th class="w3-text-white w3-padding-2 w3-center">Contraseña expira</th>                                        
                    <th class="w3-text-white w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-center w3-text-white"
                           uib-tooltip="<spring:message code="client.table.option.add"/>"
                           data-toggle="modal" data-target="#userModal"
                           ng-click=" ctrl.reset(); ctrl.initUserModalData(ctrl.user,'create')">
                            <i class="material-icons" >                            
                                person_add
                            </i>                            
                        </a>
                    </th>                
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="user in ctrl.users track by user.id" 
                    class="w3-hover-orange w3-padding-0 w3-center">            
                    <td class="w3-padding-2 w3-center">{{ user.id }}</td>
                    <td class="w3-padding-2 w3-center">{{ user.username }}</td> 
                    <td class="w3-padding-2 w3-center">{{ user.enabled }}</td>
                    <td class="w3-padding-2 w3-center">{{ user.account_expired }}</td>
                    <td class="w3-padding-2 w3-center">{{ user.account_locked }}</td>                    
                    <td class="w3-padding-2 w3-center">{{ user.password_expired }}</td>                                       
                    <td class="w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Detalles" data-toggle="modal" data-target="#userModal"
                           ng-click="ctrl.initUserModalData(user,'detail')">
                            <i class="material-icons w3-text-blue">                            
                                search
                            </i>                            
                        </a>                        
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Editar" data-toggle="modal" data-target="#userModal"
                           ng-click="ctrl.initUserModalData(user, 'edit')">
                            <i class="material-icons w3-text-green">                            
                                mode_edit
                            </i>                            
                        </a>                                               
                    </td>         
                </tr>
            </tbody>
        </table>                
    </div>
    <div id="userModal" class="modal fade" role="dialog">
        <div ng-include="'/MobileCardCRM/admin/usuario/detail'"></div>
    </div>
</div>