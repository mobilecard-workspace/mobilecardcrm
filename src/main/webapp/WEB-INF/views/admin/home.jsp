<%--
    Document   : home
    Created on : 19/03/2017, 09:29:53 PM
    Author     : Lasar-Soporte
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<header id="mainHeader" class="w3-center w3-text-white">
    <h4 class="w3-padding-8" >Módulo Administrador</h4>                
</header>
<div class="w3-center">
    <h5 class="w3-text-grey">
        Bienvenido al portal administrativo de MobileCard, en el menu desplegable
        ubicado en el panel a tu izquierda se encuentran las opciones disponibles.
    </h5>
</div>
