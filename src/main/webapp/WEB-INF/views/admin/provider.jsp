<%-- 
    Document   : provider
    Created on : 26/03/2017, 11:40:04 AM
    Author     : Lasar-Soporte
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="ProviderController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">                
        <h4 class="w3-padding-8" >Módulo Administrador - Proveedores</h4>                
    </header>
    <div class="w3-center" ng-show="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin" ng-hide="ctrl.wait">                        
        <table id="providersTable" datatable="ng" 
               dt-options="ctrl.dtProviderOptions" dt-column-defs="ctrl.dtProviderColumnDefs" 
               dt-instance="ctrl.dtProviderInstance"
               class="table-responsive table-bordered w3-tiny">
            <thead>
                <tr class="table-header w3-padding-0">            
                    <th class="w3-text-white w3-padding-2 w3-center">ID</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Nombre Comercio</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Comisión</th>                                       
                    <th class="w3-text-white w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-center w3-text-white"
                           uib-tooltip="Agregar Proveedor" data-toggle="modal" data-target="#providerModal"
                           ng-click="ctrl.initProviderModalData('', 'create')">
                            <i class="material-icons" >                            
                                add
                            </i>                            
                        </a>
                    </th>                
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="provider in ctrl.providers track by provider.id_proveedor" 
                    class="w3-hover-orange w3-padding-0 w3-center">            
                    <td class="w3-padding-2 w3-center">{{ provider.id_proveedor }}</td>
                    <td class="w3-padding-2 w3-center">{{ provider.prv_nombre_comercio }}</td>
                    <td class="w3-padding-2 w3-center">{{ provider.comision }}</td>                                      
                    <td class="w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Detalles" data-toggle="modal" data-target="#providerModal"
                           ng-click="ctrl.initProviderModalData(provider, 'detail')">
                            <i class="material-icons w3-text-blue">                            
                                search
                            </i>                            
                        </a>
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Editar" data-toggle="modal" data-target="#providerModal"
                           ng-click="ctrl.initProviderModalData(provider, 'edit')">
                            <i class="material-icons w3-text-green">                            
                                mode_edit
                            </i>                            
                        </a>                                               
                    </td>         
                </tr>
            </tbody>
        </table>
    </div>
    <div id="providerModal" class="modal fade" role="dialog">
        <div ng-include="'/MobileCardCRM/admin/provider/detail'"></div>
    </div>
</div>