<%-- 
    Document   : conciliation
    Created on : 08/05/2017, 03:48:58 PM
    Author     : Lasar-Soporte
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="ConciliationController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">
        <h4 class="w3-padding-8" >Módulo Administrador - Conciliación</h4>
    </header>
    <div class="w3-center" ng-show="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin" ng-hide="ctrl.wait">
        <md-content class="md-padding w3-border">
            <md-tabs md-selected="0" md-border-bottom md-dynamic-height>
                <md-tab label="Estado de cuenta">
                    <md-content style="min-height: 410px">                                            
                        <div class="w3-center" ng-show="ctrl.wait">
                            <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
                        </div>
                        <div class="w3-small" ng-if="ctrl.search && !ctrl.wait">        
                            <form class="form-group col-lg-12 col-md-12 col-sm-12 test">
                                <div>                                                                            
                                    <!-- busqueda por rango de fecha-->
                                    <div class="w3-margin ">
                                        <div class="form-group">
                                            <label class="w3-label">Rango de estado de cuenta</label>                                                
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <label class="">Desde:</label>
                                                    <p class="input-group">                                    
                                                        <input type="text" class="form-control" uib-datepicker-popup
                                                               ng-model="ctrl.accountStatusDates.initDate" 
                                                               is-open="ctrl.initialDate.opened"  
                                                               ng-required="false" close-text="Cerrar" />
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default" 
                                                                    ng-click="ctrl.openInitialDate()">
                                                                <i class="glyphicon glyphicon-calendar"></i>
                                                            </button>
                                                        </span>                                     
                                                    </p>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <label class="">Hasta:</label>
                                                    <p class="input-group">                                    
                                                        <input type="text" class="form-control" uib-datepicker-popup 
                                                               ng-model="ctrl.accountStatusDates.endDate" 
                                                               is-open="ctrl.finalDate.opened"
                                                               ng-required="false" close-text="Cerrar" />
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default" 
                                                                    ng-click="ctrl.openFinalDate()">
                                                                <i class="glyphicon glyphicon-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>                        
                                        </div>
                                    </div>                
                                    <div class="w3-margin ">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
                                                    ng-click="ctrl.getAccountStatus()"
                                            ng-disabled="ctrl.validateAccountStatusDates()">Generar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="w3-padding-0 w3-margin-top" ng-if="ctrl.finishedSearch && !ctrl.wait">                        
                            <table id="conciliationsTable" datatable="ng" 
                                   dt-options="ctrl.dtOptions" dt-column-defs="ctrl.dtColumnDefs" dt-instance="ctrl.dtInstance"
                                   class="table-responsive table-bordered w3-tiny">
                                <thead>
                                    <tr class="table-header w3-padding-0">            
                                        <th class="w3-text-white w3-padding-2 w3-center">ID Bitacora</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">ID Usuario</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Nombre</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Apellido</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Número de tarjeta</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Fecha</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Hora</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Concepto</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Monto</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Status Transacción</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Código error</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Cargo aplicado</th>
                                        <!--<th class="w3-text-white w3-padding-2 w3-center">Num autorización TAE</th>-->
                                        <th class="w3-text-white w3-padding-2 w3-center">Autorización Proveedor</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Autorización Bancaria</th>                                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="entry in ctrl.accountStatus" 
                                        class="w3-hover-orange w3-padding-0 w3-center">            
                                        <td class="w3-padding-2 w3-center">{{ entry.id_bitacora }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.id_usuario }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.usr_nombre }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.usr_apellido }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.tarjeta_compra }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.bit_fecha }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.bit_hora }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.bit_concepto }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.bit_cargo }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.bit_status }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.bit_codigo_error }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.cargo_Aplicado }}</td>
                                        <!--<td class="w3-padding-2 w3-center">{{ entry.numTae }}</td>-->
                                        <td class="w3-padding-2 w3-center">{{ ctrl.getNumAutorizacionTAE(entry) }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.autorizacion }}</td>         
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </md-content>
                </md-tab> 
                <md-tab label="Generación de Archivo I+D">
                    <md-content style="min-height: 410px">
                        <div class="w3-center" ng-show="ctrl.wait">
                            <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
                        </div>
                        <div class="w3-small" ng-if="!ctrl.wait">        
                            <form class="form-group col-lg-12 col-md-12 col-sm-12 test">
                                <div>                                                                            
                                    <!-- busqueda por rango de fecha-->
                                    <div class="w3-margin ">
                                        <div class="form-group">                                            
                                            <div class="row">                                                
                                                <div class="col-sm-12 col-md-12 col-lg-12">
                                                    <label  class="w3-label">
                                                        Tipo de archivo
                                                    </label>
                                                    <select class="form-control" id="inputCountryClient"                                                    
                                                            ng-model="ctrl.fileData.type">
                                                        <option value="ADDCEL" >ADDCEL</option>
                                                        <option value="TUTAG" >TUTAG</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <label class="w3-label">
                                                        Fecha
                                                    </label>
                                                    <p class="input-group">                                    
                                                        <input type="text" class="form-control" uib-datepicker-popup
                                                               ng-model="ctrl.fileData.date" 
                                                               is-open="ctrl.fileDate.opened"  
                                                               ng-required="false" close-text="Cerrar" />
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default" 
                                                                    ng-click="ctrl.openFileDate()">
                                                                <i class="glyphicon glyphicon-calendar"></i>
                                                            </button>
                                                        </span>                                     
                                                    </p>                                                    
                                                </div>                                               
                                            </div>                        
                                        </div>
                                    </div>                
                                    <div class="w3-margin ">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
                                                    ng-click="ctrl.createFile()"
                                            ng-disabled="ctrl.validateCreateFileData()">Generar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>                        
                    </md-content>
                </md-tab>
                <md-tab label="Conciliación AMEX">
                    <md-content style="min-height: 410px">                                            
                        <div class="w3-center" ng-show="ctrl.wait">
                            <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
                        </div>
                        <div class="w3-small" ng-if="!ctrl.showAmexConciliationSummary && !ctrl.wait">        
                            <form class="form-group col-lg-12 col-md-12 col-sm-12 test">
                                <div>                                                                            
                                    <!-- busqueda por rango de fecha-->
                                    <div class="w3-margin ">
                                        <div class="form-group">
                                            <label class="w3-label">Fecha de conciliación AMEX:</label>                                                
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12">                                                    
                                                    <p class="input-group">                                    
                                                        <input type="text" class="form-control" uib-datepicker-popup="{{ctrl.dateFormat}}"
                                                               ng-model="ctrl.amexConciliationDate" 
                                                               is-open="ctrl.amexConciliationDateField.opened"  
                                                               ng-required="false" close-text="Cerrar" />
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default" 
                                                                    ng-click="ctrl.openAmexConciliationDate()">
                                                                <i class="glyphicon glyphicon-calendar"></i>
                                                            </button>
                                                        </span>                                     
                                                    </p>
                                                </div>                                               
                                            </div>                        
                                        </div>
                                    </div>                
                                    <div class="w3-margin ">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
                                                    ng-click="ctrl.getAmexConciliation()"
                                            ng-disabled="ctrl.validateAmexConciliationDate()">Conciliar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>                                                        
                        </div>     
                        <div class="row w3-center w3-margin-0 w3-small" ng-if="ctrl.showAmexConciliationSummary && !ctrl.wait">
                            <div id="amexConciliationSummary" compile-html="ctrl.amexConciliationSummary"
                                 class="col-lg-12 col-md-12 col-sm-12"></div>
                        </div>
                    </md-content>
                </md-tab>
            </md-tabs>
        </md-content>
    </div>        
</div>