<%-- 
    Document   : userTag
    Created on : 26/03/2017, 10:42:37 AM
    Author     : Lasar-Soporte
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="UserTagController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">                
        <h4 class="w3-padding-8" >Módulo Administrador - Tags</h4>                
    </header>
    <div class="w3-center" ng-show="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin w3-small" ng-if="ctrl.search && !ctrl.wait">        
        <form class="form-group col-lg-12 col-md-12 col-sm-12 test">
            <div>
                
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por Id de usuario:</label> 
                        <input class="form-control" type="text" name="id_usuario" placeholder="Id del Usuario"
                                ng-model="ctrl.userTagSearch.id_usuario" />
                    </div>
                </div>
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por Número de tag</label> 
                        <input class="form-control" type="text" name="tag" placeholder="Tag"
                                ng-model="ctrl.userTagSearch.tag" />
                    </div>
                </div>
                <div class="w3-margin ">
                    <div class="form-group">
                        <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
                                ng-click="ctrl.searchTagByParameters()"
                        ng-disabled="ctrl.validateTagSearch()">Buscar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="w3-margin" ng-if="ctrl.finishedSearch && !ctrl.wait">                        
        <table id="reverseTransactionsTable" datatable="ng" 
               dt-options="ctrl.dtUserTagOptions" dt-column-defs="ctrl.dtUserTagColumnDefs" dt-instance="ctrl.dtUserTagInstance"
               class="table-responsive table-bordered w3-tiny">
            <thead>
                <tr class="table-header w3-padding-0">            
                    <th class="w3-text-white w3-padding-2 w3-center">ID</th>
                    <th class="w3-text-white w3-padding-2 w3-center">ID Usuario</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Tipo Recarga</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Etiqueta</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Tag</th>
                    <th class="w3-text-white w3-padding-2 w3-center">DV</th>                                        
                    <th class="w3-text-white w3-padding-2 w3-center">                        
                    </th>                
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="userTag in ctrl.userTags track by userTag.id" 
                    class="w3-hover-orange w3-padding-0 w3-center">            
                    <td class="w3-padding-2 w3-center">{{ userTag.id }}</td>
                    <td class="w3-padding-2 w3-center">{{ userTag.id_usuario }}</td>
                    <td class="w3-padding-2 w3-center">{{ userTag.id_tiporecargatag }}</td>
                    <td class="w3-padding-2 w3-center">{{ userTag.etiqueta }}</td>
                    <td class="w3-padding-2 w3-center">{{ userTag.tag }}</td>
                    <td class="w3-padding-2 w3-center">{{ userTag.dv }}</td>                                        
                    <td class="w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Detalles" data-toggle="modal" data-target="#userTagModal"
                           ng-click="ctrl.initTagModalData(userTag)">
                            <i class="material-icons w3-text-blue">                            
                                search
                            </i>                            
                        </a>                                               
                    </td>         
                </tr>
            </tbody>
        </table>
    </div>
    <div id="userTagModal" class="modal fade" role="dialog">
        <div ng-include="'/MobileCardCRM/admin/tag/detail'"></div>
    </div>
</div>