<%-- 
    Document   : tusuario
    Created on : 19-jan-2018
    Author     : oskar.cahuenas
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="form-group">
	<md-content class="md-padding w3-border">
		<md-tabs md-selected="0" md-border-bottom md-dynamic-height>
			<md-tab label="Reverso de operaciones">
				<md-content style="min-height: 410px">                                            
					<div class="w3-small" ng-if="!ctrl.wait">        
					    <form class="form-group col-lg-12 col-md-12 col-sm-12 test" name="form_reversed_operations">
					        <div>                                                                            
					            <div class="w3-margin ">
					                <div class="form-group">
					                    <label class="w3-label">Ingrese los Ids de transacciones separados por espacios. Los Ids contienen únicamente dígitos</label>                                                
					                    <div class="row">
					                        <div class="col-lg-6 col-md-6 col-sm-12">
					                            <p class="input-group">              
					                            	<textarea 
					                            			class="form-control" 
					                            			rows="5" 
					                            			cols="250"
					                            			ng-model="ctrl.strReversedIds" 
					                            			ng-pattern="ctrl.reverseValidateRegex"
					                            			style="width: 100%"
					                            			/>
					                            </p>
					                        </div>
					                    </div>                        
					                </div>
					            </div>                
					            <div class="w3-margin ">
					                <div class="form-group">
					                    <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
					                            ng-click="ctrl.getReversedOperations()"
					                    		ng-disabled="!form_reversed_operations.$valid">
					                    		Aplicar Reverso
					                    </button>
					                </div>
					            </div>
					            
					        </div>
					    </form>
					</div>
					
					<hr style="visibility: hidden; clear: both" ng-if="!ctrl.wait && ctrl.showSuccessMessage"/>
					<div class="row">
                       	<div class="col-md-3">
                       	</div>
				  		<div class="col-md-6">
							<div class="w3-card-4 w3-green w3-padding-32 w3-center" ng-if="!ctrl.wait && ctrl.showSuccessMessage">
	                       		Se actualizaron exitosamente los registros con Ids {{reversedIdsTxt}}
	                		</div>
				  		</div>
				  		<div class="col-md-3">
                       	</div>
				 	</div>
					
					<hr style="visibility: hidden; clear: both" ng-if="!ctrl.wait && ctrl.showErrorMessage"/>
					<div class="row">
                       	<div class="col-md-3">
                       	</div>
				  		<div class="col-md-6">
							<div class="w3-card-4 w3-red w3-padding-32 w3-center" ng-if="!ctrl.wait && ctrl.showErrorMessage">
	                       	Error en la carga de datos. Intente de nuevo.
	                		</div>
				  		</div>
				  		<div class="col-md-3">
                       	</div>
				 	</div>
					
				</md-content>
		    </md-tab> 
		</md-tabs>
	</md-content>
</div>