<%-- 
    Document   : tusuario
    Created on : 19-jan-2018
    Author     : oskar.cahuenas
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="AmericanExpressController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">                
        <h4 class="w3-padding-8" >Módulo American Express</h4>                
    </header>
    <div class="w3-center" ng-if="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin w3-small" ng-if="!ctrl.wait && ctrl.action == 'non_processed_operations'">        
    	<div ng-include="'/MobileCardCRM/admin/american_express/pages/non_processed_operations'"></div>
    </div>
 	<div class="w3-margin w3-small" ng-if="!ctrl.wait && ctrl.action == 'reverse'">        
    	<div ng-include="'/MobileCardCRM/admin/american_express/pages/reverse_operations'"></div>
    </div>
</div>