<%-- 
    Document   : tusuario
    Created on : 19-jan-2018
    Author     : oskar.cahuenas
--%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <div class="form-group">
	<md-content class="md-padding w3-border">
		<md-tabs md-selected="0" md-border-bottom md-dynamic-height>
			<md-tab label="Operaciones no procesadas">
				<md-content style="min-height: 410px">                                            
					<div class="w3-small" ng-if="!ctrl.wait">        
					    <form class="form-group col-lg-12 col-md-12 col-sm-12 test" name="form_non_processed_dates">
					        <div>                                                                            
					            <div class="w3-margin ">
					                <div class="form-group">
					                    <label class="w3-label">Seleccione fecha</label>                                                
					                    <div class="row">
					                        <div class="col-lg-6 col-md-6 col-sm-12">
					                            <p class="input-group">                                    
					                                <input type="text" class="form-control" 
					                                		uib-datepicker-popup
					                                        datepicker-options = "ctrl.datePickerOptions"
					                                        ng-model="ctrl.nonProcessedDates.initDate" 
					 										is-open="ctrl.initialDate.opened"  
					                                        ng-required="true" 
					                                        close-text="Cerrar" 
					                                />
					                                <span class="input-group-btn">
					                                    <button type="button" class="btn btn-default" 
					                                            ng-click="ctrl.openInitialDate()">
					                                        <i class="glyphicon glyphicon-calendar"></i>
					                                    </button>
					                                </span>                                     
					                            </p>
					                        </div>
					                    </div>                        
					                </div>
					            </div>                
					            <div class="w3-margin ">
					                <div class="form-group">
					                    <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
					                            ng-click="ctrl.getNonProcessedOperations()"
					                    		ng-disabled="!form_non_processed_dates.$valid">
					                    		Generar
					                    </button>
					                </div>
					            </div>
					        </div>
					    </form>
					</div>
					<div class="w3-padding-0 w3-margin-top" ng-if="!ctrl.wait && ctrl.showNonProcessedTable">                        
                            <table id="nonProcessedOperationsTable" datatable="ng" 
                                   dt-options="ctrl.dataTable.dtOptions" 
                                   dt-column-defs="ctrl.dataTable.dtColumnDefs" 
                                   dt-instance="ctrl.dataTable.dtInstance"
                                   class="table-responsive table-bordered w3-tiny">
                                <thead>
                                    <tr class="table-header w3-padding-0">            
                                        <th class="w3-text-white w3-padding-2 w3-center">Monto</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">CID</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Nombres</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Apellidos</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Dirección</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Teléfono</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Email</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">CP</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Mensaje Id</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Tipo</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Subtipo</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Reversal</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Reversal Idn</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Acc Reference</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Approval Code</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Fecha</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Procesada</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Tipo Tarjeta</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Acceptor Id</th>                                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="entry in ctrl.dataTable.transactions" 
                                        class="w3-hover-orange w3-padding-0 w3-center">            
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionMonto }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionCid }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionNombres }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionApellidos }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionDireccion }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionTelefono }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionEmail }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionCp }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.MensajeId }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionTipo }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionSubTipo }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionReversal }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionReversalIdn }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionAccReference }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionApprovalCode }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionDate }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionProcesada }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionTipoTarjeta }}</td>
                                        <td class="w3-padding-2 w3-center">{{ entry.transaccionAcceptorId }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr style="visibility: hidden; clear: both" ng-if="!ctrl.wait && ctrl.showErrorMessage"/>
                        <div class="row">
                        	<div class="col-md-3">
                        	</div>
					  		<div class="col-md-6">
								<div class="w3-card-4 w3-red w3-padding-32 w3-center" ng-if="!ctrl.wait && ctrl.showErrorMessage">
		                       	Error en la carga de datos. Intente de nuevo.
		                		</div>
					  		</div>
					  		<div class="col-md-3">
                        	</div>
					 	</div>
                </md-content>
		    </md-tab> 
		</md-tabs>
	</md-content>
</div>