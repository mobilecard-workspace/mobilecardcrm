<%-- 
    Document   : userTagTemplate
    Created on : 12/04/2017, 03:26:54 PM
    Author     : Lasar-Soporte
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" ng-click="ctrl.reset()">
                &times;
            </button>
            <h4 class="modal-title">TAG - {{ctrl.userTag.id}}</h4>
        </div>        
        <div class="modal-body">
            <div class="w3-center" ng-if="ctrl.modalWait">
                <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
            </div>        
            <div ng-if="!ctrl.modalWait">
                <div class="container-fluid">
                    <form class="form-horizontal" role="form">
                        <div class="form-group w3-tiny">                                                                                                                                       
                            <div class="row w3-margin">
                                <div class="col-lg-12">
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            ID Usuario
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.userTag.id_usuario"
                                                   ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Tipo de recarga
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.userTag.id_tiporecargatag" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Etiqueta
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.userTag.etiqueta"
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Tag
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.userTag.tag" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            dv
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.userTag.dv" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>                                    
                                </div>                                
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">                        
            <button type="button" class="btn btn-default w3-right"                    
                    data-dismiss="modal" ng-click="ctrl.reset()">
                Cerrar
            </button>            
        </div>
    </div>

</div>