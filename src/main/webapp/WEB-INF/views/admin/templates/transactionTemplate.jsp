<%-- 
    Document   : transactionTemplate
    Created on : Apr 12, 2017, 10:49:38 AM
    Author     : marcopascale
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" ng-click="ctrl.reset()">
                &times;
            </button>
            <h4 class="modal-title">Transacción - {{ctrl.transaction.id_bitacora}}</h4>
        </div>        
        <div class="modal-body">
            <div class="w3-center" ng-if="ctrl.modalWait">
                <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
            </div>        
            <div ng-if="!ctrl.modalWait">
                <div class="container-fluid">
                    <form class="form-horizontal" role="form" ng-if="!ctrl.blockingClient">
                        <div class="form-group w3-tiny">                                                                                                                                       
                            <div class="row w3-margin">
                                <div class="col-lg-6 w3-left w3-bordered w3-border w3-border-theme col">
                                    <!--<div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">ID Transacción</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputIdTrasaction"
                                                   ng-model="ctrl.transaction.id_bitacora" ng-disabled="true"/>
                                        </div>           
                                    </div>-->
                                    <div class="row w3-center w3-bordered w3-border-bottom w3-border-theme">
                                        <h4 class="text-muted">Datos del cliente</h4>
                                    </div>
                                    <div class="row w3-margin">
                                        <label class="col-sm-12 col-md-4 col-lg-4 control-label">Nombre de Usuario</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputIdProveedor"
                                                   ng-model="ctrl.client.usr_login" ng-disabled="true"/>
                                        </div>           
                                    </div>                                    
                                    <div class="row w3-margin">
                                        <label class="col-sm-12 col-md-4 col-lg-4 control-label">Nombre cliente</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputIdProveedor"
                                                   ng-model="ctrl.client.usr_nombre" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Apellido cliente</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputIdProveedor"
                                                   ng-model="ctrl.client.usr_apellido" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin" ng-if="ctrl.client.id_usr_status === 3">
                                        <div class="col-sm-12 col-md-12 col-lg-12 w3-center">
                                            <h5  class="w3-text-red">Usuario Bloqueado</h5>                                        
                                        </div>                                        
                                    </div>
                                    <button type="button" class="btn w3-left w3-red" ng-if="ctrl.client.id_usr_status === 1"                   
                                            ng-click="ctrl.blockClient()">
                                        Bloquear cliente
                                    </button>
                                </div>
                                <div class="col-lg-6 w3-right w3-bordered w3-border w3-border-theme col">
                                    <div class="row w3-center w3-bordered w3-border-bottom w3-border-theme">
                                        <h4 class="text-muted">Datos de la transacción</h4>
                                    </div> 
                                    <div class="row w3-margin">
                                        <label class="col-sm-12 col-md-4 col-lg-4 control-label">Tarjeta</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputBuyCard"
                                                   ng-model="ctrl.transaction.tarjeta_compra" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Destino</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputBuyCard"
                                                   ng-model="ctrl.transaction.referencia" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Importe</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputBuyCard"
                                                   ng-model="ctrl.transaction.bit_cargo" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Concepto</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <textarea type="text" class="form-control" 
                                                   id="inputStatus"
                                                   ng-model="ctrl.transaction.bit_concepto" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Status</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputStatus"
                                                   ng-model="ctrl.transaction.statusMessages.shortMessage" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin" ng-if="ctrl.transaction.bit_status !== 1">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Resultado</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <textarea type="text" class="form-control" 
                                                   id="inputStatus"
                                                   ng-model="ctrl.transaction.statusMessages.longMessage" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin" ng-if="ctrl.transaction.bit_status === 'Aprobada'">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Número de autorización</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputStatus"
                                                   ng-model="ctrl.transaction.bit_no_autorizacion" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Fecha</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" date='dd-MM-yyyy' 
                                                   id="inputDate" class="form-control" 
                                                   ng-model="ctrl.transaction.bit_fecha" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">Hora</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   id="inputTime"
                                                   ng-model="ctrl.transaction.bit_hora" ng-disabled="true"/>
                                        </div>           
                                    </div>                                    
                                </div>
                            </div>                            
                        </div>
                    </form>
                    <div ng-include="'/MobileCardCRM/admin/cliente/detail'" ng-if="ctrl.blockingClient"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        
        </div>
    </div>

</div>
