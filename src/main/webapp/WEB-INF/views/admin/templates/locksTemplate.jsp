<%-- 
    Document   : locksTemplate
    Created on : 26/06/2017, 11:01:12 AM
    Author     : Lasar-Soporte
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header w3-center">
            <button type="button" class="close" data-dismiss="modal" ng-click="ctrl.reset()">
                &times;
            </button>
            <h3 class="modal-title" ng-if="ctrl.cardDetail">
                Tarjeta - {{ctrl.lockedCard.tarjeta}}
            </h3>
            <h4 class="modal-title" ng-if="!ctrl.cardDetail">
                Dispositivo - {{ctrl.lockedImei.imei}}
            </h4>
        </div>        
        <div class="modal-body" ng-if="ctrl.cardDetail">            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row w3-margin">
                            <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                for="inputExpiredDateCardClient">Número</label>
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <input type="text" class="form-control" 
                                       ng-disabled="!ctrl.lockingCard"
                                       ng-model="ctrl.lockedCard.tarjeta"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row w3-margin">
                            <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                    for="inputStatusClient">Activo</label>
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <select class="form-control" ng-model="ctrl.lockedCard.activo">                                    
                                    <option value="0" ng-selected="ctrl.lockedCard.activo === 0">Activo</option>
                                    <option value="1" ng-selected="ctrl.lockedCard.activo === 1">Bloqueado</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-body" ng-if="!ctrl.cardDetail">            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row w3-margin">
                            <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                for="inputExpiredDateCardClient">Imei</label>
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <input type="text" class="form-control" 
                                       ng-disabled="!ctrl.lockingImei"
                                       ng-model="ctrl.lockedImei.imei"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row w3-margin">
                            <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                    for="inputStatusClient">Activo</label>
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <select class="form-control" ng-model="ctrl.lockedImei.activo">                                    
                                    <option value="0" ng-selected="ctrl.lockedImei.activo === 0">Activo</option>
                                    <option value="1" ng-selected="ctrl.lockedImei.activo === 1">Bloqueado</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">            
            <button type="button" ng-if="!ctrl.cardDetail"
                    class="btn btn-default w3-left w3-green" 
                    ng-click="ctrl.addOrUpdateLockedImei()"
                    ng-disabled="ctrl.validateDataLockedImei()"
                    data-dismiss="modal">
                Guardar
            </button>
            <button type="button" ng-if="ctrl.cardDetail"
                    class="btn btn-default w3-left w3-green" 
                    ng-click="ctrl.addOrUpdateLockedImei()"
                    ng-disabled="ctrl.validateDataLockedCard()"
                    data-dismiss="modal">
                Guardar
            </button>
            <button type="button" class="btn btn-default w3-right"                    
                    data-dismiss="modal" ng-click="ctrl.reset()">
                Cancelar
            </button>
        </div>
    </div>

</div>
