<%-- 
    Document   : clientTemplate
    Created on : 04/04/2017, 07:40:29 PM
    Author     : Lasar-Soporte
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal-dialog modal-lg">    
    <!-- Modal content-->
    <div class="modal-content">        
        <div class="modal-header w3-center">
            <button type="button" class="close" data-dismiss="modal" ng-click="ctrl.reset()">
                &times;
            </button>
            <h3 class="modal-title" ng-if="ctrl.clientEmpty">
                Cliente no encontrado
            </h3>
            <h4 class="modal-title" ng-if="!ctrl.clientEmpty">
                Cliente - {{ctrl.client.usr_login}}
            </h4>
        </div>
        <div class="w3-center" ng-if="ctrl.wait">
            <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
        </div>        
        <div class="modal-body" ng-if="!ctrl.clientEmpty">
            <div ng-cloak ng-if="!ctrl.wait">
                <md-content>
                    <md-tabs md-dynamic-height md-border-bottom>
                        <md-tab>
                            <md-tab-label>Datos de acceso</md-tab-label>
                            <md-tab-body>
                                <div class="container-fluid">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group w3-tiny">                                                                                                                                       
                                            <div class="row w3-margin">
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputLoginClient">Login</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputLoginClient"
                                                                   ng-model="ctrl.client.usr_login" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>                                                    
                                                    <div class="row w3-margin" ng-show="!ctrl.editingStatusClient">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputEmailClient">Email</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="email" class="form-control" 
                                                                   id="inputEmail3"
                                                                   ng-model="ctrl.client.email" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>           
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin" ng-if="ctrl.action === 'register'">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputPasswordClient">Contraseña</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="password" class="form-control" 
                                                                   id="inputPasswordClient"
                                                                   ng-model="ctrl.client.usr_pwd"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin" ng-if="ctrl.action === 'register'">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputPasswordClient">Repetir Contraseña</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="password" class="form-control" 
                                                                   id="inputPasswordClient"
                                                                   ng-model="ctrl.pwd_aux"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputStatusClient">Status</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <select class="form-control" id="inputStatusClient"                                                    
                                                                    ng-model="ctrl.client.id_usr_status"
                                                                    ng-options="status.id_usr_status as status.desc_usr_status for status in ctrl.clientStatus"
                                                                    ng-disabled="!ctrl.editingClient && !ctrl.creatingClient">                                                                
                                                            </select>
                                                        </div>
                                                    </div>                                                                        
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </md-tab-body>
                        </md-tab>
                        <md-tab>                            
                            <md-tab-label>Datos Generales</md-tab-label>                            
                            <md-tab-body>
                                <div class="container-fluid">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group w3-tiny">                                                                                                                                       
                                            <div class="row w3-margin">
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputNameClient">Nombre</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputNameClient"
                                                                   ng-model="ctrl.client.usr_nombre" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputLastNameClient">Apellido</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputLastNameClient"
                                                                   ng-model="ctrl.client.usr_apellido" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin" ng-show="!ctrl.editingStatusClient">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputPhoneClient">Telefono</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputPhoneClient"
                                                                   ng-model="ctrl.client.usr_telefono" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin" ng-show="!ctrl.editingStatusClient">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputAddressClient">Dirección</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <textarea type="text" class="form-control" 
                                                                      id="inputAddressClient"
                                                                      ng-model="ctrl.client.usr_direccion" 
                                                                      ng-disabled="!ctrl.editingClient && !ctrl.creatingClient">
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputCountryClient">País</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <select class="form-control" id="inputCountryClient"                                                    
                                                                    ng-model="ctrl.client.idpais"                                                                     
                                                                    ng-options="country.idpaises as country.nombre for country in countries"
                                                                    ng-disabled="!ctrl.editingClient && !ctrl.creatingClient">                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputFechaNacimientoClient">Fecha de nacimiento</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <p class="input-group">                                    
                                                                <input type="text" class="form-control" uib-datepicker-popup 
                                                                       ng-model="ctrl.client.usr_fecha_nac" 
                                                                       ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"
                                                                       is-open="ctrl.bornDate.opened"
                                                                       ng-required="false" close-text="Cerrar" 
                                                                       id="inputFechaNacimientoClient"/>
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default" 
                                                                            ng-click="ctrl.openBornDate()">
                                                                        <i class="glyphicon glyphicon-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </p>                                                                                
                                                        </div>                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </md-tab-body>
                        </md-tab>
                        <md-tab>
                            <md-tab-label>
                                <span ng-if="!ctrl.showWallet">
                                    Datos Bancarios
                                </span>
                                <span ng-if="ctrl.showWallet">
                                    Wallet
                                </span>
                            </md-tab-label>
                            <md-tab-body>
                                <div class="container-fluid" ng-if="!ctrl.showWallet">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group w3-tiny">                                                                                                                                       
                                            <div class="row w3-margin">
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputTypeCardClient">Tipo de tarjeta</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <select class="form-control" id="inputCountryClient"                                                    
                                                                    ng-model="ctrl.client.id_tipo_tarjeta"
                                                                    ng-options="typeCard.id_tipo_tarjeta as typeCard.desc_tipo_tarjeta 
                                                                        for typeCard in ctrl.typeCards"
                                                                    ng-disabled="!ctrl.editingClient && !ctrl.creatingClient">                                                                                                                                    
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputNumberCardClient">Número de tarjeta</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputNumberCardClient"
                                                                   ng-model="ctrl.client.usr_tdc_numero" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputNumberCardClient">Nombre del tarjeta Habiente</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputNumberCardClient"
                                                                   ng-model="ctrl.clientCard.tarjeta_habiente" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputExpiredDateCardClient">Vigencia de tarjeta</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputExpiredDateCardClient"
                                                                   ng-model="ctrl.client.usr_tdc_vigencia" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputExpiredDateCardClient">CVV</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputExpiredDateCardClient"
                                                                   ng-model="ctrl.clientCard.cvv" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="container-fluid" ng-if="!ctrl.addingClientCard && ctrl.showWallet">
                                    <div class="row w3-margin">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <table id="clientWalletTable" datatable="ng" 
                                                   dt-options="ctrl.dtWalletOptions" dt-column-defs="ctrl.dtWalletColumnDefs" 
                                                   dt-instance="ctrl.dtWalletInstance"
                                                   class="table-responsive table-bordered w3-tiny">
                                                <thead>
                                                    <tr class="table-header w3-padding-0">            
                                                        <th class="w3-text-white w3-padding-2 w3-center">Número</th>
                                                        <th class="w3-text-white w3-padding-2 w3-center">Tarjetahabiente</th>
                                                        <th class="w3-text-white w3-padding-2 w3-center">Tipo</th>
                                                        <th class="w3-text-white w3-padding-2 w3-center">Vigencia</th>                                                        
                                                        <th class="w3-text-white w3-padding-2 w3-center">CVV</th>
                                                        <th class="w3-text-white w3-padding-2 w3-center">
                                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-center w3-text-white"
                                                               uib-tooltip="Agregar tarjeta" ng-if="ctrl.editingClient"
                                                               ng-click="ctrl.initAddClientCard()">
                                                                <i class="material-icons">                            
                                                                    <i class="material-icons">add</i>
                                                                </i>                                                                                           
                                                            </a>
                                                        </th>                
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="card in ctrl.clientCards track by card.idtarjetasusuario" 
                                                        class="w3-hover-orange w3-padding-0 w3-center">            
                                                        <td class="w3-padding-2 w3-center">{{ card.numerotarjeta}}</td>
                                                        <td class="w3-padding-2 w3-center">{{ card.nombre_tarjeta}}</td>
                                                        <td class="w3-padding-2 w3-center">{{ ctrl.getClientCardType(card.idtarjetas_tipo)}}</td>
                                                        <td class="w3-padding-2 w3-center">{{ card.vigencia}}</td>
                                                        <!--<td class="w3-padding-2 w3-center">{{ card.ct}}</td>-->
                                                        <td class="w3-padding-2 w3-center">***</td>
                                                        <td class="w3-padding-2 w3-center">                                                                                    
                                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                                                               ng-if="ctrl.editingClient" uib-tooltip="Editar tarjeta"
                                                               ng-click="ctrl.initClientCardData(card)">
                                                                <i class="material-icons w3-text-green">                            
                                                                    mode_edit
                                                                </i>                            
                                                            </a>                                               
                                                        </td>         
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid" ng-if="ctrl.addingClientCard && ctrl.showWallet">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group w3-tiny">                                                                                                                                       
                                            <div class="row w3-margin">
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputTypeCardClient">Tipo de tarjeta</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <select class="form-control" id="inputCountryClient"                                                    
                                                                    ng-model="ctrl.clientCard.idtarjetas_tipo"
                                                                    ng-options="typeCard.id_tipo_tarjeta as typeCard.desc_tipo_tarjeta 
                                                                        for typeCard in ctrl.typeCards"
                                                                    ng-disabled="!ctrl.editingClient && !ctrl.creatingClient">                                                                                                                                    
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputNumberCardClient">Número de tarjeta</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputNumberCardClient"
                                                                   ng-model="ctrl.clientCard.numerotarjeta" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputNumberCardClient">Nombre del tarjeta Habiente</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputNumberCardClient"
                                                                   ng-model="ctrl.clientCard.nombre_tarjeta" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputBankCardClient">Franquicia</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <select class="form-control" id="inputCountryClient"                                                    
                                                                    ng-model="ctrl.clientCard.idfranquicia"
                                                                    ng-options="franchise.idfranquicias as franchise.nombre 
                                                                        for franchise in ctrl.franchises"
                                                                    ng-disabled="!ctrl.editingClient && !ctrl.creatingClient">                                                                                                                                    
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6">
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputExpiredDateCardClient">Vigencia de tarjeta</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputExpiredDateCardClient"
                                                                   ng-model="ctrl.clientCard.vigencia" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputExpiredDateCardClient">CVV</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <input type="text" class="form-control" 
                                                                   id="inputExpiredDateCardClient"
                                                                   ng-model="ctrl.clientCard.ct" 
                                                                   ng-disabled="!ctrl.editingClient && !ctrl.creatingClient"/>
                                                        </div>
                                                    </div>
                                                    <div class="row w3-margin">
                                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                                for="inputBankCardClient">Banco</label>
                                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                                            <select class="form-control" id="inputCountryClient"                                                    
                                                                    ng-model="ctrl.clientCard.idbanco"
                                                                    ng-options="bank.id_banco as bank.desc_banco 
                                                                        for bank in ctrl.banks"
                                                                    ng-disabled="!ctrl.editingClient && !ctrl.creatingClient">                                                                                                                                    
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </md-tab-body>                            
                        </md-tab>
                    </md-tabs>
                </md-content>
            </div>                               
        </div>
        <div class="modal-footer" ng-if="!ctrl.wait">
            <button type="button" class="btn w3-left w3-blue" 
                    ng-click="ctrl.editingClient = true; ctrl.detailClient = false;"
                    ng-if="ctrl.detailClient">
                Editar
            </button>
            <button type="button" class="btn w3-left w3-green" 
                    data-dismiss="modal" ng-click="ctrl.editClient()"
                    ng-disabled="ctrl.validateDataClient()"
                    ng-show="ctrl.editingClient && !ctrl.addingClientCard">
                Guardar
            </button>
            <button type="button" class="btn w3-left w3-green" 
                    data-dismiss="modal" ng-click="ctrl.addClient()"
                    ng-disabled="ctrl.validateDataClient()"
                    ng-show="ctrl.creatingClient && !ctrl.addingClientCard">
                Guardar
            </button>
            <button type="button" class="btn btn-primary w3-right"
                    ng-if="ctrl.action !== 'register'"                     
                    data-dismiss="modal" ng-click="ctrl.reset()"
                    ng-show="!ctrl.addingClientCard">
                Cancelar
            </button>
            <button type="button" class="btn btn-default w3-right"
                    ng-if="ctrl.action === 'register'"                    
                    ng-click="ctrl.reset()">
                Limpiar
            </button>
            <button type="button" class="btn w3-left w3-green" 
                    ng-click="ctrl.addOrUpdateClientCard()"
                    ng-disabled="ctrl.validateDataClientCard()"
                    ng-show="ctrl.addingClientCard">
                Agregar
            </button>
            <button type="button" class="btn w3-right w3-blue" 
                    ng-click="ctrl.resetClientCard()"
                    ng-show="ctrl.addingClientCard">
                Cancelar
            </button>
        </div>
    </div>

</div>
