/* global angular */

app.controller('ReverseTransactionController', ['$rootScope', '$scope', 'ReverseTransactionService', '$location', 
    '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 
    function($rootScope, $scope, ReverseTransactionService, $location, $window, DTOptionsBuilder, DTColumnDefBuilder) {            
    
    var self = this;  
    self.totalReverseTransactions = "";
    self.reverseTransactionsToShow = 500;
    self.lastPageLoaded = 0;
    self.pageDT = 0;
    self.reverseTransactions = [];
    self.search = true;
    
    self.dateFormat = 'yyyy-MM-dd';
    
    self.initialDate = {
        opened: false
    };
    
    self.finalDate = {
        opened: false
    };        
    
    self.registrationDate = {
        opened: false
    };
    
    self.bornDate = {
        opened: false
    };
    
    self.reverseTransaction = {
        id_bitacora : "",
        id_usuario : "",
        secuencia : "",
        fechahora : "",
        autorizacion : "",
        claveoperacion : "",
        claveventa : "",
        descripcion : "",
        errorcode : "",
        importe : "",
        resultado : ""
    };
    
      
    self.reverseTransactionSearch = {        
        fechaHora_inicial: "",
        fechaHora_final: "",
        usr_nombre: "",
        usr_apellido: ""        
    };
      
    self.validateReverseTransactionSearch = function () {
        if (self.reverseTransactionSearch.usr_nombre !== ""){
            return false;
        }else if (self.reverseTransactionSearch.usr_apellido !== ""){
            return false;
        }else if (self.reverseTransactionSearch.fechaHora_inicial !== "" && self.reverseTransactionSearch.fechaHora_final !== ""){
            return false;
        }
        
        return true;
    };  
    
    self.searchReverseTransactionByParameters = function(){
        self.wait = true;
        self.finishedSearch = false;
        self.search = false;
               
        ReverseTransactionService.searchReverseTransactions(self.reverseTransactionSearch)        
        .then(
            function(d) {
                console.log(d);
                self.reverseTransactions = d.data;
               /* for (var i = 0; i < self.reverseTransactions.length; i++){
                    var registrationDate = self.reverseTransactions[i].usr_fecha_registro;
                    var bornDate = self.reverseTransactions[i].usr_fecha_nac;
                    self.reverseTransactions[i].usr_fecha_registro = new Date(registrationDate);
                    self.reverseTransactions[i].usr_fecha_registro.setMinutes(self.reverseTransactions[i].usr_fecha_registro.getMinutes() + self.reverseTransactions[i].usr_fecha_registro.getTimezoneOffset());
                    self.reverseTransactions[i].usr_fecha_nac = new Date(bornDate);
                    self.reverseTransactions[i].usr_fecha_nac.setMinutes(self.reverseTransactions[i].usr_fecha_nac.getMinutes() + self.reverseTransactions[i].usr_fecha_nac.getTimezoneOffset());
                }*/
                self.finishedSearch = true;
                self.search = false;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching reverseTransactions For List');
                self.showMessageError('Error while fetching reverseTransactions',errResponse);
            }
        );
    };
        
    self.getReverseTransactionList = function(){
        self.wait = true;
        ReverseTransactionService.getReverseTransactionList()        
        .then(
            function(d) {                
                self.reverseTransactions = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching reverseTransactions For List');
                self.showMessageError('Error while fetching reverseTransactions',errResponse);
            }
        );
    };
    
    self.getReverseTransactionsByPage = function(numberPage, maxResults){
        self.wait = true;
        ReverseTransactionService.getReverseTransactionsByPage(numberPage, maxResults)        
        .then(
            function(d) {                
                self.reverseTransactions = self.reverseTransactions.concat(d.data);
                self.lastPageLoaded = self.reverseTransactions.length;
                self.wait = false;                
                self.pageDT = 0;
            },
            function(errResponse){
                console.error('Error while fetching reverseTransactions For List');
                self.showMessageError('Error while fetching reverseTransactions',errResponse);
            }
        );
    };
       
    self.getNumberOfReverseTransactions = function(){
        self.wait = true;
        ReverseTransactionService.getNumberOfReverseTransactions()        
        .then(
            function(numberOfReverseTransactions) {                
                self.totalReverseTransactions = numberOfReverseTransactions.data;
                if (self.totalReverseTransactions <= 500){
                    self.getReverseTransactionList();
                }else {                    
                    self.getReverseTransactionsByPage(self.lastPageLoaded, self.reverseTransactionsToShow);
                }                
            },
            function(errResponse){
                console.error('Error while fetching reverseTransactions For List');
                self.showMessageError('Error while fetching reverseTransactions',errResponse);
            }
        );
    };
    
    self.reset = function(){
        
        self.reverseTransaction = {
            id_bitacora : "",
            id_usuario : "",
            secuencia : "",
            fechahora : "",
            autorizacion : "",
            claveoperacion : "",
            claveventa : "",
            descripcion : "",
            errorcode : "",
            importe : "",
            resultado : ""
        };
        
        self.reverseTransactionSearch = {        
            fechaHora_inicial: "",
            fechaHora_final: "",
            usr_nombre: "",
            usr_apellido: ""        
        };
    };           
           
    
    self.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withDOM('lBfrtip')
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true)
            .withButtons([                                
                {
                    text: 'Nueva Busqueda',
                    key: '3',
                    action: function (e, dt, node, config) {
                        self.initSearch();
                    }
                }
            ]);           
            
    self.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6)
    ];
    
    self.dtInstance = {};

    self.initReverseTransactionModalData = function (reverseTransaction) {
        self.reverseTransaction = reverseTransaction;
        self.detailReverseTransaction = true; 
    };

    self.initSearch = function () {
        self.finishedSearch = false;        
        self.search = true;
        self.reverseTransactions = [];
        self.reset();
        $scope.$apply();
    };
       
    self.openInitialDate = function() {
        self.initialDate.opened  = true;
    };
    
    self.openFinalDate = function() {
        self.finalDate.opened  = true;
    };
    
    self.openRegistrationDate = function() {
        self.registrationDate.opened  = true;
    };
    
    self.openBornDate = function() {
        self.bornDate.opened  = true;
    };
    
    self.getCountries = function (){
        var countries = {};
        for (var i = 0; i < $rootScope.countries.length; i++){
            countries[$rootScope.countries[i].idpaises] = $rootScope.countries[i].nombre;
        }
        return countries;
    };
    
    self.countries = self.getCountries();

}]);