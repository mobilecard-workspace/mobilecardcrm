/* global angular */

app.controller('AmericanExpressController', ['$rootScope', '$scope', '$routeParams', 'ClientService', 'CountryService',
    'ProviderService', 'BankService', 'CardService', 'ClientStatusService', 'FranchiseService', 'AmericanExpressService' , '$location', 
    '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 
    function($rootScope, $scope, $routeParams, ClientService, CountryService, ProviderService, BankService, CardService,
        ClientStatusService, FranchiseService, AmericanExpressService, $location, $window, DTOptionsBuilder, DTColumnDefBuilder) {            
    
    var self = this;  
 
    self.action = $routeParams.action;
    self.dateFormat = 'yyyy-MM-dd';
    self.reverseValidateRegex = "\\s*(\\d+\\s*)+";
    self.wait = true;
    self.showNonProcessedTable = false;
    self.showErrorMessage = false;
    self.showSuccessMessage = false;
    self.strReversedIds = '';
    self.transactions = [];
    self.nonProcessedDates = {
        initDate: ''
    };
    self.initialDate = {
    	opened: false
    };
    self.datePickerOptions = {
    	maxDate: new Date() //no fechas futuras
    };
    //settings tabla op. no procesadas.
    self.dataTable = {
    		dtInstance : {},
    		dtOptions: 
    			DTOptionsBuilder.newOptions()
				    .withPaginationType('full_numbers')
				    .withDisplayLength(10)
				    .withDOM('lBfrtip')
				    .withOption('order', [0, 'desc'])
				    .withOption('stateSave', true)
				    .withButtons([                                
				        'csv',
				        'excel',
				        {
				            text: 'Nueva Busqueda',
				            key: '3',
				            action: function (e, dt, node, config) {
				            	//reset form fecha transacciones no procesadas
				            	self.nonProcessedDates = {
				                        initDate: ''
				                 };
				                self.wait = false;
				                self.showNonProcessedTable = false;
				                self.showErrorMessage = false;
				                $scope.$apply();
				            }
				        }            
				    ]),
			dtColumnDefs: 
				[
			        DTColumnDefBuilder.newColumnDef(0),
			        DTColumnDefBuilder.newColumnDef(1),
			        DTColumnDefBuilder.newColumnDef(2),
			        DTColumnDefBuilder.newColumnDef(3),
			        DTColumnDefBuilder.newColumnDef(4),
			        DTColumnDefBuilder.newColumnDef(5),
			        DTColumnDefBuilder.newColumnDef(6),
			        DTColumnDefBuilder.newColumnDef(7),
			        DTColumnDefBuilder.newColumnDef(8),
			        DTColumnDefBuilder.newColumnDef(9),
			        DTColumnDefBuilder.newColumnDef(10),
			        DTColumnDefBuilder.newColumnDef(11),
			        DTColumnDefBuilder.newColumnDef(12),
			        DTColumnDefBuilder.newColumnDef(13),
			        DTColumnDefBuilder.newColumnDef(14),
			        DTColumnDefBuilder.newColumnDef(15),
			        DTColumnDefBuilder.newColumnDef(16),
			        DTColumnDefBuilder.newColumnDef(17),
			        DTColumnDefBuilder.newColumnDef(18)
			    ],
			    
    		transactions: []
    };
    	
    self.openInitialDate = function() {
        self.initialDate.opened  = true;
    };
    
    self.validateReversedId = function () {
        return (self.strReversedIds.trim() !== '');
    };
    
    self.getNonProcessedOperations = function(){
    	self.wait = true;
    	AmericanExpressService.nonProcessedOperations(self.nonProcessedDates.initDate)
    	.then(
    		function (data) {
	    		//carga datos exito
    			self.dataTable.transactions = data.data;
	    		self.showNonProcessedTable = true;
	    		self.showErrorMessage = false;
	    		self.wait = false;
    		},
    		function(errResponse){
    			//carga datos error
    			self.dataTable.transactions = [];
	    		self.showNonProcessedTable = false;
                self.showErrorMessage = true;
	    		self.wait = false;
            }
    	);
    };
    
    self.getReversedOperations = function(){
    	self.wait = true;
    	var idsArrStr = self.strReversedIds.split(/\s+/);
    	var idsArr = [];
    	var i;
    	for (i = 0; i < idsArrStr.length; ++i) {
    		idsArr.push(parseInt(idsArrStr[i]));
    	}
    	AmericanExpressService.reverseOperations(idsArr)
    	.then(
	    	function (data) {
	    		//reverso exito
	    		$scope.reversedIdsTxt = idsArrStr.join(",")
				self.showErrorMessage = false;
				self.showSuccessMessage = true;
	    		self.wait = false;
	    		self.strReversedIds = "";
	    	},
			function(errResponse){
				//reverso error
				self.showErrorMessage = true;
				self.showSuccessMessage = false;
	    		self.wait = false;
	        });
    };
    
    self.wait = false;
    
}]);