/* global angular */

app.controller('UserTagController', ['$rootScope', '$scope', 'UserTagService', '$location', 
    '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 
    function($rootScope, $scope, UserTagService, $location, $window, DTOptionsBuilder, DTColumnDefBuilder) {            
    
    var self = this;  
    self.totalUserTags = "";
    self.userTagsToShow = 500;
    self.lastPageLoaded = 0;
    self.pageDT = 0;
    self.userTags = [];
    self.finishedSearch = false;        
    self.search = true;
            
    self.getUserTagList = function(){
        self.wait = true;
        UserTagService.getUserTagList()        
        .then(
            function(d) {                
                self.userTags = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching userTags For List');
                self.showMessageError('Error while fetching userTags',errResponse);
            }
        );
    };
    
    self.getUserTagsByPage = function(numberPage, maxResults){
        self.wait = true;
        UserTagService.getUserTagsByPage(numberPage, maxResults)        
        .then(
            function(d) {                
                self.userTags = self.userTags.concat(d.data);
                self.lastPageLoaded = self.userTags.length;                
                self.wait = false;
                self.showPageInfo();
                self.pageDT = 0;
            },
            function(errResponse){
                console.error('Error while fetching userTags For List');
                self.showMessageError('Error while fetching userTags',errResponse);
            }
        );
    };
       
    self.getNumberOfUserTags = function(){
        self.wait = true;
        UserTagService.getNumberOfUserTags()        
        .then(
            function(numberOfUserTags) {                
                self.totalUserTags = numberOfUserTags.data;
                if (self.totalUserTags <= 500){
                    self.getUserTagList();
                }else {                    
                    self.getUserTagsByPage(self.lastPageLoaded, self.userTagsToShow);
                }                
            },
            function(errResponse){
                console.error('Error while fetching userTags For List');
                self.showMessageError('Error while fetching userTags',errResponse);
            }
        );
    };
    
    self.reset = function(){                                                           
                    
    };           
    
    self.userTag = {
        id_usuario: "", 
        id_tiporecargatag: "",
        etiqueta: "", 
        tag: "", 
        dv: "",
        id: ""
    };

    self.userTagSearch = {
        id_usuario: "",
        tag: ""      
    };
    
    self.validateTagSearch = function () {
        if (self.userTagSearch.id_usuario !== ""){
            return false;
        }else if (self.userTagSearch.tag !== ""){
            return false;
        }
        
        return true;
    };
    
    self.reset = function () {
        self.tag = {
            id_usuario: "", 
            id_tiporecargatag: "",
            etiqueta: "", 
            tag: "", 
            dv: "",
            id: ""
        };

        self.userTagSearch = {
            id_usuario: "",
            tag: ""      
        };
    };
    
    self.dtUserTagOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withDOM('lBfrtip')
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true)
            .withButtons([                                
                {
                    text: 'Nueva Busqueda',
                    key: '3',
                    action: function (e, dt, node, config) {
                        self.initSearch();
                    }
                }
            ]);            
            
    self.dtUserTagColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5)
    ];
    
    self.dtUserTagInstance = {};

    self.initSearch = function () {
        self.finishedSearch = false;        
        self.search = true;
        self.userTags = [];
        self.reset();
        $scope.$apply();
    };
    
    self.searchTagByParameters = function(){
        self.wait = true;
        self.finishedSearch = false;
        self.search = false;
        if (self.userTagSearch.id_usuario === ""){
            self.userTagSearch.id_usuario = -1;
        }        
        UserTagService.searchTags(self.userTagSearch)        
        .then(
            function(d) {                
                self.userTags = d.data;                
                self.finishedSearch = true;
                self.search = false;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching userTags For List');
                self.showMessageError('Error while fetching userTags',errResponse);
            }
        );
    };
    
    self.initTagModalData = function (userTag) {
        self.userTag = userTag;
        self.detailUserTag = true; 
    };    
    
    self.getCountries = function (){
        var countries = {};
        for (var i = 0; i < $rootScope.countries.length; i++){
            countries[$rootScope.countries[i].idpaises] = $rootScope.countries[i].nombre;
        }
        return countries;
    };
    
    self.countries = self.getCountries();

}]);