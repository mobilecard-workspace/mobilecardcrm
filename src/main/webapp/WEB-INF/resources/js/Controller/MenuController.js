/* global app */

app.controller('MenuController', ['$rootScope', '$scope', '$location', '$window',
    '$route', 'CountryService', 'UserService', 
    function($rootScope, $scope, $location, $window, $route, CountryService, UserService) {            
    
    var self = this;            
            
    self.menuClass = "toggled";
    self.clientCollapse = true;
    self.wait = true;
    self.userModules = [];
    
    self.menuToggle = function(){
        if (self.menuClass === ""){
            self.menuClass = "toggled";
        }else{
            self.menuClass = "";
        }
    };
    
    self.menu = {
        clients: {
            active: false
        },
        settings: {
            active: false
        },
         americanExpress: {
        	active: false
        }
    };
    
    self.getCountryList = function(){
        self.wait = true;
        CountryService.getCountryList()        
        .then(
            function(d) {                
                $rootScope.countries = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching countries List');
                self.showMessageError('Error while fetching countries',errResponse);
            }
        );
    };
    
    self.getPermissionList = function(){
        self.wait = true;
        UserService.getPermissionList()        
        .then(
            function(d) {   
            	self.permission = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching User Permission');
                //self.showMessageError('Error while fetching roles',errResponse);
            }
        );
    };
    
    self.getModulesList = function(){
        self.wait = true;
        UserService.getModulesList()        
        .then(
            function(d) {                
                $rootScope.modules = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching User Permission');
                //self.showMessageError('Error while fetching roles',errResponse);
            }
        );
    };
    
    self.getUserModules = function(){
        self.wait = true;
        UserService.getUserModulesList()        
        .then(
            function(d) {                
                self.userModules = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching User Permission');
                //self.showMessageError('Error while fetching roles',errResponse);
            }
        );
    };
    
    self.getUserModules();
    self.getModulesList();
    self.getCountryList();
    self.getPermissionList();
    
    self.showModuleUser = function (id_module){
        for (var i = 0; i < self.userModules.length; i++){
            if (self.userModules[i].id_module === id_module){
                return true;
            }
        }
        return false;
    };
    
    self.showClientView = function (action){
        $location.path('/clientes/' + action);     
        if ($location.path() === '/clientes/' + action){
            $route.reload();
        }
    };
    
    self.showTransactionView = function (){        
        $location.path('/transacciones');               
        if ($location.path() === '/transacciones'){
            $route.reload();
        }
    };
    
    self.showReverseView = function (){
        $location.path('/reversos');     
        if ($location.path() === '/reversos'){
            $route.reload();
        }
    };
    
    self.showLocksView = function (){
        $location.path('/bloqueos');     
        if ($location.path() === '/bloqueos'){
            $route.reload();
        }
    };
    
    self.showTagsView = function (){
        $location.path('/tags');     
        if ($location.path() === '/tags'){
            $route.reload();
        }
    };
    
    self.showProvidersView = function (){
        $location.path('/proveedores');     
        if ($location.path() === '/proveedores'){
            $route.reload();
        }
    };
    
    self.showUsersView = function (){
        $location.path('/usuarios');     
        if ($location.path() === '/usuarios'){
            $route.reload();
        }
    };
    
    self.showConciliationView = function (){
        $location.path('/conciliacion');     
        if ($location.path() === '/conciliacion'){
            $route.reload();
        }
    };
    
    self.showHomeView = function (){
        $location.path('/home');     
        if ($location.path() === '/home'){
            $route.reload();
        }
    };    
    
    self.showAmericanExpressView = function (action){
        $location.path('/american_express/' + action);     
        if ($location.path() === '/american_express/' + action){
            $route.reload();
        }
    };
    
    $rootScope.transactionStatus = {

        200: {
            longMessage: "Indica que la transacción es segura y se puede enviar a Payworks.",
            shortMessage: "Transacción segura"
        },
        201 : {
            longMessage: "Se detecto un error general en el sistema de Visa o Master Card, favor de esperar unos momentos para reintentar la transacción.",
            shortMessage: "Error general"
        },
        421 : {
            longMessage: "El servicio 3D Secure no está disponible, favor de esperar unos momentos para reintentar la transacción.",
            shortMessage: "Servicio no disponible"
        },
        422 : {
            longMessage: "Se produjo un problema genérico al momento de realizar la Autenticación.",
            shortMessage: "Error de autenticación"
        },
        423 : {
            longMessage: "La Autenticación de la Tarjeta no fue exitosa.",
            shortMessage: "Error de autenticación"
        },
        424 : {
            longMessage: "Autenticación 3D Secure no fue completada, no está ingresando correctamente la contraseña 3D Secure.",
            shortMessage: "Error de autenticación"
        },
        425 : {
            longMessage: "Autenticación Inválida, no está ingresando correctamente la contraseña3D Secure.",
            shortMessage: "Error de autenticación"
        },
        430 : {
            longMessage: "Tarjeta de Crédito nulo, la numero de Tarjeta se envió vacía.",
            shortMessage: "Dato inválido"
        },
        431 : {
            longMessage: "Fecha de expiración nulo, la fecha de expricacion se envió vacía.",
            shortMessage: "Dato inválido"
        },
        432 : {
            longMessage: "Monto nulo, la variable Total se envió vacía.",
            shortMessage: "Dato inválido"
        },
        433 : {
            longMessage: "Id del comercio nulo, la variable MerchantId se envió vacía.",
            shortMessage: "Dato inválido"
        },
        434 : {
            longMessage: "Liga de retorno nula, la variable ForwardPath se envió vacía.",
            shortMessage: "Dato inválido"
        },
        435 : {
            longMessage: "Nombre del comercio nulo, la variable MerchantName se envió vacía.",
            shortMessage: "Dato inválido"
        },
        436 : {
            longMessage: "Formato de TC incorrecto, la variable Card debe ser de 16 dígitos.",
            shortMessage: "Dato inválido"
        },
        437 : {
            longMessage: "Formato de Fecha de Expiración incorrecto, la variable Expires debe tener el siguiente formato: YY/MM donde YY se refiere al año y MM se refiere al mes de vencimiento de la tarjeta.",
            shortMessage: "Dato inválido"
        },
        438 : {
            longMessage: "Fecha de Expiración incorrecto, indica que el plástico esta vencido.",
            shortMessage: "Dato inválido"
        },
        439 : {
            longMessage: "Monto incorrecto, la variable Total debe ser un número menor a 999,999,999,999.## con la fracción decimal opcional, esta debe ser a lo más de 2 décimas.",
            shortMessage: "Dato inválido"
        },
        440 : {
            longMessage: "Formato de nombre del comercio incorrecto, debe ser una cadena de máximo 25 caracteres alfanuméricos.",
            shortMessage: "Dato inválido"
        }                                   
    };    
    
}]);