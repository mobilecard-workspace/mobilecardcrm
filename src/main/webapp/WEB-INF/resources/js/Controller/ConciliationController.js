/* global app */

app.controller('ConciliationController', ['$rootScope', '$scope', '$location', '$window', 'uibDateParser',
    '$route', 'ConciliationService', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'FileSaver', 'Blob', '$compile',
    function($rootScope, $scope, $location, $window, uibDateParser, $route, ConciliationService, 
    DTOptionsBuilder, DTColumnDefBuilder, FileSaver, Blob, $compile) {            
    
    var self = this;            
    
    self.wait = false;
    self.search = true;
    self.finishedSearch = false;
    self.accountStatus = [];
    self.showAmexConciliationSummary = false;
    self.amexConciliationSummary = "";
    
    self.accountStatusDates = {
        initDate: "",
        endDate: ""
    };
    
    self.amexConciliationDate = "";
    
    self.fileData = {
        date: "",
        type: ""
    };
    
    self.reset = function (){
        self.accountStatusDates = {
            initDate: "",
            endDate: ""
        };
        
        self.fileData = {
            date: "",
            type: ""
        };
        
    };
        
    self.initSearch = function (){
        self.reset();
        self.wait = false;
        self.search = true;
        self.finishedSearch = false;
        self.accountStatus = [];
        $scope.$apply();
    };    
    
    self.dateFormat = 'yyyy-MM-dd';
    
    self.initialDate = {
        opened: false
    };
    
    self.finalDate = {
        opened: false
    };
    
    self.fileDate = {
        opened: false
    };
    
    self.amexConciliationDateField = {
        opened: false
    };
    
    self.openInitialDate = function() {
        self.initialDate.opened  = true;
    };
    
    self.openFinalDate = function() {
        self.finalDate.opened  = true;
    };
    
    self.openFileDate = function() {
        self.fileDate.opened  = true;
    };
    
    self.openAmexConciliationDate = function () {
        self.amexConciliationDateField.opened = true;
    };
    
    self.validateAccountStatusDates = function () {
        if (self.accountStatusDates.initDate !== "" && self.accountStatusDates.endDate !== ""){
            return false;
        }       
        return true;
    };
    
    self.validateAmexConciliationDate = function () {
        if (self.amexConciliationDate !== ""){
            return false;
        }       
        return true;
    };
    
    self.validateCreateFileData = function () {
        if (self.fileData.date !== "" && self.fileData.type !== ""){
            return false;
        }      
        return true;
    };
    
    self.getAccountStatus = function(){
        self.wait = true;
        ConciliationService.getAccountStatus(self.accountStatusDates.initDate, self.accountStatusDates.endDate)        
        .then(
            function(d) {                
                self.accountStatus = d.data;
                self.finishedSearch = true;
                self.search = false;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching accountStatus List');
                self.showMessageError('Error while fetching accountStatus',errResponse);
                self.wait = false;
                self.search = true;
                self.finishedSearch = false;
            }
        );
    };        
    
    self.createFile = function(){
        self.wait = true;               
        var filename = self.fileData.type + "_";
        var fullYear = self.fileData.date.getFullYear();
        var month = self.fileData.date.getMonth() + 1;
        var date = self.fileData.date.getDate();
        if (date < 10){
            filename = filename + "0" + date; 
        }else{
            filename = filename + "" + date; 
        }        
        if (month < 10){
            filename = filename + "0" + month; 
        }else{
            filename = filename + "" + month; 
        }
        filename = filename + "" + fullYear;        
        ConciliationService.createFile(self.fileData.date, self.fileData.type)        
        .then(
            function(d) {
                self.download(d.data, filename);
                self.wait = false;                
            },
            function(errResponse){
                self.wait = false;
                console.error('Error while fetching accountStatus List');
                self.showMessageError('Error while fetching accountStatus',errResponse);                
            }
        );
    };
    
    self.download = function(text, filename) {
        var data = new Blob([text], { type: 'text/plain;charset=utf-8' });
        FileSaver.saveAs(data, filename);
    };
    
    self.getAmexConciliation = function(){ 
        self.wait = true;
        ConciliationService.getAmexConciliation(self.amexConciliationDate)        
        .then(
            function(d) {                                                              
                self.amexConciliationSummary = d.data;
                self.showAmexConciliationSummary = true;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching amexTransactions List');
                //self.showMessageError('Error while fetching amexTransactions',errResponse);
            }
        );
    };              
    
    self.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDisplayLength(10)
        .withDOM('lBfrtip')
        .withOption('order', [0, 'desc'])
        .withOption('stateSave', true)
        .withButtons([                                
            //'pdf',
            'csv',
            'excel',
            {
                text: 'Nueva Busqueda',
                key: '3',
                action: function (e, dt, node, config) {
                    self.initSearch();
                }
            }            
        ]);           
            
    self.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8),
        DTColumnDefBuilder.newColumnDef(9),
        DTColumnDefBuilder.newColumnDef(10),
        DTColumnDefBuilder.newColumnDef(11),
        //DTColumnDefBuilder.newColumnDef(12),
        DTColumnDefBuilder.newColumnDef(12),
        DTColumnDefBuilder.newColumnDef(13)
    ];
    
    self.dtInstance = {};
    
    self.getNumAutorizacionTAE = function (entry){
        if (entry.num_TarjetaIAVE_autorizacion !== null && entry.num_TarjetaIAVE_autorizacion !== ""){
            return entry.num_TarjetaIAVE_autorizacion;
        }else{
            return entry.num_TarjetaIAVE_autorizacion_1;
        }
    };
    
    self.dtAmexConciliationSummaryOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDisplayLength(10)
        .withDOM('lBfrtip')
        .withOption('order', [0, 'desc'])
        .withOption('stateSave', true)
        .withButtons([                                
            'pdf',            
            {
                text: 'Regresar',
                key: '3',
                action: function (e, dt, node, config) {
                    self.initAmexConciliation();
                }
            }            
        ]);           
            
    self.dtAmexConciliationSummaryColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2)
    ];
    
    self.dtAmexConciliationSummaryInstance = {};
    
    self.initAmexConciliation = function () {
        self.showAmexConciliationSummary = false;
        self.amexConciliationSummary = "";
        $scope.$apply();
    };
    
}]);