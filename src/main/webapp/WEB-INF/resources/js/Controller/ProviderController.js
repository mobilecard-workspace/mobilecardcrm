/* global angular */

app.controller('ProviderController', ['$rootScope', '$scope', 'ProviderService', 'CountryService',
    '$location', '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 
    function($rootScope, $scope, ProviderService, CountryService, 
        $location, $window, DTOptionsBuilder, DTColumnDefBuilder) {            
    
    var self = this;  
    self.totalProviders = "";
    self.providersToShow = 500;
    self.lastPageLoaded = 0;
    self.pageDT = 0;
    self.providers = [];
    self.detailProvider = false;
    self.editingProvider = false; 
    self.creatingProvider = false;
            
    self.provider = {
        id_proveedor: "",
        prv_num_cliente: "",
        prv_pos_id: "",
        prv_nombre_comercio: "",
        prv_domicilio: "",
        prv_dealer_login: "",
        prv_dealer_password: "",
        prv_pos_login: "",
        prv_pos_password: "",
        prv_nombre_completo: "",
        prv_clave_personal: "",
        prv_status: 1,
        prv_claveWS: "",
        prv_path: "",
        id_categoria: "",
        comision: "",
        compatible_Amex: "",
        compatible_Visa: "",
        comision_porcentaje: "",
        min_comision_porcentaje: "",
        min_comision_fija: "",
        AFILIACION: "",
        tipo_cambio: "",
        MAX_TRANSACCION: ""
    };        
    
    self.getProviderList = function(){
        self.wait = true;
        ProviderService.getProviderList()        
        .then(
            function(d) {                
                self.providers = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
    
    self.getProviderStatusList = function(){
        self.wait = true;
        ProviderService.getProviderStatusList()        
        .then(
            function(d) {                
                self.providerStatuses = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching Provider Status For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
    
    self.getProvidersByPage = function(numberPage, maxResults){
        self.wait = true;
        ProviderService.getProvidersByPage(numberPage, maxResults)        
        .then(
            function(d) {                
                self.providers = self.providers.concat(d.data);
                self.lastPageLoaded = self.providers.length;                
                self.wait = false;
                self.showPageInfo();
                self.pageDT = 0;
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
      
    self.editProvider = function () {
        self.wait = true;
        ProviderService.editProvider(self.provider)        
        .then(
            function(result) {                
                console.log("Provider updated successful",result);
                self.providers = result.data;
                self.wait = false;
                self.creatingProvider = false;
            },
            function(errResponse){
                console.error('Error while fetching provider');
//                self.showMessageError('Error while fetching Provider',errResponse);
                self.wait = false;
                self.creatingProvider = false;
            }
        );
    };
    
    self.addProvider = function () {
        self.wait = true;
        ProviderService.addProvider(self.provider)        
        .then(
            function(result) {                
                console.log("Provider added successful",result);
                self.providers = result.data;
                self.wait = false;
                self.creatingProvider = false;
            },
            function(errResponse){
                console.error('Error while fetching provider');
//                self.showMessageError('Error while fetching Provider',errResponse);
                self.wait = false;
                self.creatingProvider = false;
            }
        );
    };    
    
    self.getNumberOfProviders = function(){
        self.wait = true;
        ProviderService.getNumberOfProviders()        
        .then(
            function(numberOfProviders) {                
                self.totalProviders = numberOfProviders.data;
                if (self.totalProviders <= 500){
                    self.getProviderList();
                }else {                    
                    self.getProvidersByPage(self.lastPageLoaded, self.providersToShow);
                }                
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
    
    self.reset = function(){                                                           
        self.provider = {
            id_proveedor: "",
            prv_num_cliente: "",
            prv_pos_id: "",
            prv_nombre_comercio: "",
            prv_domicilio: "",
            prv_dealer_login: "",
            prv_dealer_password: "",
            prv_pos_login: "",
            prv_pos_password: "",
            prv_nombre_completo: "",
            prv_clave_personal: "",
            prv_status: 1,
            prv_claveWS: "",
            prv_path: "",
            id_categoria: "",
            comision: "",
            compatible_Amex: "",
            compatible_Visa: "",
            comision_porcentaje: "",
            min_comision_porcentaje: "",
            min_comision_fija: "",
            AFILIACION: "",
            tipo_cambio: "",
            MAX_TRANSACCION: ""
        };
    };           
           
    self.getProviderList();
    self.getProviderStatusList();
    
    self.dtProviderOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withDOM('lfrtip')
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true);            
            
    self.dtProviderColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2)
    ];
    
    self.dtProviderInstance = {};

    self.initProviderModalData = function (provider, type) {
        switch (type) {
            case "detail":
                self.provider = provider;
                self.detailProvider = true;
                self.editingProvider = false;
                self.creatingProvider = false;
                break;
            case "edit":
                self.provider = provider;
                self.detailProvider = false;
                self.editingProvider = true;
                self.creatingProvider = false;                
                break;            
            case "create":
                self.detailProvider = false;
                self.editingProvider = false;
                self.creatingProvider = true;                
                break;
        };        
    };            
    
    self.getCountryList = function(){
        self.wait = true;
        CountryService.getCountryList()        
        .then(
            function(d) {                
                self.countries = self.formatCountries(d.data);
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching countries List');
                self.showMessageError('Error while fetching countries',errResponse);
            }
        );
    };
    
    self.getCountryList();
    
    self.formatCountries = function (data){
        var countries = {};
        for (var i = 0; i < data.length; i++){
            countries[data[i].idpaises] = data[i].nombre;
        }
        return countries;
    };

}]);