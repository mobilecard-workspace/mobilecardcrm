'use strict';
 
app.factory('BankService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getBankList: function(){
            return  $http({
                url: "admin/banco/list",
                method: 'GET'                
            });            
        },
        
        getBanksByPage: function(pageNumber, maxResults){
            return $http.post('admin/banco/list', [pageNumber, maxResults] );                       
        },
        
        getNumberOfBanks: function(){
            return  $http({
                url: "admin/banco/total",
                method: 'POST'                
            });            
        }
    };
}]);
