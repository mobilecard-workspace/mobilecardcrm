'use strict';
 
app.factory('UserTagService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getUserTagList: function(){
            return  $http({
                url: "admin/tag/list",
                method: 'GET'                
            });            
        },
        
        getUserTagsByPage: function(pageNumber, maxResults){
            return $http.post('admin/tag/list', [pageNumber, maxResults] );                       
        },
        
        getNumberOfUserTags: function(){
            return  $http({
                url: "admin/tag/total",
                method: 'POST'                
            });            
        },
        
        searchTags: function(userTagSearch){
            return $http.post('admin/tag/searchTags', userTagSearch );                       
        }
    };
}]);
