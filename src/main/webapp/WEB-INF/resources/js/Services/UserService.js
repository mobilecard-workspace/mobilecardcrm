'use strict';
 
app.factory('UserService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getUserList: function(){
            return  $http({
                url: "admin/usuario/list",
                method: 'GET'                
            });            
        },
        
        getPermissionList: function(){
            return  $http({
                url: "admin/usuario/permission",
                method: 'GET'                
            });            
        },
        
        getModulesList: function(){
            return  $http({
                url: "admin/usuario/modules",
                method: 'GET'                
            });            
        },
        
        getUserModulesList: function(){
            return  $http({
                url: "admin/usuario/permission",
                method: 'GET'                
            });            
        },
        
        editUser: function (user) {
            return $http.post('admin/usuario/edit',  user);                       
        },
        
        addUser: function (user, permission) {
            return $http.post('admin/usuario/add', user);                       
        }                    
    };
}]);


