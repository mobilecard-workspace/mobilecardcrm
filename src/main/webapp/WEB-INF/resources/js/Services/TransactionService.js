'use strict';
 
app.factory('TransactionService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getTransactionList: function(){
            return  $http({
                url: "admin/transaccion/list",
                method: 'GET'                
            });            
        },
        
        getAvailableOptions: function(){
            return  $http({
                url: "admin/transaccion/list_products",
                method: 'GET'                
            });            
        },
        
        getTransactionsByPage: function(pageNumber, maxResults){
            return $http.post('admin/transaccion/list', [pageNumber, maxResults] );                       
        },
        
        getNumberOfTransactions: function(){
            return  $http({
                url: "admin/transaccion/total",
                method: 'POST'                
            });            
        },
        searchTransactions: function(transactionSearch){
            return $http.post('admin/transaccion/searchTransactions', transactionSearch );                       
        }
    };
}]);


