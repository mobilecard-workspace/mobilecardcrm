'use strict';
 
app.factory('ProviderService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getProviderList: function(){
            return  $http({
                url: "admin/provider/list",
                method: 'GET'                
            });            
        },
        
        getProvidersByPage: function(pageNumber, maxResults){
            return $http.post('admin/provider/list', [pageNumber, maxResults] );                       
        },
        
        getProviderById: function(id){
            return $http.post('admin/provider/providerbyid', id );                       
        },

        
        getNumberOfProviders: function(){
            return  $http({
                url: "admin/provider/total",
                method: 'POST'                
            });            
        },
        
        editProvider: function (provider) {
            return $http.post('admin/provider/edit', provider );                       
        },
        
        addProvider: function (provider) {
            return $http.post('admin/provider/add', provider );                       
        },
        
        getProviderStatusList: function(){
            return  $http({
                url: "admin/provider/listProviderStatuses",
                method: 'GET'                
            });            
        }
    };
}]);
