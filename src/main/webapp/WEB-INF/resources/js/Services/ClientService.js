'use strict';
 
app.factory('ClientService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getClientList: function(){
            return  $http({
                url: "admin/cliente/list",
                method: 'GET'                
            });            
        },
        
        getClientsByPage: function(pageNumber, maxResults){
            return $http.post('admin/cliente/list', [pageNumber, maxResults] );                       
        },
        
        getClientByImei: function(imei){
            return $http.post('admin/cliente/clientbyimei', imei );                       
        },

        getClientByCard: function(tarjeta){
            return $http.post('admin/cliente/clientbycard', tarjeta );                       
        },
        
        getClientById: function(id){
            return $http.post('admin/cliente/clientbyid', id );                       
        },
        
        editClient: function (client) {
            return $http.post('admin/cliente/edit', client );                       
        },
        
        addClient: function (client) {
            return $http.post('admin/cliente/add', client );                       
        },
        
        getNumberOfClients: function(){
            return  $http({
                url: "admin/cliente/total",
                method: 'POST'                
            });            
        },
        
        searchClients: function(clientSearch){
            return $http.post('admin/cliente/searchClients', clientSearch );                       
        },
        
        getClientCards: function (id_usuario) {
            return $http.post('admin/cliente/listClientCards', id_usuario);                       
        },
        
        addOrUpdateClientCard: function (clientCard) {
            return $http.post('admin/cliente/addClientCard', clientCard );                       
        },
        
        blockClient: function (client) {
            return $http.post('admin/cliente/blockClient', client );                       
        }
                
    };
}]);
