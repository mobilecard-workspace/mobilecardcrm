'use strict';
 
app.factory('CountryService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getCountryList: function(){
            return  $http({
                url: "admin/pais/list",
                method: 'GET'                
            });            
        },
        
        getCountriesByPage: function(pageNumber, maxResults){
            return $http.post('admin/pais/list', [pageNumber, maxResults] );                       
        },
        
        getNumberOfCountries: function(){
            return  $http({
                url: "admin/pais/total",
                method: 'POST'                
            });            
        }
    };
}]);
