'use strict';
 
app.factory('LocksService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getLockedCardList: function(){
            return  $http({
                url: "admin/bloqueo/listLockedCard",
                method: 'GET'                
            });            
        },
        
        getLockedCardsByPage: function(pageNumber, maxResults){
            return $http.post('admin/bloqueo/listLockedCard', [pageNumber, maxResults] );                       
        },
        
        getNumberOfLockedCards: function(){
            return  $http({
                url: "admin/bloqueo/totalLockedCard",
                method: 'POST'                
            });            
        },
        
        getLockedImeiList: function(){
            return  $http({
                url: "admin/bloqueo/listLockedImei",
                method: 'GET'                
            });            
        },
        
        getLockedImeisByPage: function(pageNumber, maxResults){
            return $http.post('admin/bloqueo/listLockedImei', [pageNumber, maxResults] );                       
        },
        
        getNumberOfLockedImeis: function(){
            return  $http({
                url: "admin/bloqueo/totalLockedImei",
                method: 'POST'                
            });            
        },
        getLockedCard: function(numcard){
            return $http.post('admin/bloqueo/searchLockedCard',numcard );                       
        },
        getLockedImei: function(numImei){
            return $http.post('admin/bloqueo/searchLockedImei',numImei );                       
        },
        
        updateLockedCard: function(lockedCard){
            return $http.post('admin/bloqueo/updateLockedCard',lockedCard );                       
        },
        
        updateLockedImei: function(lockedImei){
            return $http.post('admin/bloqueo/updateLockedImei',lockedImei );                       
        },
        
        addLockedCard: function(lockedCard){
            return $http.post('admin/bloqueo/addLockedCard',lockedCard );                       
        },
        
        addLockedImei: function(lockedImei){
            return $http.post('admin/bloqueo/addLockedImei',lockedImei );                       
        }
    };
}]);
