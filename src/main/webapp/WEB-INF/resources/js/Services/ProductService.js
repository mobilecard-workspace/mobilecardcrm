'use strict';
 
app.factory('ProductService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getProductList: function(){
            return  $http({
                url: "admin/transaccion/productList",
                method: 'GET'                
            });            
        }
    };
}]);
