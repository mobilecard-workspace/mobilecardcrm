'use strict';
 
app.factory('AmericanExpressService', ['$http', '$q','$location', '$resource', function($http, $q, $location, $resource){
 
    return {                                                      
        
    	nonProcessedOperations: function(processDate){
    		  return $http.post('admin/american_express/services/non_processed_operations', processDate );                       
        },
        
        reverseOperations: function(ids){
            return $http.put('admin/american_express/services/reverse_operations', ids);                       
        }                        
    };
}]);
