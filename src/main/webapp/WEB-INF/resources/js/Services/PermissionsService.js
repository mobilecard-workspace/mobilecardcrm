'use strict';
 
app.factory('PermissionsService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                

        getPermission: function(idPermission){
            return $http.post('addOrUpdatePermission/', idPermission);                        
        },
        
        getPermissionsList: function(){
            return  $http({
                url: "permissionList",
                method: 'GET'                
            });            
        },        
       
        addOrUpdatePermission: function(permission){
            return $http.post('addOrUpdatePermission/', permission);            
        },
        
        deletePermission: function(permission){
            return $http.post('deletePermission/', permission);
        },
        
        getUserPermissionsList: function(){
            return  $http({
                url: "userPermissions",
                method: 'GET'                
            });            
        }
         
    };
 
}]);
