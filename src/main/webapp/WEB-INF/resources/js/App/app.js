var app = angular.module('MyApp', ['ngRoute','ngCookies', 'ngMaterial', 'datatables',
    'ui.bootstrap', 'datatables.buttons', 'ngFileSaver', 'ngResource', 'ngSanitize']);


app.config(function($routeProvider, $httpProvider, $compileProvider) {
	var baseUrl = '/MobileCardCRM';

	$routeProvider
    	.when('/', 
    	    {templateUrl : baseUrl  + '/admin/home/'})
    	.when('/clientes/:action', 
    		{templateUrl : baseUrl  + '/admin/cliente/'})
    	.when('/usuarios', 
    		{templateUrl : baseUrl  + '/admin/usuario/'})
    	.when('/conciliacion', 
    		{templateUrl : baseUrl  + '/admin/conciliacion/'})
    	.when('/transacciones', 
    		{templateUrl : baseUrl  + '/admin/transaccion/'})
    	.when('/reversos', 
    		{templateUrl : baseUrl  + '/admin/reverso/'})
    	.when('/bloqueos', 
    		{templateUrl : baseUrl  + '/admin/bloqueo/'}) 
    	.when('/tags', 
    		{templateUrl : baseUrl  + '/admin/tag/'})
    	.when('/proveedores', 
    		{templateUrl : baseUrl  + '/admin/provider/'})
    	.when('/connectionLost', 
    		{templateUrl : 'connectionLost'})
    	.when('/american_express/:action', 
    		{templateUrl : baseUrl  + '/admin/american_express/'})
    	.otherwise('/');

    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
});

app.filter('lockStatusFormat', function() {
    return function(x) {
        var txt = "";
        if (x == 0){
            txt = "Activo";
        }else if (x == 1) {
            txt = "Bloqueado";
        }
        return txt;
    };
});

app.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.directive('compileHtml', compileHtml);

function compileHtml($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.compileHtml);
            }, function (value) {
                element.html(value);
                $compile(element.contents())(scope);
            });
        }
    };
}

