package com.addcel.mx.mobilecardcrm.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.addcel.mx.mobilecardcrm.model.Role;
import com.addcel.mx.mobilecardcrm.model.User;
import com.addcel.mx.mobilecardcrm.model.UserForAuth;
import com.addcel.mx.mobilecardcrm.service.RoleService;
import com.addcel.mx.mobilecardcrm.service.RoleServiceImpl;
import com.addcel.mx.mobilecardcrm.service.UserService;
import com.addcel.mx.mobilecardcrm.service.UserServiceImpl;

public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private HttpServletRequest request;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getName();
		String password = (String) authentication.getCredentials();
		UserForAuth userForAuth = new UserForAuth();
		userForAuth.setUsername(username);
		User user = null;
		List<GrantedAuthority> authorities = null;

		user = userService.authenticateUser(username, password);

		userForAuth.setId(user.getId());
		authorities = new ArrayList<>();
		List<Role> userRoles = user.getRoles();
		if (userRoles != null) {
			for (Role userRole : userRoles) {
				System.out.println("Role " + userRole.getAuthority());
				authorities.add(new SimpleGrantedAuthority(userRole.getAuthority()));
			}
		}
		userForAuth.setAuthorities(authorities);

		return new MyAuthentication(userForAuth, password, authorities);
	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
