package com.addcel.mx.mobilecardcrm.security;

import java.util.Collection;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author wsolano
 */
public class MyAuthentication extends UsernamePasswordAuthenticationToken {

    public MyAuthentication(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public MyAuthentication(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }    

}
