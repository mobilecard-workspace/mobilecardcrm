/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Country;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("countryDao")
@Transactional
public class CountryDaoImpl implements CountryDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addCountry(Country country) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.persist(country);
    }

    @Override
    public void updateCountry(Country country) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.update(country);
    }

    @Override
    public List<Country> listCountries() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Country.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List countries = cr.list();
        
        return countries;
    }

    @Override
    public Country getCountryById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(Country.class);
        Criterion id_criterion = Restrictions.eq("idPaises", id);
        criteria.add(id_criterion);
        
        Country country = (Country) criteria.list();
        
        return country;
    }

    @Override
    public void removeCountry(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<Country> getPageCountries(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Country.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List countries = cr.list();
        
        return countries;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(Country.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }
    
}
