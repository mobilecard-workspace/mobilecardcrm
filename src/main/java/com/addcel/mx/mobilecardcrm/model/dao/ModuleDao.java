package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Module;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface ModuleDao {
    
    public void addModule(Module Module);
    public void updateModule(Module Module);
    public List<Module> listModules();
    public Module getModuleById(int id);
    public void removeModule(int id);
}
