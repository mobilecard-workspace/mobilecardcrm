package com.addcel.mx.mobilecardcrm.model.admin;

/**
 *
 * @author Lasar-Soporte
 */
public class ReverseTransactionSearch {
    
    private String fechaHora_inicial;
    private String fechaHora_final;
    private String usr_nombre;
    private String usr_apellido;

    public String getFechaHora_inicial() {
        return fechaHora_inicial;
    }

    public void setFechaHora_inicial(String fechaHora_inicial) {
        this.fechaHora_inicial = fechaHora_inicial;
    }

    public String getFechaHora_final() {
        return fechaHora_final;
    }

    public void setFechaHora_final(String fechaHora_final) {
        this.fechaHora_final = fechaHora_final;
    }

    public String getUsr_nombre() {
        return usr_nombre;
    }

    public void setUsr_nombre(String usr_nombre) {
        this.usr_nombre = usr_nombre;
    }

    public String getUsr_apellido() {
        return usr_apellido;
    }

    public void setUsr_apellido(String usr_apellido) {
        this.usr_apellido = usr_apellido;
    }        
}
