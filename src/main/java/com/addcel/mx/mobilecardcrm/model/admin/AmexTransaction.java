package com.addcel.mx.mobilecardcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "transaccion")
public class AmexTransaction {
    
    @Id
    @Column(name="transaccionIdn")    
    private Long transaccionIdn;
    
    private String transaccionId;
    private String transaccionLocalDateTime;
    private String transaccionDsc;
    private String transaccionUsuario;
    private String transaccionTarjeta;
    private String transaccionExpira;
    private String transaccionMonto;
    private String transaccionCid;
    private String transaccionNombres;
    private String transaccionApellidos;
    private String transaccionDireccion;
    private String transaccionTelefono;
    private String transaccionEmail;
    private String transaccionCp;
    private String MensajeId;
    private String transaccionTipo;
    private String transaccionSubTipo;
    private String transaccionReversal;
    private String transaccionReversalIdn;
    private String transaccionAccReference;
    private String transaccionApprovalCode;
    private String transaccionDate;
    private String transaccionProcesada;
    private String transaccionTipoTarjeta;
    private String transaccionAcceptorId;
    private int plazo;

    public Long getTransaccionIdn() {
        return transaccionIdn;
    }

    public void setTransaccionIdn(Long transaccionIdn) {
        this.transaccionIdn = transaccionIdn;
    }

    public String getTransaccionId() {
        return transaccionId;
    }

    public void setTransaccionId(String transaccionId) {
        this.transaccionId = transaccionId;
    }

    public String getTransaccionLocalDateTime() {
        return transaccionLocalDateTime;
    }

    public void setTransaccionLocalDateTime(String transaccionLocalDateTime) {
        this.transaccionLocalDateTime = transaccionLocalDateTime;
    }

    public String getTransaccionDsc() {
        return transaccionDsc;
    }

    public void setTransaccionDsc(String transaccionDsc) {
        this.transaccionDsc = transaccionDsc;
    }

    public String getTransaccionUsuario() {
        return transaccionUsuario;
    }

    public void setTransaccionUsuario(String transaccionUsuario) {
        this.transaccionUsuario = transaccionUsuario;
    }

    public String getTransaccionTarjeta() {
        return transaccionTarjeta;
    }

    public void setTransaccionTarjeta(String transaccionTarjeta) {
        this.transaccionTarjeta = transaccionTarjeta;
    }

    public String getTransaccionExpira() {
        return transaccionExpira;
    }

    public void setTransaccionExpira(String transaccionExpira) {
        this.transaccionExpira = transaccionExpira;
    }

    public String getTransaccionMonto() {
        return transaccionMonto;
    }

    public void setTransaccionMonto(String transaccionMonto) {
        this.transaccionMonto = transaccionMonto;
    }

    public String getTransaccionCid() {
        return transaccionCid;
    }

    public void setTransaccionCid(String transaccionCid) {
        this.transaccionCid = transaccionCid;
    }

    public String getTransaccionNombres() {
        return transaccionNombres;
    }

    public void setTransaccionNombres(String transaccionNombres) {
        this.transaccionNombres = transaccionNombres;
    }

    public String getTransaccionApellidos() {
        return transaccionApellidos;
    }

    public void setTransaccionApellidos(String transaccionApellidos) {
        this.transaccionApellidos = transaccionApellidos;
    }

    public String getTransaccionDireccion() {
        return transaccionDireccion;
    }

    public void setTransaccionDireccion(String transaccionDireccion) {
        this.transaccionDireccion = transaccionDireccion;
    }

    public String getTransaccionTelefono() {
        return transaccionTelefono;
    }

    public void setTransaccionTelefono(String transaccionTelefono) {
        this.transaccionTelefono = transaccionTelefono;
    }

    public String getTransaccionEmail() {
        return transaccionEmail;
    }

    public void setTransaccionEmail(String transaccionEmail) {
        this.transaccionEmail = transaccionEmail;
    }

    public String getTransaccionCp() {
        return transaccionCp;
    }

    public void setTransaccionCp(String transaccionCp) {
        this.transaccionCp = transaccionCp;
    }

    public String getMensajeId() {
        return MensajeId;
    }

    public void setMensajeId(String MensajeId) {
        this.MensajeId = MensajeId;
    }

    public String getTransaccionTipo() {
        return transaccionTipo;
    }

    public void setTransaccionTipo(String transaccionTipo) {
        this.transaccionTipo = transaccionTipo;
    }

    public String getTransaccionSubTipo() {
        return transaccionSubTipo;
    }

    public void setTransaccionSubTipo(String transaccionSubTipo) {
        this.transaccionSubTipo = transaccionSubTipo;
    }

    public String getTransaccionReversal() {
        return transaccionReversal;
    }

    public void setTransaccionReversal(String transaccionReversal) {
        this.transaccionReversal = transaccionReversal;
    }

    public String getTransaccionReversalIdn() {
        return transaccionReversalIdn;
    }

    public void setTransaccionReversalIdn(String transaccionReversalIdn) {
        this.transaccionReversalIdn = transaccionReversalIdn;
    }

    public String getTransaccionAccReference() {
        return transaccionAccReference;
    }

    public void setTransaccionAccReference(String transaccionAccReference) {
        this.transaccionAccReference = transaccionAccReference;
    }

    public String getTransaccionApprovalCode() {
        return transaccionApprovalCode;
    }

    public void setTransaccionApprovalCode(String transaccionApprovalCode) {
        this.transaccionApprovalCode = transaccionApprovalCode;
    }

    public String getTransaccionDate() {
        return transaccionDate;
    }

    public void setTransaccionDate(String transaccionDate) {
        this.transaccionDate = transaccionDate;
    }

    public String getTransaccionProcesada() {
        return transaccionProcesada;
    }

    public void setTransaccionProcesada(String transaccionProcesada) {
        this.transaccionProcesada = transaccionProcesada;
    }

    public String getTransaccionTipoTarjeta() {
        return transaccionTipoTarjeta;
    }

    public void setTransaccionTipoTarjeta(String transaccionTipoTarjeta) {
        this.transaccionTipoTarjeta = transaccionTipoTarjeta;
    }

    public String getTransaccionAcceptorId() {
        return transaccionAcceptorId;
    }

    public void setTransaccionAcceptorId(String transaccionAcceptorId) {
        this.transaccionAcceptorId = transaccionAcceptorId;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    
    
}
