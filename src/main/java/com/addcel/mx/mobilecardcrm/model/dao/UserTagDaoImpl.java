package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.UserTag;
import com.addcel.mx.mobilecardcrm.model.admin.UserTagSearch;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("userTagDao")
@Transactional
public class UserTagDaoImpl implements UserTagDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addUserTag(UserTag userTag) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.persist(userTag);
    }

    @Override
    public void updateUserTag(UserTag userTag) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.update(userTag);
    }

    @Override
    public List<UserTag> listUserTags() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(UserTag.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List userTags = cr.list();
        
        return userTags;
    }

    @Override
    public UserTag getUserTagById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(UserTag.class);
        Criterion id_criterion = Restrictions.eq("idPaises", id);
        criteria.add(id_criterion);
        
        UserTag userTag = (UserTag) criteria.list();
        
        return userTag;
    }

    @Override
    public void removeUserTag(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<UserTag> getPageUserTags(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(UserTag.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List userTags = cr.list();
        
        return userTags;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(UserTag.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }

    @Override
    public List<UserTag> searchTags(UserTagSearch userTagSearch) {
        
        Boolean validSearch = false;
        List<UserTag> userTags = new ArrayList();
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(UserTag.class);
        Conjunction conjunction = Restrictions.conjunction();
        
        if (!userTagSearch.getId_usuario().equals(new Long(-1))){
            System.out.println("id " + userTagSearch.getId_usuario());
            Criterion id_usuarioCriterion = Restrictions.eq("id_usuario", userTagSearch.getId_usuario());
            conjunction.add(id_usuarioCriterion);
            validSearch = true;
        }
        
        if (!userTagSearch.getTag().equals("")){
            System.out.println("tag " + userTagSearch.getTag());
            Criterion tagCriterion = Restrictions.eq("tag", userTagSearch.getTag());
            conjunction.add(tagCriterion);
            validSearch = true;
        }
        
        if (validSearch){
            System.out.println("Valid Search");
            cr.add(conjunction);
            userTags = cr.list();            
        }   
        
        return userTags;
    }        
    
}
