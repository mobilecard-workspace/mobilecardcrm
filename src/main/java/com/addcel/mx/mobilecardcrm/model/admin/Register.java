package com.addcel.mx.mobilecardcrm.model.admin;

/**
 *
 * @author Lasar-Soporte
 */
public class Register {
    
    private String tarjeta_compra;
    private String bit_fecha;
    private String bit_hora;
    private String bit_concepto;
    private int bit_codigo_error;
    private double bit_cargo;
    private double cargo_Aplicado;
    private double Comision;
    private String num_Tarjeta_Autorizacion;
    private String num_TarjetaIAVE_autorizacion;
    private String transaccion;
    private String Autorizacion;
    private String destino;

    public String getTarjeta_compra() {
        return tarjeta_compra;
    }

    public void setTarjeta_compra(String tarjeta_compra) {
        this.tarjeta_compra = tarjeta_compra;
    }

    public String getBit_fecha() {
        return bit_fecha;
    }

    public void setBit_fecha(String bit_fecha) {
        this.bit_fecha = bit_fecha;
    }

    public String getBit_hora() {
        return bit_hora;
    }

    public void setBit_hora(String bit_hora) {
        this.bit_hora = bit_hora;
    }

    public String getBit_concepto() {
        return bit_concepto;
    }

    public void setBit_concepto(String bit_concepto) {
        this.bit_concepto = bit_concepto;
    }

    public int getBit_codigo_error() {
        return bit_codigo_error;
    }

    public void setBit_codigo_error(int bit_codigo_error) {
        this.bit_codigo_error = bit_codigo_error;
    }

    public double getBit_cargo() {
        return bit_cargo;
    }

    public void setBit_cargo(double bit_cargo) {
        this.bit_cargo = bit_cargo;
    }

    public double getCargo_Aplicado() {
        return cargo_Aplicado;
    }

    public void setCargo_Aplicado(double cargo_Aplicado) {
        this.cargo_Aplicado = cargo_Aplicado;
    }

    public double getComision() {
        return Comision;
    }

    public void setComision(double Comision) {
        this.Comision = Comision;
    }

    public String getNum_Tarjeta_Autorizacion() {
        return num_Tarjeta_Autorizacion;
    }

    public void setNum_Tarjeta_Autorizacion(String num_Tarjeta_Autorizacion) {
        this.num_Tarjeta_Autorizacion = num_Tarjeta_Autorizacion;
    }

    public String getNum_TarjetaIAVE_autorizacion() {
        return num_TarjetaIAVE_autorizacion;
    }

    public void setNum_TarjetaIAVE_autorizacion(String num_TarjetaIAVE_autorizacion) {
        this.num_TarjetaIAVE_autorizacion = num_TarjetaIAVE_autorizacion;
    }

    public String getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    public String getAutorizacion() {
        return Autorizacion;
    }

    public void setAutorizacion(String Autorizacion) {
        this.Autorizacion = Autorizacion;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }
    
    
    
}
