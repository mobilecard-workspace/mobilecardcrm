package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.User;
import com.addcel.mx.mobilecardcrm.utils.PasswordEncrypter;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("userDao")
@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session session;
	private Criteria criteria;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void addUser(User user) {
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(user);
		tx.commit();
		session.close();
	}

	@Override
	public void updateUser(User user) {
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}
		Transaction tx = session.beginTransaction();
		session.update(user);
		tx.commit();
		session.close();
	}

	@Override
	public List<User> listUsers() {
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}

		Criteria cr = session.createCriteria(User.class);
		List users = cr.list();

		return users;
	}

	@Override
	public User getUserById(Long id) {
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}
		Criteria criteria = session.createCriteria(User.class);
		Criterion id_criterion = Restrictions.eq("id", id);
		criteria.add(id_criterion);

		User user = (User) criteria.list().get(0);

		return user;
	}

	@Override
	public void removeUser(int id) {
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}
		User user = session.load(User.class, id);
		Transaction tx = session.beginTransaction();
		session.remove(user);
		tx.commit();
		session.close();
	}

	@Override
	public User authenticateUser(String username, String password) {
		session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		Criterion username_criterion = Restrictions.eq("username", username);
		Criterion enable_criterion = Restrictions.eq("enabled", true);
		Criterion password_criterion = null;
		Conjunction conjunction = Restrictions.conjunction();
		conjunction.add(username_criterion);
		conjunction.add(enable_criterion);
		try {
			PasswordEncrypter passwordEncrypter = new PasswordEncrypter();
			password_criterion = Restrictions.eq("password", passwordEncrypter.getPassEncrypted(password));
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
		conjunction.add(password_criterion);
		criteria.add(conjunction);

		User user = new User();
		if (!criteria.list().isEmpty()) {
			user = (User) criteria.list().get(0);
			Hibernate.initialize(user.getRoles());
		}

		return user;
	}
}
