/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.Role;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface RoleDao {
    
    public void addRole(Role role);
    public void updateRole(Role role);
    public List<Role> listRoles();
    public Role getRoleById(int id);
    public void removeRole(int id);
    public Role getRoleByAuthority(String name);
    
}
