package com.addcel.mx.mobilecardcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "t_usr_status")
public class ClientStatus {
    
    private int id_usr_status; 
    private String desc_usr_status;

    @Id
    @Column(name="id_usr_status")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId_usr_status() {
        return id_usr_status;
    }

    public void setId_usr_status(int id_usr_status) {
        this.id_usr_status = id_usr_status;
    }

    public String getDesc_usr_status() {
        return desc_usr_status;
    }

    public void setDesc_usr_status(String desc_usr_status) {
        this.desc_usr_status = desc_usr_status;
    }
    
    
    
}
