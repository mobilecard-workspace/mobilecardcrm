package com.addcel.mx.mobilecardcrm.model.admin;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "crm_user_module")
public class UserModule implements Serializable{
    
    @Id
    @Column(name="id_user")    
    private Long id_user;
    
    @Id
    @Column(name="id_module")    
    private int id_module;

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public int getId_module() {
        return id_module;
    }

    public void setId_module(int id_module) {
        this.id_module = id_module;
    }
    
    
}
