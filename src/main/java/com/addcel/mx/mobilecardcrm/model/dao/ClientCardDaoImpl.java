package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Client;
import com.addcel.mx.mobilecardcrm.model.admin.ClientCard;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("clientCardDao")
@Transactional
public class ClientCardDaoImpl implements ClientCardDao{

    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addClientCard(ClientCard clientCard) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();            
        }
        Transaction tx = session.beginTransaction();
        
        session.saveOrUpdate(clientCard); 
        
        tx.commit();
        
        session.close();
    }

    @Override
    public void updateClientCard(ClientCard clientCard) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();            
        }
        Transaction tx = session.beginTransaction();
        
        session.update(clientCard); 
        
        tx.commit();
        
        session.close();
    }

    @Override
    public List<ClientCard> listClientCards() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(ClientCard.class);

        List clients = cr.list();
        
        return clients;
    }

    @Override
    public ClientCard getClientCardById(Long id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(ClientCard.class);
        Criterion id_criterion = Restrictions.eq("idtarjetasusuario", id);
        criteria.add(id_criterion);
        List<ClientCard> clientCards = criteria.list();
        ClientCard clientCard = null;
        if (clientCards.size() > 0){
            clientCard = (ClientCard) criteria.list().get(0);
        }
        
        return clientCard;
    }

    @Override
    public List<ClientCard> getClientCardsByClientId(Long clientId) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(ClientCard.class);
        Criterion id_criterion = Restrictions.eq("idusuario", clientId);
        criteria.add(id_criterion);
        List<ClientCard> clientCards = criteria.list();        
        
        return clientCards;
    }

    @Override
    public ClientCard getClientCardByNumberCard(String numcard) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(ClientCard.class);
        Criterion id_criterion = Restrictions.eq("numerotarjeta", numcard);
        criteria.add(id_criterion);
        List<ClientCard> clientCards = criteria.list();
        ClientCard clientCard = null;
        if (clientCards.size() > 0){
            clientCard = (ClientCard) criteria.list().get(0);
        }                
        
        return clientCard;
    }

    @Override
    public void removeClientCard(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction tx = session.beginTransaction();
        
        session.remove(id);
        
        tx.commit();
        
        session.close();        
    }
    
    
    
}
