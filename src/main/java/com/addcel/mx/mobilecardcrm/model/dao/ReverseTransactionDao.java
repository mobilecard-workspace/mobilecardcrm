package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransaction;
import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransactionSearch;
import java.util.List;

/**
 *
 * @author wsolano
 */

public interface ReverseTransactionDao {
    public void addReverseTransaction(ReverseTransaction client);
    public void updateReverseTransaction(ReverseTransaction client);
    public List<ReverseTransaction> listReverseTransactions();
    public List<ReverseTransaction> getReverseTransactionByIdUsuario(int id_usuario);
    public void removeReverseTransaction(int id);    
    public List<ReverseTransaction> getPageReverseTransactions(int offset, int max);
    public Long getNumberRows ();
    
    public List<ReverseTransaction> searchReverseTransactions (ReverseTransactionSearch reverseTransactionSearch);
}
