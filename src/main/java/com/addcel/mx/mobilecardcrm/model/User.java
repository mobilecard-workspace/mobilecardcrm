package com.addcel.mx.mobilecardcrm.model;

import com.addcel.mx.mobilecardcrm.model.admin.UserModule;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "user")
public class User {
        
    private Long id;    
    private int version;
    private boolean account_expired;
    private boolean account_locked;
    private boolean enabled;
    private boolean password_expired;
    private String password;
    private String username;
    private List<Role> roles;
    private List<UserModule> userModules;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isAccount_expired() {
        return account_expired;
    }

    public void setAccount_expired(boolean account_expired) {
        this.account_expired = account_expired;
    }

    public boolean isAccount_locked() {
        return account_locked;
    }

    public void setAccount_locked(boolean account_locked) {
        this.account_locked = account_locked;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isPassword_expired() {
        return password_expired;
    }

    public void setPassword_expired(boolean password_expired) {
        this.password_expired = password_expired;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role", joinColumns = {
                    @JoinColumn(name = "user_id", nullable = false, updatable = false) },
                    inverseJoinColumns = { @JoinColumn(name = "role_id",
                                    nullable = false, updatable = false) })
    public List<Role> getRoles() {
            return this.roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id_user")
    public List<UserModule> getUserModules() {
        return userModules;
    }

    public void setUserModules(List<UserModule> userModules) {
        this.userModules = userModules;
    }
    
    
}
