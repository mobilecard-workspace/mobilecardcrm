package com.addcel.mx.mobilecardcrm.model.dao;

import java.util.Date;
import java.util.List;

import com.addcel.mx.mobilecardcrm.model.admin.AmexTransaction;

/**
 *
 * @author oskar.cahuenas
 */
public interface AmericanExpressDao {

    public List<AmexTransaction> getNonProcessedTransactions(Date date);
    
    public void reverseTransactions(List<Long> transactionIds);
    

}
