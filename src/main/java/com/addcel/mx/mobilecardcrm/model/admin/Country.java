package com.addcel.mx.mobilecardcrm.model.admin;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "paises")
public class Country {
    
    private Long idpaises;
    private String nombre;
    private String codigo_iata;
    
    @Id
    public Long getIdpaises() {
        return idpaises;
    }

    public void setIdpaises(Long id_paises) {
        this.idpaises = id_paises;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo_iata() {
        return codigo_iata;
    }

    public void setCodigo_iata(String codigo_iata) {
        this.codigo_iata = codigo_iata;
    }        
    
}
