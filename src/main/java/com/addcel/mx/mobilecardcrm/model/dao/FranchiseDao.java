package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Franchise;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface FranchiseDao {
    
    public void addFranchise(Franchise franchise);
    public void updateFranchise(Franchise franchise);
    public List<Franchise> listFranchises();
    public Franchise getFranchiseById(int id);
    public void removeFranchise(int id);    
    public List<Franchise> getPageFranchises(int offset, int max);
    public Long getNumberRows ();
    
}
