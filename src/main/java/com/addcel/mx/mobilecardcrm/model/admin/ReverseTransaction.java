package com.addcel.mx.mobilecardcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author wsolano
 */
@Entity
@Table(name = "bitacora_reverso")
public class ReverseTransaction {
    
    private int id_bitacora;
    private Long id_usuario;
    private String secuencia;
    private String fechahora;
    private String autorizacion;
    private String claveoperacion;
    private String claveventa;
    private String descripcion;
    private String errorcode;
    private Double importe;
    private String resultado;

    
    @Id
    @Column(name="id_bitacora")
    public int getId_bitacora() {
        return id_bitacora;
    }

    public void setId_bitacora(int id_bitacora) {
        this.id_bitacora = id_bitacora;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getFechahora() {
        return fechahora;
    }

    public void setFechahora(String fechahora) {
        this.fechahora = fechahora;
    }

    public String getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }

    public String getClaveoperacion() {
        return claveoperacion;
    }

    public void setClaveoperacion(String claveoperacion) {
        this.claveoperacion = claveoperacion;
    }

    public String getClaveventa() {
        return claveventa;
    }

    public void setClaveventa(String claveventa) {
        this.claveventa = claveventa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
    
    
}
