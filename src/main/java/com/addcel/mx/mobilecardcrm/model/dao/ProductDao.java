/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Product;
import java.util.List;

/**
 *
 * @author marcopascale
 */
public interface ProductDao {
    
    public List<Product> getListProducts();
    
}
