package com.addcel.mx.mobilecardcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "crm_module")
public class Module {
    
    @Id
    @Column(name="idcrm_module") 
    private int idcrm_module;
    
    private String description;

    public int getIdcrm_module() {
        return idcrm_module;
    }

    public void setIdcrm_module(int idcrm_module) {
        this.idcrm_module = idcrm_module;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}
