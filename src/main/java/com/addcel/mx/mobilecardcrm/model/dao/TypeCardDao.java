package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.TypeCard;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface TypeCardDao {
    
    public void addTypeCard(TypeCard typeCard);
    public void updateTypeCard(TypeCard typeCard);
    public List<TypeCard> listTypeCards();
    public TypeCard getTypeCardById(int id);
    public void removeTypeCard(int id);    
    public List<TypeCard> getPageTypeCards(int offset, int max);
    public Long getNumberRows ();
    
}
