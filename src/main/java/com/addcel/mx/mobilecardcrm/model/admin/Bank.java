package com.addcel.mx.mobilecardcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "t_banco")
public class Bank {
    
    private Long id_banco;
    private String desc_banco;
    private String desc_banco_corta;
    private Integer bin_bajo;
    private Integer bin_alto;
    private Integer activo; 

    @Id
    @Column(name="id_banco")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId_banco() {
        return id_banco;
    }

    public void setId_banco(Long id_banco) {
        this.id_banco = id_banco;
    }

    public String getDesc_banco() {
        return desc_banco;
    }

    public void setDesc_banco(String desc_banco) {
        this.desc_banco = desc_banco;
    }

    public String getDesc_banco_corta() {
        return desc_banco_corta;
    }

    public void setDesc_banco_corta(String desc_banco_corta) {
        this.desc_banco_corta = desc_banco_corta;
    }

    public Integer getBin_bajo() {
        return bin_bajo;
    }

    public void setBin_bajo(Integer bin_bajo) {
        this.bin_bajo = bin_bajo;
    }

    public Integer getBin_alto() {
        return bin_alto;
    }

    public void setBin_alto(Integer bin_alto) {
        this.bin_alto = bin_alto;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
    
    
    
}
