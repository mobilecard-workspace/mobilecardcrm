package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.UserTag;
import com.addcel.mx.mobilecardcrm.model.admin.UserTagSearch;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface UserTagDao {
    
    public void addUserTag(UserTag userTags);
    public void updateUserTag(UserTag userTags);
    public List<UserTag> listUserTags();
    public UserTag getUserTagById(int id);
    public void removeUserTag(int id);    
    public List<UserTag> getPageUserTags(int offset, int max);
    public Long getNumberRows ();
    
    public List<UserTag> searchTags(UserTagSearch userTagSearch);
    
}
