/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Client;
import com.addcel.mx.mobilecardcrm.model.admin.Product;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author marcopascale
 */
@Repository("ProductDao")
@Transactional
public class ProductDaoImpl implements ProductDao{
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;
    
    @Override
    public List<Product> getListProducts() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Product.class);
        List products = cr.list();
        
        return products;
    }
    
}
