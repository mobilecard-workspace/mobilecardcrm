package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Module;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("moduleDao")
@Transactional
public class ModuleDaoImpl implements ModuleDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addModule(Module Module) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }        
        Transaction tx = session.beginTransaction();        
        session.saveOrUpdate(Module);
        tx.commit();        
        session.close();                        
    }

    @Override
    public void updateModule(Module Module) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }             
        Transaction tx = session.beginTransaction();        
        session.update(Module);
        tx.commit();        
        session.close();                
    }

    @Override
    public List<Module> listModules() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Module.class);
        List Modules = cr.list();
        
        return Modules;
    }

    @Override
    public Module getModuleById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(Module.class);
        Criterion id_criterion = Restrictions.eq("id", id);
        criteria.add(id_criterion);
        
        Module module = (Module) criteria.list().get(0);
        
        return module;
    }

    @Override
    public void removeModule(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }               
        Module Module = session.load(Module.class, id);
        Transaction tx = session.beginTransaction();        
        session.remove(Module);
        tx.commit();        
        session.close();                
    }
    
}
