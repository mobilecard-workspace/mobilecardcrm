package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Bank;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface BankDao {
    
    public void addBank(Bank bank);
    public void updateBank(Bank bank);
    public List<Bank> listBanks();
    public Bank getBankById(int id);
    public void removeBank(int id);    
    public List<Bank> getPageBanks(int offset, int max);
    public Long getNumberRows ();
    
}
