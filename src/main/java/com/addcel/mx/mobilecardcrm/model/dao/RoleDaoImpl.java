package com.addcel.mx.mobilecardcrm.model.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.addcel.mx.mobilecardcrm.model.Role;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("roleDao")
@Component
public class RoleDaoImpl implements RoleDao{
        
    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addRole(Role role) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }                
        session.persist(role);                
    }

    @Override
    public void updateRole(Role role) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }               
        session.update(role);
                
    }

    @Override
    public List<Role> listRoles() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria cr = session.createCriteria(Role.class);        
        List roles = cr.list();
        
        return roles;
    }

    @Override
    public Role getRoleById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(Role.class);
        Criterion id_criterion = Restrictions.eq("id", id);
        criteria.add(id_criterion);
        
        Role role = (Role) criteria.list();
        
        return role;
    }

    @Override
    public void removeRole(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }                
        Role role = session.load(Role.class, id);
        session.remove(role);
                
    }

	@Override
	public Role getRoleByAuthority(String authority) {
		 try {
	            session = sessionFactory.getCurrentSession();
	        } catch (HibernateException e) {
	            session = sessionFactory.openSession();
	        }
		 	Criteria criteria = session.createCriteria(Role.class);
	        Criterion id_criterion = Restrictions.eq("authority", authority);
	        criteria.add(id_criterion);
	        Role role = (Role)criteria.uniqueResult();
	        session.close();
	        return role;
	}
    
}
