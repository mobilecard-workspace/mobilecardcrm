package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.Client;
import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransaction;
import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransactionSearch;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Parameter;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author wsolano
 */
@Repository("reverseTransactionDao")
@Transactional
public class ReverseTransactionDaoImpl implements ReverseTransactionDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addReverseTransaction(ReverseTransaction transaction) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.persist(transaction);
    }

    @Override
    public void updateReverseTransaction(ReverseTransaction transaction) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.update(transaction);
    }

    @Override
    public List<ReverseTransaction> listReverseTransactions() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(ReverseTransaction.class);
        cr.setFirstResult(1);
        cr.setMaxResults(500);
        List transactions = cr.list();

        return transactions;
    }

    @Override
    public List<ReverseTransaction> getReverseTransactionByIdUsuario(int id_usuario) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(ReverseTransaction.class);
        Criterion id_criterion = Restrictions.eq("id_usuario", id_usuario);
        criteria.add(id_criterion);

        List<ReverseTransaction> reverseTransactions = criteria.list();

        return reverseTransactions;
    }

    @Override
    public void removeReverseTransaction(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<ReverseTransaction> getPageReverseTransactions(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(ReverseTransaction.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List transactions = cr.list();

        return transactions;
    }

    @Override
    public Long getNumberRows() {

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria criteria = session.createCriteria(ReverseTransaction.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        return count;
    }

    @Override
    public List<ReverseTransaction> searchReverseTransactions(ReverseTransactionSearch reverseTransactionSearch) {
        Boolean searchByClientData = false;
        Boolean searchByTransactionData = false;
        List<Client> clients = new ArrayList<>();
        List<ReverseTransaction> reverseTransactions = new ArrayList();
        String queryString = "SELECT br.* FROM bitacora_reverso br ";
        Date initialDate = new Date();
        Date finalDate = new Date();
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        if (!reverseTransactionSearch.getFechaHora_inicial().isEmpty() && !reverseTransactionSearch.getFechaHora_final().isEmpty()) {
            queryString = queryString + "INNER JOIN t_bitacora tb ON br.id_bitacora = tb.id_bitacora ";
            searchByTransactionData = true;
        }
        if (!reverseTransactionSearch.getUsr_apellido().isEmpty() || !reverseTransactionSearch.getUsr_nombre().isEmpty()) {
            queryString = queryString + "INNER JOIN t_usuarios tu ON br.id_usuario = tu.id_usuario ";
            searchByClientData = true;
        }

        if (searchByClientData || searchByTransactionData) {
            queryString = queryString + "WHERE ";
            Boolean existAnd = false;
            if (searchByClientData) {
                if (!reverseTransactionSearch.getUsr_apellido().isEmpty()) {
                    queryString = queryString + "tu.usr_apellido = :usr_apellido ";
                    existAnd = true;
                }
                if (!reverseTransactionSearch.getUsr_nombre().isEmpty()) {
                    if (existAnd) {
                        queryString = queryString + "AND tu.usr_nombre = :usr_nombre ";
                    } else {
                        queryString = queryString + "tu.usr_nombre = :usr_nombre ";
                        existAnd = true;
                    }
                }
            }
            if (searchByTransactionData) {
                try {
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    initialDate = (Date) format.parse(reverseTransactionSearch.getFechaHora_inicial());
                    finalDate = (Date) format.parse(reverseTransactionSearch.getFechaHora_final());                    
                    if (existAnd){
                        queryString = queryString + "AND tb.bit_fecha BETWEEN :initialDate AND :finalDate ";
                    }else{
                        queryString = queryString + "tb.bit_fecha BETWEEN :initialDate AND :finalDate ";
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(ClientDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }                
            }
            
            System.out.println("Query " + queryString);
            
            Query query = session.createSQLQuery(queryString)
                .addEntity(ReverseTransaction.class);
            
            if (searchByTransactionData){
                query.setParameter("initialDate", initialDate);
                query.setParameter("finalDate", finalDate);
            }
            if (searchByClientData){
                if (!reverseTransactionSearch.getUsr_apellido().isEmpty()) {
                    query.setParameter("usr_apellido", reverseTransactionSearch.getUsr_apellido());
                }
                if (!reverseTransactionSearch.getUsr_nombre().isEmpty()) {
                    query.setParameter("usr_nombre", reverseTransactionSearch.getUsr_nombre());
                }
            }
            for (Parameter parameter: query.getParameters()){
                System.out.println(parameter.getPosition() + " " + parameter.getName() + " " + query.getParameterValue(parameter.getName()));
            }            
            reverseTransactions = query.list();
        }                       
        return reverseTransactions;
    }

}
