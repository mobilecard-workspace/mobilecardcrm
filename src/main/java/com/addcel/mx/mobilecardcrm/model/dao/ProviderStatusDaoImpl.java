package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.ProviderStatus;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("providerStatusDao")
@Transactional
public class ProviderStatusDaoImpl implements ProviderStatusDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addProviderStatus(ProviderStatus providerStatus) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.persist(providerStatus);
    }

    @Override
    public void updateProviderStatus(ProviderStatus providerStatus) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.update(providerStatus);
    }

    @Override
    public List<ProviderStatus> listProviderStatus() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(ProviderStatus.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List providerStatuss = cr.list();
        
        return providerStatuss;
    }

    @Override
    public ProviderStatus getProviderStatusById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(ProviderStatus.class);
        Criterion id_criterion = Restrictions.eq("id_banco", id);
        criteria.add(id_criterion);
        
        ProviderStatus providerStatus = (ProviderStatus) criteria.list();
        
        return providerStatus;
    }

    @Override
    public void removeProviderStatus(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<ProviderStatus> getPageProviderStatus(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(ProviderStatus.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List providerStatuss = cr.list();
        
        return providerStatuss;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(ProviderStatus.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }
    
    
}
