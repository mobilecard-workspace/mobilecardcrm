package com.addcel.mx.mobilecardcrm.model.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.addcel.mx.mobilecardcrm.model.admin.AmexTransaction;

/**
 *
 * @author oskar.cahuenas
 */
@Repository("americanExpressDao")
@Component
public class AmericanExpressDaoImpl implements AmericanExpressDao {

	private static final String SQL_DATE_FORMAT = "yyyy-MM-dd";
	
	@Autowired
	@Qualifier("ecommerceSessionFactory")
	private SessionFactory sessionFactory;

	@Override
	public List<AmexTransaction> getNonProcessedTransactions(Date date) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<AmexTransaction>  criteriaQuery = cb.createQuery(AmexTransaction.class);
		Root<AmexTransaction> root = criteriaQuery.from(AmexTransaction.class);
		criteriaQuery = criteriaQuery.select(root);
		
		//armardo condición WHERE
		DateFormat df = new SimpleDateFormat(SQL_DATE_FORMAT);
 		//funcion TRIM mysql
		Expression<String> trim = cb.function("TRIM", String.class, root.get("transaccionApprovalCode"));
		//funcion DATE_FORMAT mysql
		ParameterExpression<String> dateMask = cb.parameter(String.class);
		Expression<String> dateFormat = cb.function("DATE_FORMAT", String.class, root.get("transaccionDate"), dateMask);
		Predicate condition = cb.and(
				cb.notEqual(trim, ""),
				cb.equal(dateFormat, df.format(date)),
				cb.equal(root.get("transaccionProcesada"), "N")
				);
		criteriaQuery.where(condition);
		
		Query<AmexTransaction> q = session.createQuery(criteriaQuery);
		q.setParameter(dateMask, "%Y-%m-%d");
		return q.getResultList();
	}

	@Override
	public void reverseTransactions(List<Long> transactionIds) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"UPDATE  AmexTransaction SET transaccionReversal = 'R ' WHERE transaccionIdn IN :ids ");
		query.setParameter("ids", transactionIds);
		query.executeUpdate();
	}

}
