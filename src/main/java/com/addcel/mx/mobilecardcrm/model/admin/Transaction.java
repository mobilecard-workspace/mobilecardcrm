package com.addcel.mx.mobilecardcrm.model.admin;

import java.sql.Date;
import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author wsolano
 */
@Entity
@Table(name = "t_bitacora")
public class Transaction {
    
    private int id_bitacora; 
    private Long id_usuario; 
    private Long id_proveedor; 
    private Long id_producto; 
    private Date bit_fecha; 
    private Time bit_hora;
    private String bit_concepto;
    private Double bit_cargo;
    private String bit_ticket;
    private String bit_no_autorizacion;
    private Integer bit_codigo_error;
    private Integer bit_card_id;
    private Integer bit_status; 
    private String imei;
    private String destino;
    private String tarjeta_compra; 
    private String tipo; 
    private String software;
    private String modelo; 
    private String wkey;
    private Integer pase;    
    
    @Id
    @Column(name="id_bitacora", unique = true)    
    public int getId_bitacora() {
        return id_bitacora;
    }

    public void setId_bitacora(int id_bitacora) {
        this.id_bitacora = id_bitacora;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(Long id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public Long getId_producto() {
        return id_producto;
    }

    public void setId_producto(Long id_producto) {
        this.id_producto = id_producto;
    }

    public Date getBit_fecha() {
        return bit_fecha;
    }

    public void setBit_fecha(Date bit_fecha) {
        this.bit_fecha = bit_fecha;
    }

    public Time getBit_hora() {
        return bit_hora;
    }

    public void setBit_hora(Time bit_hora) {
        this.bit_hora = bit_hora;
    }

    public String getBit_concepto() {
        return bit_concepto;
    }

    public void setBit_concepto(String bit_concepto) {
        this.bit_concepto = bit_concepto;
    }

    public Double getBit_cargo() {
        return bit_cargo;
    }

    public void setBit_cargo(Double bit_cargo) {
        this.bit_cargo = bit_cargo;
    }

    public String getBit_ticket() {
        return bit_ticket;
    }

    public void setBit_ticket(String bit_ticket) {
        this.bit_ticket = bit_ticket;
    }

    public String getBit_no_autorizacion() {
        return bit_no_autorizacion;
    }

    public void setBit_no_autorizacion(String bit_no_autorizacion) {
        this.bit_no_autorizacion = bit_no_autorizacion;
    }

    public Integer getBit_codigo_error() {
        return bit_codigo_error;
    }

    public void setBit_codigo_error(Integer bit_codigo_error) {
        this.bit_codigo_error = bit_codigo_error;
    }

    public Integer getBit_card_id() {
        return bit_card_id;
    }

    public void setBit_card_id(Integer bit_card_id) {
        this.bit_card_id = bit_card_id;
    }

    public Integer getBit_status() {
        return bit_status;
    }

    public void setBit_status(Integer bit_status) {
        this.bit_status = bit_status;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getTarjeta_compra() {
        return tarjeta_compra;
    }

    public void setTarjeta_compra(String tarjeta_compra) {
        this.tarjeta_compra = tarjeta_compra;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public Integer getPase() {
        return pase;
    }

    public void setPase(Integer pase) {
        this.pase = pase;
    }
    
    
}
