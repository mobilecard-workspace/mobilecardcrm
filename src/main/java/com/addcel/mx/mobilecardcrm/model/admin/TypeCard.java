package com.addcel.mx.mobilecardcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "t_tipo_tarjeta")
public class TypeCard {
    
    private int id_tipo_tarjeta;
    private String desc_tipo_tarjeta;
    private Integer activo;
    
    @Id
    @Column(name="id_tipo_tarjeta")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId_tipo_tarjeta() {
        return id_tipo_tarjeta;
    }

    public void setId_tipo_tarjeta(int id_tipo_tarjeta) {
        this.id_tipo_tarjeta = id_tipo_tarjeta;
    }

    public String getDesc_tipo_tarjeta() {
        return desc_tipo_tarjeta;
    }

    public void setDesc_tipo_tarjeta(String desc_tipo_tarjeta) {
        this.desc_tipo_tarjeta = desc_tipo_tarjeta;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
         
}
