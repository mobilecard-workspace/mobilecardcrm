package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.AccountStatus;
import com.addcel.mx.mobilecardcrm.model.admin.AmexTransaction;
import com.addcel.mx.mobilecardcrm.model.admin.Register;
import com.addcel.mx.mobilecardcrm.utils.Crypto;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("conciliationDao")
@Transactional
public class ConciliationDaoImpl implements ConcilationDao {

    @Autowired
    private SessionFactory sessionFactory; 
    
    @Autowired
    private SessionFactory ecommerceSessionFactory; 

    private Session session;
    private Session ecommerceSession;

    @Override
    public List<Register> findAddcelPayments(String date) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Date dateAux = new Date();        
        List<Register> registers = new ArrayList<>();

        String queryString = "select tb.tarjeta_compra, \n" +
                            "	tb.bit_fecha, \n" +
                            "	time(tb.bit_hora) bit_hora,\n" +
                            "    tb.bit_concepto,\n" +
                            "	tb.bit_codigo_error, \n" +
                            "	tb.bit_cargo, \n" +
                            "	bp.Cargo cargo_Aplicado,\n" +
                            "	bp.comision Comision, \n" +
                            "	bp.tarjeta num_Tarjeta_Autorizacion,\n" +
                            "	bp.TarjetaIAVE num_TarjetaIAVE_autorizacion, \n" +
                            "	bp.transaccion transaccion,\n" +
                            "	bp.autorizacion Autorizacion,\n" +
                            "	tb.destino \n" +
                            "from t_bitacora tb\n" +
                            "	left join t_bitacoraProsa bp on bp.id_bitacora = tb.id_bitacora\n" +
                            "	left join t_bitacoraiave bi on bi.id_bitacora = tb.id_bitacora \n" +
                            "where tb.bit_status = 1 \n" +
                            "	and (tb.id_proveedor in (5,8) or (tb.id_proveedor = 17 and tb.id_producto = 112))\n" +
                            "	and  (tb.bit_codigo_error is null or tb.bit_codigo_error = 0) \n" +
                            "	and tb.bit_fecha = :date \n" +
                            "	order by tb.id_bitacora;";
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateAux = (Date) format.parse(date);            
        } catch (ParseException ex) {
            Logger.getLogger(ConciliationDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        Query query = session.createSQLQuery(queryString);

        query.setParameter("date", dateAux);
        
        List<Object[]> list = query.list();
        
        for (Object[] result : list) {            
            Register register = new Register();
            if (result[0] != null) {
                register.setTarjeta_compra(result[0].toString());
            }
            if (result[1] != null) {
                register.setBit_fecha(result[1].toString());
            }
            if (result[2] != null) {
                register.setBit_hora(result[2].toString());
            }
            if (result[3] != null) {
                register.setBit_concepto(result[3].toString());
            }            
            if (result[4] != null) {
                register.setBit_codigo_error(Integer.parseInt(result[4].toString()));
            }            
            if (result[5] != null) {
                register.setBit_cargo(Double.parseDouble(result[5].toString()));
            }
            if (result[6] != null) {
                register.setCargo_Aplicado(Double.parseDouble(result[6].toString()));
            }
            if (result[7] != null) {
                register.setComision(Double.parseDouble(result[7].toString()));
            }
            if (result[8] != null) {
                register.setNum_Tarjeta_Autorizacion(result[8].toString());
            }
            if (result[9] != null) {
                register.setNum_TarjetaIAVE_autorizacion(result[9].toString());
            }
            if (result[10] != null) {
                register.setTransaccion(result[10].toString());
            }
            if (result[11] != null) {
                register.setAutorizacion(result[11].toString());
            }
            if (result[12] != null) {
                register.setDestino(result[12].toString());
            }
            registers.add(register);
        }        
        return registers;
    }

    @Override
    public List<Register> findIdPayments(String date) {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Date dateAux = new Date();        
        List<Register> registers = new ArrayList<>();

        String queryString = "select\n" +
"			tb.tarjeta_compra, \n" +
"			tb.bit_fecha, \n" +
"			time(tb.bit_hora) bit_hora,\n" +
"			tb.bit_concepto, \n" +
"			tb.bit_codigo_error, \n" +
"			tb.bit_cargo, \n" +
"			bp.Cargo cargo_Aplicado,\n" +
"			bp.comision Comision, \n" +
"			bp.tarjeta num_Tarjeta_Autorizacion,\n" +
"			bp.TarjetaIAVE num_TarjetaIAVE_autorizacion, \n" +
"			bp.transaccion transaccion,\n" +
"			bp.autorizacion Autorizacion,\n" +
"			tb.destino \n" +
"		from t_bitacora tb\n" +
"      		left join t_bitacoraProsa bp on bp.id_bitacora = tb.id_bitacora \n" +
"      		left join t_bitacoraiave bi on bi.id_bitacora = tb.id_bitacora \n" +
"     	where tb.bit_status = 1 \n" +
"            and tb.id_proveedor = 17  \n" +
"            and tb.id_producto != 112\n" +
"            and (tb.bit_codigo_error is null or tb.bit_codigo_error = 0) \n" +
"            and tb.bit_fecha = :date order by tb.id_bitacora;";
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateAux = (Date) format.parse(date);            
        } catch (ParseException ex) {
            Logger.getLogger(ConciliationDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        Query query = session.createSQLQuery(queryString);

        query.setParameter("date", dateAux);
        
        List<Object[]> list = query.list();
        
        for (Object[] result : list) {            
            Register register = new Register();
            if (result[0] != null) {
                register.setTarjeta_compra(result[0].toString());
            }
            if (result[1] != null) {
                register.setBit_fecha(result[1].toString());
            }
            if (result[2] != null) {
                register.setBit_hora(result[2].toString());
            }
            if (result[3] != null) {
                register.setBit_concepto(result[3].toString());
            }            
            if (result[4] != null) {
                register.setBit_codigo_error(Integer.parseInt(result[4].toString()));
            }            
            if (result[5] != null) {
                register.setBit_cargo(Double.parseDouble(result[5].toString()));
            }
            if (result[6] != null) {
                register.setCargo_Aplicado(Double.parseDouble(result[6].toString()));
            }
            if (result[7] != null) {
                register.setComision(Double.parseDouble(result[7].toString()));
            }
            if (result[8] != null) {
                register.setNum_Tarjeta_Autorizacion(result[8].toString());
            }
            if (result[9] != null) {
                register.setNum_TarjetaIAVE_autorizacion(result[9].toString());
            }
            if (result[10] != null) {
                register.setTransaccion(result[10].toString());
            }
            if (result[11] != null) {
                register.setAutorizacion(result[11].toString());
            }
            if (result[12] != null) {
                register.setDestino(result[12].toString());
            }
            registers.add(register);
        }        
        return registers;
    }

    @Override
    public List<Register> findLegacyPayments(String date) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Date dateAux = new Date();        
        List<Register> registers = new ArrayList<>();

        String queryString = "select\n" +
"			tb.tarjeta_compra, \n" +
"			tb.bit_fecha, \n" +
"			time(tb.bit_hora) bit_hora,\n" +
"			tb.bit_concepto, \n" +
"			tb.bit_codigo_error, \n" +
"			tb.bit_cargo, \n" +
"			bp.Cargo cargo_Aplicado,\n" +
"			bp.comision Comision, \n" +
"			bp.tarjeta num_Tarjeta_Autorizacion,\n" +
"			bp.TarjetaIAVE num_TarjetaIAVE_autorizacion, \n" +
"			bp.transaccion transaccion,\n" +
"			bp.autorizacion Autorizacion,\n" +
"			tb.destino \n" +
"		from t_bitacora tb\n" +
"      		left join t_bitacoraProsa bp on bp.id_bitacora = tb.id_bitacora \n" +
"      		left join t_bitacoraiave bi on bi.id_bitacora = tb.id_bitacora \n" +
"     	where tb.bit_status = 1 \n" +
"            and tb.id_proveedor = 17  \n" +
"            and tb.id_producto != 112\n" +
"            and (tb.bit_codigo_error is null or tb.bit_codigo_error = 0 \n" +
"            and tb.bit_fecha = :date order by tb.id_bitacora;";
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateAux = (Date) format.parse(date);            
        } catch (ParseException ex) {
            Logger.getLogger(ConciliationDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        Query query = session.createSQLQuery(queryString);

        query.setParameter("date", dateAux);
        
        List<Object[]> list = query.list();
        
        for (Object[] result : list) {            
            Register register = new Register();
            if (result[0] != null) {
                register.setTarjeta_compra(result[0].toString());
            }
            if (result[1] != null) {
                register.setBit_fecha(result[1].toString());
            }
            if (result[2] != null) {
                register.setBit_hora(result[2].toString());
            }
            if (result[3] != null) {
                register.setBit_concepto(result[3].toString());
            }            
            if (result[4] != null) {
                register.setBit_codigo_error(Integer.parseInt(result[4].toString()));
            }            
            if (result[5] != null) {
                register.setBit_cargo(Double.parseDouble(result[5].toString()));
            }
            if (result[6] != null) {
                register.setCargo_Aplicado(Double.parseDouble(result[6].toString()));
            }
            if (result[7] != null) {
                register.setComision(Double.parseDouble(result[7].toString()));
            }
            if (result[8] != null) {
                register.setNum_Tarjeta_Autorizacion(result[8].toString());
            }
            if (result[9] != null) {
                register.setNum_TarjetaIAVE_autorizacion(result[9].toString());
            }
            if (result[10] != null) {
                register.setTransaccion(result[10].toString());
            }
            if (result[11] != null) {
                register.setAutorizacion(result[11].toString());
            }
            if (result[12] != null) {
                register.setDestino(result[12].toString());
            }
            registers.add(register);
        }        
        return registers;
    }

    @Override
    public List<AccountStatus> getAccounStatus(String initDate, String endDate, Boolean forAmex) {

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Date initialDate = new Date();
        Date finalDate = new Date();
        List<AccountStatus> accountStatuses = new ArrayList<>();

        String queryString = "select \n"
                + "			tb.id_bitacora, \n"
                + "			tb.id_usuario, \n"
                + "			tu.usr_login, \n"
                + "			tu.usr_nombre, \n"
                + "			tu.usr_apellido, \n"
                + "			tb.tarjeta_compra, \n"
                + "			tb.bit_fecha, \n"
                + "			time( tb.bit_hora) bit_hora, \n"
                + "			tb.bit_concepto, \n"
                + "			tb.bit_codigo_error, \n"
                + "			tb.bit_status,\n"
                + "			tb.bit_cargo, \n"
                + "			bp.Cargo cargo_Aplicado, \n"
                + "			bp.comision Comision, \n"
                + "			bp.tarjeta num_Tarjeta_Autorizacion,\n"
                + "			bp.TarjetaIAVE num_TarjetaIAVE_autorizacion, \n"
                + "			bp.transaccion transaccion,\n"
                + "			bp.autorizacion Autorizacion, \n"
                + "			biv.autono num_TarjetaIAVE_autorizacion_1, \n"
                + "			btae.v_telcelid numTae \n"
                + "		from t_bitacora tb \n"
                + "			inner join t_usuarios tu on tu.id_usuario = tb.id_usuario \n"
                + "			left join t_bitacoraProsa bp on bp.id_bitacora = tb.id_bitacora \n"
                + "			left join t_bitacoraiave biv on biv.id_bitacora = tb.id_bitacora \n"
                + "			left join t_bitacoratae btae on btae.id_bitacora = tb.id_bitacora \n"
                + "		where tb.bit_fecha between :initDate and :endDate\n"
                + "			and tb.bit_status != 50 order by tb.id_bitacora";

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            initialDate = (Date) format.parse(initDate);
            finalDate = (Date) format.parse(endDate);
        } catch (ParseException ex) {
            Logger.getLogger(ConciliationDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        Query query = session.createSQLQuery(queryString);

        query.setParameter("initDate", initialDate);
        query.setParameter("endDate", finalDate);

        List<Object[]> list = query.list();

        for (Object[] result : list) {            
            AccountStatus accountStatus = new AccountStatus();
            if (result[0] != null) {
                accountStatus.setId_bitacora(new Long(result[0].toString()));
            }
            if (result[1] != null) {
                accountStatus.setId_usuario(new Long(result[1].toString()));
            }
            if (result[2] != null) {
                accountStatus.setUsr_login(result[2].toString());
            }
            if (result[3] != null) {
                accountStatus.setUsr_nombre(result[3].toString());
            }
            if (result[4] != null) {
                accountStatus.setUsr_apellido(result[4].toString());
            }
            if (result[5] != null) {
                String tarjeta = null;
		String tar2 = null;
                String cardNumber = Crypto.escondeDigitos(result[5].toString());
                tarjeta = Crypto.getCardNumber(result[5].toString());                
                if (forAmex){
                    accountStatus.setTarjeta_compra(tarjeta);
                }else{                
                    if (tarjeta.isEmpty() || tarjeta == null) {
                        tar2 = "S/T";
                    } else if (tarjeta.equals("null")) {
                        tar2 = "";
                    } else {
                        switch (tarjeta.length()) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                tar2 = "************" + tarjeta;
                                break;
                            case 15:
                                tar2 = tarjeta.substring(0, 5) + "******" + tarjeta.substring(11);
                                break;
                            case 16:
                                tar2 = tarjeta.substring(0, 5) + "******" + tarjeta.substring(12);
                                break;
                            default:
                                tar2 = "";
                                break;
                        }
                    }
                    accountStatus.setTarjeta_compra(tar2);
                }
            }
            if (result[6] != null) {
                accountStatus.setBit_fecha(result[6].toString());
            }
            if (result[7] != null) {
                accountStatus.setBit_hora(result[7].toString());
            }
            if (result[8] != null) {
                accountStatus.setBit_concepto(result[8].toString());
            }
            if (result[9] != null) {
                accountStatus.setBit_codigo_error(Integer.parseInt(result[9].toString()));
            }            
            if (result[10] != null) {
                accountStatus.setBit_status(Integer.parseInt(result[10].toString()));
            }
            if (result[11] != null) {
                accountStatus.setBit_cargo(Double.parseDouble(result[11].toString()));
            }
            if (result[12] != null) {
                accountStatus.setCargo_Aplicado(Double.parseDouble(result[12].toString()));
            }
            if (result[13] != null) {
                accountStatus.setComision(Double.parseDouble(result[13].toString()));
            }
            if (result[14] != null) {
                accountStatus.setNum_Tarjeta_Autorizacion(result[14].toString());
            }
            if (result[15] != null) {
                accountStatus.setNum_TarjetaIAVE_autorizacion(result[15].toString());
            }
            if (result[16] != null) {
                accountStatus.setTransaccion(result[16].toString());
            }
            if (result[17] != null) {
                accountStatus.setAutorizacion(result[17].toString());
            }
            if (result[18] != null) {
                accountStatus.setNum_TarjetaIAVE_autorizacion_1(result[18].toString());
            }
            if (result[19] != null) {
                accountStatus.setNumTae(result[19].toString());
            }
            accountStatuses.add(accountStatus);
        }

        return accountStatuses;

    }

    @Override
    public List<AmexTransaction> getAmexTransactions(String date) {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        List<AmexTransaction> amexTransactions = new ArrayList<>();
        
        String queryString = "select * \n" +
                            "from ecommerce.Transaccion  \n" +
                            "where trim(transaccionApprovalCode) != '' \n" +
                            "and transaccionProcesada = 'N' \n" +
                            "and date_format(transaccionDate,'%Y-%m-%d') = :date";
        
        Query query = session.createSQLQuery(queryString);
              
        query.setParameter("date", date);
        
        List<Object[]> list = query.list();

        for (Object[] result : list) {            
            AmexTransaction amexTransaction = new AmexTransaction();
            if (result[0] != null) {
                amexTransaction.setTransaccionIdn(new Long(result[0].toString()));
            }
            if (result[1] != null) {
                amexTransaction.setTransaccionId(result[1].toString());
            }
            if (result[2] != null) {
                amexTransaction.setTransaccionLocalDateTime(result[2].toString());
            }
            if (result[3] != null) {
                amexTransaction.setTransaccionDsc(result[3].toString());
            }
            if (result[4] != null) {
                amexTransaction.setTransaccionUsuario(result[4].toString());
            }
            if (result[5] != null) {
                amexTransaction.setTransaccionTarjeta(result[5].toString());
            }
            if (result[6] != null) {
                amexTransaction.setTransaccionExpira(result[6].toString());
            }
            if (result[7] != null) {
                amexTransaction.setTransaccionMonto(result[7].toString());
            }
            if (result[8] != null) {
                amexTransaction.setTransaccionCid(result[8].toString());
            }
            if (result[9] != null) {
                amexTransaction.setTransaccionNombres(result[9].toString());
            }            
            if (result[10] != null) {
                amexTransaction.setTransaccionApellidos(result[10].toString());
            }
            if (result[11] != null) {
                amexTransaction.setTransaccionDireccion(result[11].toString());
            }
            if (result[12] != null) {
                amexTransaction.setTransaccionTelefono(result[12].toString());
            }
            if (result[13] != null) {
                amexTransaction.setTransaccionEmail(result[13].toString());
            }
            if (result[14] != null) {
                amexTransaction.setTransaccionCp(result[14].toString());
            }
            if (result[15] != null) {
                amexTransaction.setMensajeId(result[15].toString());
            }
            if (result[16] != null) {
                amexTransaction.setTransaccionTipo(result[16].toString());
            }
            if (result[17] != null) {
                amexTransaction.setTransaccionSubTipo(result[17].toString());
            }
            if (result[18] != null) {
                amexTransaction.setTransaccionReversal(result[18].toString());
            }
            if (result[19] != null) {
                amexTransaction.setTransaccionReversalIdn(result[19].toString());
            }
            if (result[20] != null) {
                amexTransaction.setTransaccionAccReference(result[20].toString());
            }
            if (result[21] != null) {
                amexTransaction.setTransaccionApprovalCode(result[21].toString());
            }
            if (result[22] != null) {
                amexTransaction.setTransaccionDate(result[22].toString());
            }
            if (result[23] != null) {
                amexTransaction.setTransaccionProcesada(result[23].toString());
            }
            if (result[24] != null) {
                amexTransaction.setTransaccionTipoTarjeta(result[24].toString());
            }
            if (result[25] != null) {
                amexTransaction.setTransaccionAcceptorId(result[25].toString());
            }
            if (result[26] != null) {
                amexTransaction.setPlazo(Integer.parseInt(result[26].toString()));
            }
            amexTransactions.add(amexTransaction);
        }

        return amexTransactions;
        
    }
    
    @Override
    public void updateAmexTransaction(AmexTransaction amexTransaction) {                       
        try {
            session = ecommerceSessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = ecommerceSessionFactory.openSession();            
        }
        Transaction tx = session.beginTransaction();
        
        session.update(amexTransaction); 
        
        tx.commit();
        
        session.close();
    }
    
}
