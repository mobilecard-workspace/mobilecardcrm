package com.addcel.mx.mobilecardcrm.model.admin;

/**
 *
 * @author Lasar-Soporte
 */
public class ClientSearch {
    
    private Long id_usuario;
    private String usr_login;
    private String usr_fecha_registro_inicial;
    private String usr_fecha_registro_final;
    private String usr_nombre;
    private String usr_apellido;
    private String usr_telefono;
    
    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getUsr_login() {
        return usr_login;
    }

    public void setUsr_login(String usr_login) {
        this.usr_login = usr_login;
    }

    public String getUsr_fecha_registro_inicial() {
        return usr_fecha_registro_inicial;
    }

    public void setUsr_fecha_registro_inicial(String usr_fecha_registro_inicial) {
        this.usr_fecha_registro_inicial = usr_fecha_registro_inicial;
    }

    public String getUsr_fecha_registro_final() {
        return usr_fecha_registro_final;
    }

    public void setUsr_fecha_registro_final(String usr_fecha_registro_final) {
        this.usr_fecha_registro_final = usr_fecha_registro_final;
    }

    public String getUsr_nombre() {
        return usr_nombre;
    }

    public void setUsr_nombre(String usr_nombre) {
        this.usr_nombre = usr_nombre;
    }

    public String getUsr_apellido() {
        return usr_apellido;
    }

    public void setUsr_apellido(String usr_apellido) {
        this.usr_apellido = usr_apellido;
    }

    public String getUsr_telefono() {
        return usr_telefono;
    }

    public void setUsr_telefono(String usr_telefono) {
        this.usr_telefono = usr_telefono;
    }
}
