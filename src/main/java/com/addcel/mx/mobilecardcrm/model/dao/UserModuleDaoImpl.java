/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.mobilecardcrm.model.dao;

import com.addcel.mx.mobilecardcrm.model.admin.UserModule;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("userModuleDao")
@Transactional
public class UserModuleDaoImpl implements UserModuleDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addUserModule(UserModule userModule) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }        
        Transaction tx = session.beginTransaction();        
        session.saveOrUpdate(userModule);
        tx.commit();        
        session.close();                        
    }

    @Override
    public void updateUserModule(UserModule userModule) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }             
        Transaction tx = session.beginTransaction();        
        session.update(userModule);
        tx.commit();        
        session.close();                
    }

    @Override
    public List<UserModule> listUserModules() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(UserModule.class);
        List userModules = cr.list();
        
        return userModules;
    }

    @Override
    public List<UserModule> getUserModuleByUserId(Long id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(UserModule.class);
        Criterion id_criterion = Restrictions.eq("id_user", id);
        criteria.add(id_criterion);
        
        List<UserModule> userModules = criteria.list();
        
        return userModules;
    }

    @Override
    public void removeUserModule(UserModule userModule) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }                       
        UserModule module = session.load(UserModule.class, userModule);
        Transaction tx = session.beginTransaction();        
        session.delete(module);
        tx.commit();        
        session.close();                
    }
    
}
