package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.admin.LockedCard;
import com.addcel.mx.mobilecardcrm.model.admin.LockedImei;
import com.addcel.mx.mobilecardcrm.service.LockService;
import com.addcel.mx.mobilecardcrm.utils.Crypto;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author wsolano
 */
@RestController
@RequestMapping(value="/admin/bloqueo")
public class LockController {

    @Autowired
    private LockService lockService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewLocks(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/locks");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView detailLockViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/locksTemplate");                         
        return mav;
    }
    
    @RequestMapping(value="/tarjeta", method=RequestMethod.GET)
    public ModelAndView homeAdminViewLockedCards(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/lockedCards");                         
        return mav;
    }
    
    @RequestMapping(value="/imei", method=RequestMethod.GET)
    public ModelAndView homeAdminViewLockedImeis(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/lockedImeis");                         
        return mav;
    }
    
    @RequestMapping(value = "/listLockedCard", method = RequestMethod.GET)
    public ResponseEntity<List<LockedCard>> listAllLockedCards() throws Exception {                                
                
        List<LockedCard> lockedCards = new ArrayList<>();                                             
        
        lockedCards = lockService.listLockedCards();                   
        if(lockedCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        for (LockedCard card : lockedCards){
            //System.out.println("Tarjeta bloqueada " + card.getTarjeta() + " desencriptada " + Crypto.getCardNumber(card.getTarjeta()));
            card.setTarjeta(Crypto.getCardNumber(card.getTarjeta()));
        }
        
        return new ResponseEntity<>(lockedCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listLockedCard", method = RequestMethod.POST)
    public ResponseEntity<List<LockedCard>> getLockedCardsByPage() throws Exception {                                
                
        List<LockedCard> lockedCards = new ArrayList<>();                                             
        
        lockedCards = lockService.getLockedCards();                   
        if(lockedCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(lockedCards, HttpStatus.OK);
    }
    /*search card*/
    @RequestMapping(value = "/searchLockedCard", method = RequestMethod.POST)
    public ResponseEntity<List<LockedCard>> getSearchLockedCard(@RequestBody String numcard) throws Exception {                                
                
        List<LockedCard> lockedCards = new ArrayList<>();                                                            
        String encryptedCardNumber = numcard;
        if (Crypto.isNumeric(numcard)){
            encryptedCardNumber = Crypto.aesEncrypt(numcard);            
        }
        lockedCards = lockService.getSearchLockedCard(encryptedCardNumber);         
        if(lockedCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        for (LockedCard card : lockedCards){
            String cardNumber = card.getTarjeta();
            if (!Crypto.isNumeric(cardNumber)){
                cardNumber = Crypto.getCardNumber(card.getTarjeta());            
                card.setTarjeta(cardNumber);
            }                        
        }
        return new ResponseEntity<>(lockedCards, HttpStatus.OK);
    }
    /**/
    /* search imei*/
    @RequestMapping(value = "/searchLockedImei", method = RequestMethod.POST)
    public ResponseEntity<List<LockedImei>> getSearchLockedImei(@RequestBody String numImei) throws Exception {                                
                
        List<LockedImei> lockedImeis = new ArrayList<>();                                             
        
       
        
        lockedImeis = lockService.getSearchLockedImei(numImei);               
        if(lockedImeis.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(lockedImeis, HttpStatus.OK);
    }
    /**/
    
    @RequestMapping(value = "/totalLockedCard", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfLockedCards() throws Exception {                                                                                                            
        
        Long totalLockedCards = lockService.getNumberLockedCardRows();
        return new ResponseEntity<>(totalLockedCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listLockedImei", method = RequestMethod.GET)
    public ResponseEntity<List<LockedImei>> listAllLockedImeis() throws Exception {                                
                
        List<LockedImei> lockedCards = new ArrayList<>();                                             
        
        lockedCards = lockService.listLockedImeis();                   
        if(lockedCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(lockedCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listLockedImei", method = RequestMethod.POST)
    public ResponseEntity<List<LockedImei>> getLockedImeisByPage() throws Exception {                                
                
        List<LockedImei> lockedCards = new ArrayList<>();                                                     
        
        lockedCards = lockService.getLockedImeis();                   
        if(lockedCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(lockedCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/totalLockedImei", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfLockedImeis() throws Exception {                                                                                                            
        
        Long totalLockedImeis = lockService.getNumberLockedImeiRows();
        return new ResponseEntity<>(totalLockedImeis, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/updateLockedImei", method = RequestMethod.POST)
    public ResponseEntity<List<LockedImei>> updateLockedImei(@RequestBody LockedImei lockedImei) {                                                                                                            
        
        lockService.addLockedImei(lockedImei);
        List<LockedImei> lockedImeis = lockService.getLockedImeis();
        
        return new ResponseEntity<>(lockedImeis, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/updateLockedCard", method = RequestMethod.POST)
    public ResponseEntity<?> updateLockedCard(@RequestBody LockedCard lockedCard) {                                                                                                            
        
        lockedCard.setTarjeta(Crypto.aesEncrypt(lockedCard.getTarjeta()));
        
        lockService.addLockedCard(lockedCard);        
        
        return new ResponseEntity<>( HttpStatus.OK);
    }
    
}
