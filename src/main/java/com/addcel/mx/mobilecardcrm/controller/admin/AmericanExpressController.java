package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.admin.AmexTransaction;
import com.addcel.mx.mobilecardcrm.model.admin.Client;
import com.addcel.mx.mobilecardcrm.model.admin.ClientCard;
import com.addcel.mx.mobilecardcrm.model.admin.ClientSearch;
import com.addcel.mx.mobilecardcrm.model.admin.ClientStatus;
import com.addcel.mx.mobilecardcrm.model.admin.LockedCard;
import com.addcel.mx.mobilecardcrm.service.AmericanExpressService;
import com.addcel.mx.mobilecardcrm.service.ClientCardService;
import com.addcel.mx.mobilecardcrm.service.ClientCardServiceImpl;
import com.addcel.mx.mobilecardcrm.service.ClientService;
import com.addcel.mx.mobilecardcrm.service.LockService;
import com.addcel.mx.mobilecardcrm.utils.Crypto;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author oskar.cahuenas
 */

@RestController
@RequestMapping(value="/admin/american_express")
public class AmericanExpressController {
	
	private static final DateFormat TABLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    
    @Autowired
    private AmericanExpressService americanExpressService;
    
    /**
     * Index del módulo American Express
     * 
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/american_express/index");                         
        return mav;
    }
    
    /**
     * Pantalla para la pestaña de operaciones no procesadas. Presenta caja de texto para ingresar fehca
     * 
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/pages/non_processed_operations", method=RequestMethod.GET)
    public ModelAndView nonProcessedOperations(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/american_express/nonProcessedOperations");                         
        return mav;
    }
    
    /**
     * Pantalla para la pestaña reverso de operaciones. Presenta caja de texto para ingresar los ids de transacción
     * 
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/pages/reverse_operations", method=RequestMethod.GET)
    public ModelAndView reverseOperations(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/american_express/reverseOperations");                         
        return mav;
    }
    
    
    @RequestMapping(value = "/services/non_processed_operations", method = RequestMethod.POST)
    @ResponseBody
    public List<AmexTransaction> getNonProcessedAmexTransactions(@RequestBody String transactionsDate) throws Exception {
    	List<AmexTransaction> transactions = americanExpressService.getNonProcessedTransactions(
    			TABLE_DATE_FORMAT.parse(transactionsDate.replace("\"", "")));
    	return transactions;
    }
    

    @RequestMapping(value = "/services/reverse_operations", method = RequestMethod.PUT)
    @ResponseBody
    public void reverseTransactions(@RequestBody List<Long> transactionIds) throws Exception {
    	americanExpressService.reverseTransactions(transactionIds);
    }
    
}
