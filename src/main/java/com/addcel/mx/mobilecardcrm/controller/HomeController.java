package com.addcel.mx.mobilecardcrm.controller;

import com.addcel.mx.mobilecardcrm.model.UserForAuth;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView homePage(HttpServletRequest request, HttpServletResponse response) {

        ModelAndView mav = new ModelAndView("login");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        if (!authentication.getPrincipal().equals("anonymousUser")) {
            System.out.println("User: " + SecurityContextHolder.getContext().getAuthentication().getPrincipal());

            UserForAuth user = (UserForAuth) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            System.out.println("Auth " + user.getAuthorities().toString());
            
            mav = new ModelAndView("admin/index");
            /*
            List<GrantedAuthority> roles = user.getAuthorities();            
            for (GrantedAuthority authority : roles) {
                if ("ROLE_ADMIN".equals(authority.getAuthority())) {
                    mav = new ModelAndView("admin/index");
                } else if ("ROLE_PROVEEDOR".equals(authority.getAuthority())) {
                    mav = new ModelAndView("provider/index");
                }
            }
            */
        }

        return mav;
    }

    @RequestMapping(value = "/proveedor", method = RequestMethod.GET)
    public ModelAndView homeProviderView(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView mav = new ModelAndView("provider/index");
        return mav;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView homeAdminView(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView mav = new ModelAndView("admin/index");
        return mav;
    }
}
