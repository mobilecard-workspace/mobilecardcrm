package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.User;
import com.addcel.mx.mobilecardcrm.model.UserForAuth;
import com.addcel.mx.mobilecardcrm.model.admin.Module;
import com.addcel.mx.mobilecardcrm.model.admin.UserModule;
import com.addcel.mx.mobilecardcrm.service.ModuleService;
import com.addcel.mx.mobilecardcrm.service.UserModuleService;
import com.addcel.mx.mobilecardcrm.service.UserService;
import com.addcel.mx.mobilecardcrm.utils.PasswordEncrypter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value="/admin/usuario")
public class UserController {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserModuleService userModuleService;
    
    @Autowired
    private ModuleService moduleService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/user");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView detailUserViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/userTemplate");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() throws Exception {                                
                
        List<User> users = new ArrayList<>();                                             
        
        users = userService.listUsers();                   
        if(users.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        /*
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();        
        UserForAuth userAuthenticated = (UserForAuth) authentication.getPrincipal();
        for (User user : users){
            if (user.getId() == userAuthenticated.getId()){
                users.remove(user);
                break;
            }
        }
        */
        return new ResponseEntity<>(users, HttpStatus.OK);
    }           
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<List<User>> editUser(@RequestBody User user) throws Exception {                                
                            
        PasswordEncrypter passwordEncrypter = new PasswordEncrypter();                
        
        User userAux = userService.getUserById(user.getId());
        
        if (!user.getPassword().equals(userAux.getPassword())){
            user.setPassword(passwordEncrypter.getPassEncrypted(user.getPassword()));
        }                
                        
        userService.updateUser(user);   
        
        for (UserModule userModule : user.getUserModules()){
            boolean userModuleExist = false;
            for (UserModule module : userAux.getUserModules()){
                if (module.getId_user() == userModule.getId_user() && module.getId_module() == userModule.getId_module()){
                    userModuleExist = true;
                    break;
                }                
            }
            if (!userModuleExist){
                userModuleService.addUserModule(userModule);
            }
        }
        
        for (UserModule module : userAux.getUserModules()){
            boolean userModuleExist = false;
            for (UserModule userModule : user.getUserModules()){
                if (module.getId_user() == userModule.getId_user() && module.getId_module() == userModule.getId_module()){
                    userModuleExist = true;
                    break;
                }                
            }
            System.out.println("User Module exist " + userModuleExist);
            if (!userModuleExist){
                userModuleService.removeUserModule(module);
            }
        }
        
        List<User> users = userService.listUsers();
        /*
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        UserForAuth userAuthenticated = (UserForAuth) authentication.getPrincipal();
        for (User u : users){
            if (u.getId() == userAuthenticated.getId()){
                users.remove(u);
                break;
            }
        }
        */
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<List<User>> addUser(@RequestBody User user) throws Exception {                                                
        
        PasswordEncrypter passwordEncrypter = new PasswordEncrypter();                
        
        user.setPassword(passwordEncrypter.getPassEncrypted(user.getPassword()));
        
        userService.addUser(user);                               
        
        for (UserModule userModule : user.getUserModules()){
            userModule.setId_user(user.getId());
            userModuleService.addUserModule(userModule);
        }
        
        List<User> users = userService.listUsers();        
        /*
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        UserForAuth userAuthenticated = (UserForAuth) authentication.getPrincipal();
        for (User u : users){
            if (u.getId() == userAuthenticated.getId()){
                users.remove(u);
                break;
            }
        }
        */
        return new ResponseEntity<>(users, HttpStatus.OK);
    }              
    
    @RequestMapping(value = "/permission", method = RequestMethod.GET)
    public ResponseEntity<List<UserModule>> listUserPermission() throws Exception {                                
                
        List<UserModule> userModules = new ArrayList<>();                                             
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        UserForAuth user = (UserForAuth) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        userModules = userModuleService.getUserModuleByUserId(user.getId());                   
        
        if(userModules.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(userModules, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/modules", method = RequestMethod.GET)
    public ResponseEntity<List<Module>> listUserModules() throws Exception {                                
                
        List<Module> userModules = new ArrayList<>();                                                      
        userModules = moduleService.listModules();               
        
        if(userModules.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(userModules, HttpStatus.OK);
    }
}
