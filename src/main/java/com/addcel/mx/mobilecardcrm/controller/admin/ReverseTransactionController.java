package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransaction;
import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransactionSearch;
import com.addcel.mx.mobilecardcrm.service.ReverseTransactionServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author wsolano
 */
@RestController
@RequestMapping(value="/admin/reverso")
public class ReverseTransactionController {
    
    @Autowired
    private ReverseTransactionServiceImpl reverseTransactionService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/reverseTransaction");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView reverseTransactionDetailViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/reverseTransactionTemplate");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<ReverseTransaction>> listAllReverseTransactions() throws Exception {                                
                
        List<ReverseTransaction> reverseTransactions = new ArrayList<>();                                             
        
        reverseTransactions = reverseTransactionService.listReverseTransactions();                   
        if(reverseTransactions.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(reverseTransactions, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<ReverseTransaction>> getReverseTransactionsByPage(@RequestBody int [] data) throws Exception {                                
                
        List<ReverseTransaction> reverseTransactions = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        reverseTransactions = reverseTransactionService.getPageReverseTransactions(data[0], data[1]);                   
        if(reverseTransactions.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(reverseTransactions, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfReverseTransactions() throws Exception {                                                                                                            
        
        Long totalReverseTransactions = reverseTransactionService.getNumberRows();
        return new ResponseEntity<>(totalReverseTransactions, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/searchReverse", method = RequestMethod.POST)
    public ResponseEntity<List<ReverseTransaction>> searchClients(@RequestBody ReverseTransactionSearch reverseTransactionSearch) throws Exception {                                
                
        List<ReverseTransaction> reverseTransactions = new ArrayList<>();                                                             
        
        reverseTransactions = reverseTransactionService.searchReverseTransactions(reverseTransactionSearch);
        if(reverseTransactions.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(reverseTransactions, HttpStatus.OK);
    }
    
}
