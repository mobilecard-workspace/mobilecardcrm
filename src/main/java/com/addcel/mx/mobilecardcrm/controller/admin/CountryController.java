package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.admin.Country;
import com.addcel.mx.mobilecardcrm.service.CountryServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lasar-Soporte
 */

@RestController
@RequestMapping(value="/admin/pais")
public class CountryController {
    
    @Autowired
    private CountryServiceImpl countryService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/country");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Country>> listAllCountries() throws Exception {                                
                
        List<Country> countries = new ArrayList<>();                                             
        
        countries = countryService.listCountries();                   
        if(countries.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(countries, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<Country>> getCountriesByPage(@RequestBody int [] data) throws Exception {                                
                
        List<Country> countries = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        countries = countryService.getPageCountries(data[0], data[1]);                   
        if(countries.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(countries, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfCountries() throws Exception {                                                                                                            
        
        Long totalCountries = countryService.getNumberRows();
        return new ResponseEntity<>(totalCountries, HttpStatus.OK);
    }
    
    
}
