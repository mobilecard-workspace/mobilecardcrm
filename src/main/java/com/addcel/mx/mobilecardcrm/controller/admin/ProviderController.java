package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.admin.Provider;
import com.addcel.mx.mobilecardcrm.model.admin.ProviderStatus;
import com.addcel.mx.mobilecardcrm.service.ProviderServiceImpl;
import com.addcel.mx.mobilecardcrm.service.ProviderStatusServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value = "/admin/provider")
public class ProviderController {
    
    @Autowired
    private ProviderServiceImpl providerService;
    
    @Autowired
    private ProviderStatusServiceImpl providerStatusService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/provider");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView providerDetailViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/providerTemplate");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Provider>> listAllProviders() throws Exception {                                
                
        List<Provider> providers = new ArrayList<>();                                             
        
        providers = providerService.listProviders();                   
        if(providers.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<Provider>> getProvidersByPage(@RequestBody int [] data) throws Exception {                                
                
        List<Provider> providers = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        providers = providerService.getPageProviders(data[0], data[1]);                   
        if(providers.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/providerbyid", method = RequestMethod.POST)
    public ResponseEntity<Provider> getProviderByID(@RequestBody int id) throws Exception {                                
                
                                           
        Provider provider = providerService.getProviderById(id);                   
        if(provider.equals(null)){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(provider, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfProviders() throws Exception {                                                                                                            
        
        Long totalProviders = providerService.getNumberRows();
        return new ResponseEntity<>(totalProviders, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<List<Provider>> editProvider(@RequestBody Provider provider) throws Exception {                                
        
        providerService.updateProvider(provider);                               
        
        List<Provider> providers = providerService.listProviders();
        
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<List<Provider>> addProvider(@RequestBody Provider provider) throws Exception {                                
        
        List<Long> providersId = providerService.getProvidersId();
        
        Long idProvider = new Long(0);
        while (providersId.contains(idProvider)){
            idProvider = idProvider + 1;
        }
        provider.setId_proveedor(idProvider);
        
        providerService.addProvider(provider);
        
        List<Provider> providers = providerService.listProviders();
                
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listProviderStatuses", method = RequestMethod.GET)
    public ResponseEntity<List<ProviderStatus>> listProviderStatus() throws Exception {                                
                
        List<ProviderStatus> providerStatuses = providerStatusService.listProviderStatus();                                             
                       
        if(providerStatuses.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(providerStatuses, HttpStatus.OK);
    }
    
}
