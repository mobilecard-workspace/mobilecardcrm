package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.admin.Client;
import com.addcel.mx.mobilecardcrm.model.admin.ClientCard;
import com.addcel.mx.mobilecardcrm.model.admin.ClientSearch;
import com.addcel.mx.mobilecardcrm.model.admin.ClientStatus;
import com.addcel.mx.mobilecardcrm.model.admin.LockedCard;
import com.addcel.mx.mobilecardcrm.service.ClientCardService;
import com.addcel.mx.mobilecardcrm.service.ClientCardServiceImpl;
import com.addcel.mx.mobilecardcrm.service.ClientService;
import com.addcel.mx.mobilecardcrm.service.LockService;
import com.addcel.mx.mobilecardcrm.utils.Crypto;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author wsolano
 */

@RestController
@RequestMapping(value="/admin/cliente")
public class ClientController {
    
    @Autowired
    private ClientService clientService;
    
    @Autowired
    private ClientCardServiceImpl clientCardService;
    
    @Autowired
    private LockService lockService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/client");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView detailClientViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/clientTemplate");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Client>> listAllClients() throws Exception {                                
                
        List<Client> clients = new ArrayList<>();                                             
        
        clients = clientService.listClients();                   
        if(clients.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<Client>> getClientsByPage(@RequestBody int [] data) throws Exception {                                
                
        List<Client> clients = new ArrayList<>();                                             
        
        //System.out.println("page " + data[0]);
        //System.out.println("max " + data[1]);
        
        clients = clientService.getPageClients(data[0], data[1]);                   
        if(clients.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/clientbyimei", method = RequestMethod.POST)
    public ResponseEntity<Client> getClientsByImei(@RequestBody String imei) throws Exception {                                
                
                                           
        Client client = clientService.getClientByImei(imei);                   
        if(client == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
                
        client.setUsr_tdc_numero(Crypto.escondeDigitos(client.getUsr_tdc_numero()));
                
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @RequestMapping(value = "/clientbycard", method = RequestMethod.POST)
    public ResponseEntity<Client> getClientsByCard(@RequestBody String tarjeta) throws Exception {                                
        
        String encryptedCardNumber = tarjeta;
        if (Crypto.isNumeric(tarjeta)){            
            encryptedCardNumber = Crypto.aesEncrypt(tarjeta);                        
        }
        
        Client client = clientService.getClientByCard(encryptedCardNumber);        
        if(client == null){
            ClientCard clientCard = clientCardService.getClientCardByNumberCard(encryptedCardNumber);
            if (clientCard != null){
                client = clientService.getClientById(clientCard.getIdusuario());
            }else{
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
            }
        }
        
        if (!Crypto.isNumeric(client.getUsr_tdc_numero())){
            client.setUsr_tdc_numero(tarjeta);
        }
        
        return new ResponseEntity<>(client, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/clientbyid", method = RequestMethod.POST)
    public ResponseEntity<Client> getClientsById(@RequestBody Long id) throws Exception {                                                
        
        Client client = clientService.getClientById(id);        
        if(client == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        if (!Crypto.isNumeric(client.getUsr_tdc_numero())){
            client.setUsr_tdc_numero(Crypto.escondeDigitos(client.getUsr_tdc_numero()));
        }
        
        return new ResponseEntity<>(client, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfClients() throws Exception {                                                                                                            
        
        Long totalClients = clientService.getNumberRows();
        return new ResponseEntity<>(totalClients, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<Client> editClient(@RequestBody Client client) throws Exception {                                
        /*
        System.out.println("Registro " + client.getUsr_fecha_registro());
        System.out.println("Nacimiento " + client.getUsr_fecha_nac());
        
        System.out.println("numero de tarjeta " + client.getUsr_tdc_numero());
        System.out.println("numero de tarjeta numerico " + Crypto.isNumeric(client.getUsr_tdc_numero()));
        */
        if (Crypto.isNumeric(client.getUsr_tdc_numero())){
            String encryptedCardNumber = Crypto.aesEncrypt(client.getUsr_tdc_numero());
            client.setUsr_tdc_numero(encryptedCardNumber);
        }else{
            Client clientAux = clientService.getClientById(client.getId_usuario());
            client.setUsr_tdc_numero(clientAux.getUsr_tdc_numero());
        }               
        
        if (Crypto.isNumeric(client.getUsr_pwd())){            
            client.setUsr_pwd(Crypto.aesEncrypt(client.getUsr_pwd()));
        }
        
        clientService.updateClient(client);                               
        
        Client clientToResponse = clientService.getClientById(client.getId_usuario());        
        String cardNumber = Crypto.escondeDigitos(clientToResponse.getUsr_tdc_numero());                                    
        clientToResponse.setUsr_tdc_numero(cardNumber);
        
        return new ResponseEntity<>(clientToResponse, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Client> addClient(@RequestBody Client client) throws Exception {                                
        
        if (Crypto.isNumeric(client.getUsr_tdc_numero())){            
            client.setUsr_tdc_numero(Crypto.aesEncrypt(client.getUsr_tdc_numero()));
        }
        
        if (Crypto.isNumeric(client.getUsr_pwd())){            
            client.setUsr_pwd(Crypto.aesEncrypt(client.getUsr_pwd()));
        }                        
        
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("Fecha de registro " + timestamp);                
        client.setUsr_fecha_registro(timestamp);
        
        clientService.addClient(client);                               
                
        return new ResponseEntity<>(client, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/searchClients", method = RequestMethod.POST)
    public ResponseEntity<List<Client>> searchClients(@RequestBody ClientSearch clientSearch) throws Exception {                                
                
        List<Client> clients = new ArrayList<>();                                                             
        
        clients = clientService.searchClients(clientSearch); 
        //System.out.println("CLients size " + clients.size());
        if(clients.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        for (Client client : clients){            
            //System.out.println("Tarjeta " + client.getUsr_tdc_numero() + 
              //      " desencriptada " + Crypto.getCardNumber(client.getUsr_tdc_numero()));
            String cardNumber = Crypto.escondeDigitos(client.getUsr_tdc_numero());                        
            
            client.setUsr_tdc_numero(cardNumber);
        }
        
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/status/list", method = RequestMethod.GET)
    public ResponseEntity<List<ClientStatus>> listAllClientStatus() throws Exception {                                
                
        List<ClientStatus> clientStatus = new ArrayList<>();                                             
        
        clientStatus = clientService.getClientStatusList();                   
        if(clientStatus.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(clientStatus, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listClientCards", method = RequestMethod.POST)
    public ResponseEntity<List<ClientCard>> getClientCards(@RequestBody Long idusuario) throws Exception {                                
                
        List<ClientCard> clientCards = new ArrayList<>();                                                                     
        
        clientCards = clientCardService.getClientCardsByClientId(idusuario);        
        
        for (ClientCard card : clientCards){
            card.setNumerotarjeta(Crypto.escondeDigitos(card.getNumerotarjeta()));
            card.setCt(Crypto.aesDecrypt(card.getCt()));
            card.setVigencia(Crypto.aesDecrypt(card.getVigencia()));
        }
        
        return new ResponseEntity<>(clientCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/addClientCard", method = RequestMethod.POST)
    public ResponseEntity<List<ClientCard>> addClientCard(@RequestBody ClientCard clientCard) throws Exception {                                
                        
        
        if (clientCard.getIdtarjetasusuario() == null){                            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println("Fecha de registro " + timestamp);                
            clientCard.setFecharegistro(timestamp);
        }
        
        List<ClientCard> clientCards = new ArrayList<>();                                             
        
        if (Crypto.isNumeric(clientCard.getNumerotarjeta())){
            String encryptedCardNumber = Crypto.aesEncrypt(clientCard.getNumerotarjeta());
            clientCard.setNumerotarjeta(encryptedCardNumber);
        }else{
            ClientCard clientCardAux = clientCardService.getClientCardById(clientCard.getIdtarjetasusuario());
            if (clientCardAux == null){
                Client clientAux = clientService.getClientById(clientCard.getIdusuario());
                clientCard.setNumerotarjeta(clientAux.getUsr_tdc_numero());
            }else {
                clientCard.setNumerotarjeta(clientCardAux.getNumerotarjeta());
            }
        }
        clientCard.setVigencia(Crypto.aesEncrypt(clientCard.getVigencia()));
        clientCard.setCt(Crypto.aesEncrypt(clientCard.getCt()));
        /*System.out.println("Fecha de registro client card " + clientCard.getFecharegistro());
        System.out.println("numero tarjeta client card " + clientCard.getNumerotarjeta());
        System.out.println("vigencia tarjeta client card " + clientCard.getVigencia());
        System.out.println("CVV client card " + clientCard.getCt());*/
        clientCardService.addClientCard(clientCard);
        clientCards = clientCardService.getClientCardsByClientId(clientCard.getIdusuario());        
        for (ClientCard card : clientCards){
            card.setNumerotarjeta(Crypto.escondeDigitos(card.getNumerotarjeta()));
            card.setVigencia(Crypto.aesDecrypt(card.getVigencia()));
            card.setCt(Crypto.aesDecrypt(card.getCt()));
        }
        return new ResponseEntity<>(clientCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/blockClient", method = RequestMethod.POST)
    public ResponseEntity<Client> blockClient(@RequestBody Client client) throws Exception {                                
                
        List<ClientCard> clientCards = new ArrayList<>();                                                                     
        List<LockedCard> lockedCards = new ArrayList<>();
        
        clientCards = clientCardService.getClientCardsByClientId(client.getId_usuario());
        lockedCards = lockService.getLockedCards();
        
        boolean cardAlreadyLocked = false;
        
        for (ClientCard card : clientCards){
            for (LockedCard lockedCard : lockedCards){
                if (card.getNumerotarjeta().equals(lockedCard.getTarjeta())){
                    lockedCard.setActivo(1);
                    lockService.addLockedCard(lockedCard);
                    cardAlreadyLocked = true;
                    break;
                }
            }
            if(!cardAlreadyLocked){
                LockedCard lockedCard = new LockedCard();
                lockedCard.setTarjeta(card.getNumerotarjeta());
                lockedCard.setActivo(1);
                lockService.addLockedCard(lockedCard);
            }
        }
        
        if (Crypto.isNumeric(client.getUsr_tdc_numero())){
            String encryptedCardNumber = Crypto.aesEncrypt(client.getUsr_tdc_numero());
            client.setUsr_tdc_numero(encryptedCardNumber);
        }else{
            Client clientAux = clientService.getClientById(client.getId_usuario());
            client.setUsr_tdc_numero(clientAux.getUsr_tdc_numero());
        }               
        
        if (Crypto.isNumeric(client.getUsr_pwd())){            
            client.setUsr_pwd(Crypto.aesEncrypt(client.getUsr_pwd()));
        }
        client.setId_usr_status(3);
        
        clientService.updateClient(client);
         
        return new ResponseEntity<>(client, HttpStatus.OK);
    }
    
}
