package com.addcel.mx.mobilecardcrm.controller.admin;

import com.addcel.mx.mobilecardcrm.model.admin.Bank;
import com.addcel.mx.mobilecardcrm.service.BankService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value="/admin/banco")
public class BankController {
    
    @Autowired
    private BankService bankService;        
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Bank>> listAllBanks() throws Exception {                                
                
        List<Bank> banks = new ArrayList<>();                                             
        
        banks = bankService.listBanks();                   
        if(banks.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(banks, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<Bank>> getBanksByPage(@RequestBody int [] data) throws Exception {                                
                
        List<Bank> banks = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        banks = bankService.getPageBanks(data[0], data[1]);                   
        if(banks.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(banks, HttpStatus.OK);
    }       
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfBanks() throws Exception {                                                                                                            
        
        Long totalBanks = bankService.getNumberRows();
        return new ResponseEntity<>(totalBanks, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<Bank> editBank(@RequestBody Bank bank) throws Exception {                                
        
        bankService.updateBank(bank);                               
        
        return new ResponseEntity<>(bank, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Bank> addBank(@RequestBody Bank bank) throws Exception {                                
        
        bankService.addBank(bank);                               
                
        return new ResponseEntity<>(bank, HttpStatus.OK);
    }
    /*
    @RequestMapping(value = "/searchBanks", method = RequestMethod.POST)
    public ResponseEntity<List<Bank>> searchBanks(@RequestBody BankSearch bankSearch) throws Exception {                                
                
        List<Bank> banks = new ArrayList<>();                                                             
        
        banks = bankService.searchBanks(bankSearch);                   
        if(banks.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(banks, HttpStatus.OK);
    }
    */
}
