package com.addcel.mx.mobilecardcrm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class SessionInterceptor extends HandlerInterceptorAdapter {    

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {        
        if (request.getSession().getAttribute("usuario") == null) {            
            request.setAttribute("mensajeError", "La sesion ha expirado.");
            response.sendRedirect(request.getContextPath() + "/");
            return false;
        }
        return true;
    }

}
