package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.Module;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface ModuleService {
    public void addModule(Module Module);
    public void updateModule(Module Module);
    public List<Module> listModules();
    public Module getModuleById(int id);
    public void removeModule(int id);
}
