package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.UserTag;
import com.addcel.mx.mobilecardcrm.model.admin.UserTagSearch;
import com.addcel.mx.mobilecardcrm.model.dao.UserTagDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class UserTagServiceImpl implements UserTagService{
    
    @Autowired
    private UserTagDao userTagDao;

    @Override
    public void addUserTag(UserTag userTag) {
        userTagDao.addUserTag(userTag);
    }

    @Override
    public void updateUserTag(UserTag userTag) {
        userTagDao.updateUserTag(userTag);
    }

    @Override
    public List<UserTag> listUserTags() {
        return userTagDao.listUserTags();
    }

    @Override
    public UserTag getUserTagById(int id) {
        return userTagDao.getUserTagById(id);
    }

    @Override
    public void removeUserTag(int id) {
        userTagDao.removeUserTag(id);
    }

    @Override
    public List<UserTag> getPageUserTags(int offset, int max) {
        return userTagDao.getPageUserTags(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return userTagDao.getNumberRows();
    }

    @Override
    public List<UserTag> searchTags(UserTagSearch userTagSearch) {
        return userTagDao.searchTags(userTagSearch);
    }        
}
