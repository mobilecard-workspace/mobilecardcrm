package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.UserModule;
import com.addcel.mx.mobilecardcrm.model.dao.UserModuleDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class UserModuleServiceImpl implements UserModuleService{
    
    @Autowired
    private UserModuleDao userModuleDao;

    public void setUserModuleDao(UserModuleDao userModuleDao) {
        this.userModuleDao = userModuleDao;
    }        
    
    @Override
    public void addUserModule(UserModule userModule) {
        userModuleDao.addUserModule(userModule);
    }

    @Override
    public void updateUserModule(UserModule userModule) {
        userModuleDao.updateUserModule(userModule);
    }

    @Override
    public List<UserModule> listUserModules() {
        return userModuleDao.listUserModules();
    }

    @Override
    public List<UserModule> getUserModuleByUserId(Long id) {
        return userModuleDao.getUserModuleByUserId(id);
    }

    @Override
    public void removeUserModule(UserModule userModule) {
        userModuleDao.removeUserModule(userModule);
    }    
    
}
