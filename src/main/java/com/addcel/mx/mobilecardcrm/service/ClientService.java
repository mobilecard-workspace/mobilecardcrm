package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.Client;
import com.addcel.mx.mobilecardcrm.model.admin.ClientSearch;
import com.addcel.mx.mobilecardcrm.model.admin.ClientStatus;

import java.util.List;

/**
 *
 * @author wsolano
 */
public interface ClientService {
    public void addClient(Client client);
    public void updateClient(Client client);
    public List<Client> listClients();
    public Client getClientById(Long id);
    public Client getClientByImei(String imei);
    public Client getClientByCard(String tarjeta);
    public void removeClient(int id);    
    public List<Client> getPageClients(int offset, int max);
    public Long getNumberRows();
    public List<Client> searchClients (ClientSearch clientSearch);
    
    public List<ClientStatus> getClientStatusList();
}
