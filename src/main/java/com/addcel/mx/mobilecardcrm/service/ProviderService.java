package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.Provider;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface ProviderService {
    
    public void addProvider(Provider providers);
    public void updateProvider(Provider providers);
    public List<Provider> listProviders();
    public List<Long> getProvidersId();
    public Provider getProviderById(int id);
    public void removeProvider(int id);    
    public List<Provider> getPageProviders(int offset, int max);
    public Long getNumberRows ();
    
}
