package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.UserModule;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface UserModuleService {
    
    public void addUserModule(UserModule userModule);
    public void updateUserModule(UserModule userModule);
    public List<UserModule> listUserModules();
    public List<UserModule> getUserModuleByUserId(Long id);
    public void removeUserModule(UserModule userModule);
    
}
