package com.addcel.mx.mobilecardcrm.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.addcel.mx.mobilecardcrm.model.admin.AmexTransaction;
import com.addcel.mx.mobilecardcrm.model.dao.AmericanExpressDao;

/**
 *
 * @author oskar.cahuenas
 */
@Service("americanExpressService")
@Transactional("ecommerceTransactionManager")
public class AmericanExpressServiceImpl implements AmericanExpressService {

	@Autowired
	AmericanExpressDao americanExpressDao;

	@Override
	public List<AmexTransaction> getNonProcessedTransactions(Date date) {
		return americanExpressDao.getNonProcessedTransactions(date);
	}

	@Override
	public void reverseTransactions(List<Long> transactionIds) {
		americanExpressDao.reverseTransactions(transactionIds);
	}

}
