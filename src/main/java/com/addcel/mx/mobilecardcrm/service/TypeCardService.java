package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.TypeCard;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface TypeCardService {
    
    public void addTypeCard(TypeCard typeCard);
    public void updateTypeCard(TypeCard typeCard);
    public List<TypeCard> listTypeCards();
    public TypeCard getTypeCardById(int id);
    public void removeTypeCard(int id);    
    public List<TypeCard> getPageTypeCards(int offset, int max);
    public Long getNumberRows ();
    
}
