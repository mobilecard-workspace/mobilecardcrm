package com.addcel.mx.mobilecardcrm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.addcel.mx.mobilecardcrm.model.Role;
import com.addcel.mx.mobilecardcrm.model.User;
import com.addcel.mx.mobilecardcrm.model.dao.RoleDao;
import com.addcel.mx.mobilecardcrm.model.dao.UserDao;

/**
 *
 * @author Lasar-Soporte
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;
    
    @Autowired
    private RoleDao roleDao;


    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }        
    
    @Override
    public void addUser(User user) {
    	// si el usuario que viene no tiene ningún rol se setea el admin
    	Role roleAdmin = roleDao.getRoleByAuthority("ROLE_ADMIN");
    	if (user.getRoles().isEmpty()) {
			user.getRoles().add(roleAdmin);
		}
    	userDao.addUser(user);
        
    }

    @Override
    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Override
    public List<User> listUsers() {
        return userDao.listUsers();
    }

    @Override
    public User getUserById(Long id) {
        return userDao.getUserById(id);
    }

    @Override
    public void removeUser(int id) {
        userDao.removeUser(id);
    }

    @Override
    public User authenticateUser(String username, String password) {
        return userDao.authenticateUser(username, password);
    }
    
}
