package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.Bank;
import com.addcel.mx.mobilecardcrm.model.dao.BankDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class BankServiceImpl implements BankService{
    
    @Autowired
    private BankDao bankDao;

    @Override
    public void addBank(Bank bank) {
        bankDao.addBank(bank);
    }

    @Override
    public void updateBank(Bank bank) {
        bankDao.updateBank(bank);
    }

    @Override
    public List<Bank> listBanks() {
        return bankDao.listBanks();
    }

    @Override
    public Bank getBankById(int id) {
        return bankDao.getBankById(id);
    }

    @Override
    public void removeBank(int id) {
        bankDao.removeBank(id);
    }

    @Override
    public List<Bank> getPageBanks(int offset, int max) {
        return bankDao.getPageBanks(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return bankDao.getNumberRows();
    }
    
}
