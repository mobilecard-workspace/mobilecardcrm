package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.TypeCard;
import com.addcel.mx.mobilecardcrm.model.dao.TypeCardDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class TypeCardServiceImpl implements TypeCardService{
    
    @Autowired
    private TypeCardDao typeCardDao;

    @Override
    public void addTypeCard(TypeCard typeCard) {
        typeCardDao.addTypeCard(typeCard);
    }

    @Override
    public void updateTypeCard(TypeCard typeCard) {
        typeCardDao.updateTypeCard(typeCard);
    }

    @Override
    public List<TypeCard> listTypeCards() {
        return typeCardDao.listTypeCards();
    }

    @Override
    public TypeCard getTypeCardById(int id) {
        return typeCardDao.getTypeCardById(id);
    }

    @Override
    public void removeTypeCard(int id) {
        typeCardDao.removeTypeCard(id);
    }

    @Override
    public List<TypeCard> getPageTypeCards(int offset, int max) {
        return typeCardDao.getPageTypeCards(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return typeCardDao.getNumberRows();
    }
    
}
