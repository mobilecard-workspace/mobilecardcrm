package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.Module;
import com.addcel.mx.mobilecardcrm.model.dao.ModuleDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class ModuleServiceImpl implements ModuleService{
    @Autowired
    private ModuleDao moduleDao;

    public void setModuleDao(ModuleDao ModuleDao) {
        this.moduleDao = ModuleDao;
    }        
    
    @Override
    public void addModule(Module Module) {
        moduleDao.addModule(Module);
    }

    @Override
    public void updateModule(Module Module) {
        moduleDao.updateModule(Module);
    }

    @Override
    public List<Module> listModules() {
        return moduleDao.listModules();
    }

    @Override
    public Module getModuleById(int id) {
        return moduleDao.getModuleById(id);
    }

    @Override
    public void removeModule(int id) {
        moduleDao.removeModule(id);
    }    
    
}
