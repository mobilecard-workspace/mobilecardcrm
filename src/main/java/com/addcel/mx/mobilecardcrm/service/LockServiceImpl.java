package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.LockedCard;
import com.addcel.mx.mobilecardcrm.model.admin.LockedImei;
import com.addcel.mx.mobilecardcrm.model.dao.LockDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wsolano
 */
@Service
public class LockServiceImpl implements LockService{

    @Autowired
    private LockDao lockDao;
    
    @Override
    public void addLockedCard(LockedCard lockedCard) {
        lockDao.addLockedCard(lockedCard);
    }

    @Override
    public void updateLockedCard(LockedCard lockedCard) {
        lockDao.updateLockedCard(lockedCard);
    }

    @Override
    public List<LockedCard> listLockedCards() {
        return lockDao.listLockedCards();
    }

    @Override
    public LockedCard getLockedCardById(int id) {
        return lockDao.getLockedCardById(id);
    }

    @Override
    public void removeLockedCard(int id) {
        lockDao.removeLockedCard(id);
    }

    @Override
    public List<LockedCard> getLockedCards() {
        return lockDao.getLockedCards();
    }

    @Override
    public Long getNumberLockedCardRows() {
        return lockDao.getNumberLockedCardRows();
    }

    @Override
    public void addLockedImei(LockedImei lockedImei) {
        lockDao.addLockedImei(lockedImei);
    }

    @Override
    public void updateLockedImei(LockedImei lockedImei) {
        lockDao.updateLockedImei(lockedImei);
    }

    @Override
    public List<LockedImei> listLockedImeis() {
        return lockDao.listLockedImeis();
    }

    @Override
    public LockedImei getLockedImeiById(int id) {
        return lockDao.getLockedImeiById(id);
    }

    @Override
    public void removeLockedImei(int id) {
        lockDao.removeLockedImei(id);
    }

    @Override
    public List<LockedImei> getLockedImeis() {
        return lockDao.getLockedImeis();
    }

    @Override
    public Long getNumberLockedImeiRows() {
        return lockDao.getNumberLockedImeiRows();
    }

	@Override
	public List<LockedCard> getSearchLockedCard(String numcard) {
		// TODO Auto-generated method stub
		return lockDao.getSearchLockedCard(numcard);
	}

	@Override
	public List<LockedImei> getSearchLockedImei(String numImei) {
		// TODO Auto-generated method stub
		return lockDao.getSearchLockedImei(numImei);
	}
    
}
