package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.Transaction;
import com.addcel.mx.mobilecardcrm.model.admin.TransactionSearch;
import com.addcel.mx.mobilecardcrm.model.admin.TransactionStandard;
import com.addcel.mx.mobilecardcrm.model.dao.TransactionDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wsolano
 */
@Service
public class TransactionServiceImpl implements TransactionService{
    
    @Autowired
    private TransactionDao transactionDao;

    public void setTransactionDao(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }        

    @Override
    public void addTransaction(Transaction transaction) {
        transactionDao.addTransaction(transaction);
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        transactionDao.updateTransaction(transaction);
    }

    @Override
    public List<Transaction> listTransactions() {
        return transactionDao.listTransactions();
    }

    @Override
    public Transaction getTransactionById(int id) {
        return transactionDao.getTransactionById(id);
    }

    @Override
    public void removeTransaction(int id) {
        transactionDao.removeTransaction(id);
    }

    @Override
    public List<Transaction> getPageTransactions(int offset, int max) {
        return transactionDao.getPageTransactions(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return transactionDao.getNumberRows();
    }

    @Override
    public List<TransactionStandard> searchTransactions(TransactionSearch TSearch) {
        return transactionDao.searchTransactions(TSearch);
    }
    
}
