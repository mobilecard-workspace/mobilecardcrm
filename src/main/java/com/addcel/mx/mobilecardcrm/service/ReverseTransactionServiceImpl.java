package com.addcel.mx.mobilecardcrm.service;

import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransaction;
import com.addcel.mx.mobilecardcrm.model.admin.ReverseTransactionSearch;
import com.addcel.mx.mobilecardcrm.model.dao.ReverseTransactionDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wsolano
 */
@Service
public class ReverseTransactionServiceImpl implements ReverseTransactionService{
    
    @Autowired
    private ReverseTransactionDao reverseTransactionDao;

    public void setReverseTransactionDao(ReverseTransactionDao reverseTransactionDao) {
        this.reverseTransactionDao = reverseTransactionDao;
    }        

    @Override
    public void addReverseTransaction(ReverseTransaction reverseTransaction) {
        reverseTransactionDao.addReverseTransaction(reverseTransaction);
    }

    @Override
    public void updateReverseTransaction(ReverseTransaction reverseTransaction) {
        reverseTransactionDao.updateReverseTransaction(reverseTransaction);
    }

    @Override
    public List<ReverseTransaction> listReverseTransactions() {
        return reverseTransactionDao.listReverseTransactions();
    }

    @Override
    public List<ReverseTransaction> getReverseTransactionByIdUsuario(int id_usuario) {
        return reverseTransactionDao.getReverseTransactionByIdUsuario(id_usuario);
    }

    @Override
    public void removeReverseTransaction(int id) {
        reverseTransactionDao.removeReverseTransaction(id);
    }

    @Override
    public List<ReverseTransaction> getPageReverseTransactions(int offset, int max) {
        return reverseTransactionDao.getPageReverseTransactions(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return reverseTransactionDao.getNumberRows();
    }

    @Override
    public List<ReverseTransaction> searchReverseTransactions(ReverseTransactionSearch reverseTransactionSearch) {
        return reverseTransactionDao.searchReverseTransactions(reverseTransactionSearch);
    }        
    
}
